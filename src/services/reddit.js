// search example
// https://www.reddit.com/r/EpicSeven/search/.json?q=flair:art&restrict_sr=1&include_over_18=1&sort=new&after=t3_un60fl
// getRedditPosts('EpicSeven', 'flair:art', true, 't3_un60fl');

// for galleries,
// search for gallery_data
// then, https://i.redd.it/{media_id}.{jpg/png/gif}

function getRedditFetchURL(subreddit) {
  return `https://www.reddit.com/r/${subreddit}/search/.json?restrict_sr=1&sort=new`;
}

export async function getRedditPosts(subreddit, query, includeNSFW, after) {
  const url = new URL(getRedditFetchURL(subreddit));
  const searchParams = url.searchParams;
  searchParams.set('q', query);
  searchParams.set('include_over_18', includeNSFW ? 1 : 0);
  searchParams.set('after', after ?? '');

  const response = await fetch(url.href);

  if (!response.ok) throw new Error('Fail to fetch reddit posts');

  return await response.json();
}