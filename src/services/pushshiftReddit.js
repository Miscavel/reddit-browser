// search example
// https://api.pushshift.io/reddit/submission/search/?subreddit=EpicSeven&limit=100&q=foo&after=1652773266
// getPushShiftRedditPosts('EpicSeven', 100, 'foo', undefined, 1652773266);

// for galleries,
// search for gallery_data
// then, https://i.redd.it/{media_id}.{jpg/png/gif}

function getPushShiftRedditFetchURL(subreddit) {
  return `https://api.pushshift.io/reddit/submission/search/?subreddit=${subreddit}`;
}

export async function getPushShiftRedditPosts(subreddit, limit, query, before) {
  const url = new URL(getPushShiftRedditFetchURL(subreddit));
  const searchParams = url.searchParams;

  /**
   * Only set if the value is not undefined
   */
  limit && searchParams.set('limit', limit);
  query && searchParams.set('q', query);
  before && searchParams.set('before', before);

  const response = await fetch(url.href);

  if (!response.ok) throw new Error('Fail to fetch reddit posts');

  return await response.json();
}