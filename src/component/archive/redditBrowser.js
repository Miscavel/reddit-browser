import { getRedditPosts } from '../services/reddit';
import { useEffect, useState } from 'react';

function RedditBrowser() {
  const [current, setCurrent] = useState('-');
  const [postData, setPostData] = useState({});

  function cleanUpData(resp) {
    resp?.data?.children?.forEach((post) => {
      const { url_overridden_by_dest, media_metadata, is_video, media } = post.data;
      post.data.image_urls = [];
      post.data.video_urls = [];

      if (url_overridden_by_dest?.includes('youtu.be')) {
        post.data.video_urls.push(url_overridden_by_dest.replace('youtu.be','youtube.com/embed'));
      } else if (is_video) {
        post.data.video_urls.push(media.reddit_video.fallback_url);
      } else {
        if (media_metadata) {
          Object.entries(media_metadata).forEach(([key, val]) => {
            const ext = val.m.replace('image/', '.');
            post.data.image_urls.push(`https://i.redd.it/${key}${ext}`);
          });
        } else {
          post.data.image_urls.push(url_overridden_by_dest);
        }
      }
    });
  }

  function refreshData(subreddit, query, nsfw, after) {
    getRedditPosts(subreddit, query, nsfw, after).then((resp) => {
      cleanUpData(resp);
      console.log(resp);
      setPostData(resp);
    });
  }

  function goToNextPage() {
    const { after } = postData?.data || {};
    const params = new URLSearchParams(window.location.search);
    params.set('after', after);
    window.location.search = params.toString();
  }

  useEffect(() => {
    const url = new URL(window.location.href);
    const after = url.searchParams.get('after');
    setCurrent(after ?? '-');
    refreshData('EpicSeven', 'flair:art', true, after);
  }, []);

  return (
    <>
      <div>
          Current: { current } / 
          <a href='#' style={{ color: 'green' }} onClick={goToNextPage}>
            Next: { postData?.data?.after }
          </a>
      </div>
      <div style={{ display: 'grid', gridTemplateColumns: 'auto auto auto', gap: '50px 50px', width: '75%' }}>
        {
          postData?.data?.children?.map((post, i) => {
            const { title, image_urls, video_urls, permalink } = post.data;
            return (
              <div key={i}>
                <div style={{ display: 'grid', gridTemplateRows: '75px', gridAutoRows: 'auto', height: '100%' }}>
                  <div>
                    <a href={`https://reddit.com/${permalink}`} style={{ color: 'white' }} target='_blank'>
                      {i}. {title}
                    </a>
                  </div>
                  <div style={{ display: 'grid', gridTemplateColumns: 'auto auto' }}>
                    {
                      image_urls.map((image, i) => {
                        return (
                          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }} key={i}>
                            <a href={image} target='_blank'>
                              <img src={image} style={{ width: '75%' }}></img>
                            </a>
                          </div>
                        );
                      })
                    }
                    {
                      video_urls.map((video, i) => {
                        return (
                          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }} key={i}>
                            <iframe src={video} style={{ width: '75%' }}></iframe>
                          </div>
                        );
                      })
                    }
                  </div>
                </div>
              </div>
            );
          })
        }
      </div>
      <div>
        Current: { current } / 
        <a href='#' style={{ color: 'green' }} onClick={goToNextPage}>
          Next: { postData?.data?.after }
        </a>
      </div>
    </>
  );
}

export default RedditBrowser;