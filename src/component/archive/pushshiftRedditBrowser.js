import { getPushShiftRedditPosts } from '../services/pushshiftReddit';
import { useEffect, useState } from 'react';

function PushShiftRedditBrowser() {
  const [current, setCurrent] = useState('-');
  const [postData, setPostData] = useState({});

  function getOldestPostTimestamp(resp) {
    let minTimestamp = Number.MAX_SAFE_INTEGER;
    resp.data.forEach((post) => {
      minTimestamp = Math.min(minTimestamp, post.created_utc);
    });
    return minTimestamp;
  }

  function cleanUpData(resp, flair) {
    /**
     * Consolidate image and video urls
     */
    resp?.data?.forEach((post) => {
      const { url, url_overridden_by_dest, media_metadata } = post;
      post.image_urls = [];
      post.video_urls = [];

      const actualUrl = url_overridden_by_dest ?? url;

      if (actualUrl?.includes('youtu.be')) {
        post.video_urls.push(actualUrl.replace('youtu.be','youtube.com/embed'));
      } else {
        if (media_metadata) {
          Object.entries(media_metadata).forEach(([key, val]) => {
            try {
              const ext = val.m.replace('image/', '.');
              post.image_urls.push(`https://i.redd.it/${key}${ext}`);
            } catch(e) {
              console.log(key);
              console.log(val);
            }
          });
        } else {
          post.image_urls.push(actualUrl);
        }
      }
    });

    if (flair) {
      resp.data = resp?.data?.filter((post) => {
        return post.link_flair_text === flair;
      });
    }
  }

  function fetchData(subreddit, entryPerPage, query, flair, before, prevResult) {
    return getPushShiftRedditPosts(subreddit, 100, query, before).then((resp) => {
      const oldestTimestamp = getOldestPostTimestamp(resp);
      cleanUpData(resp, flair);

      if (prevResult) {
        resp.data = [ ...prevResult.data, ...resp.data ];
      }
      
      if (resp.data.length === entryPerPage) {
        return { data: resp.data, oldestTimestamp };
      } else if (resp.data.length > entryPerPage) {
        return { data: resp.data.slice(0, entryPerPage), oldestTimestamp: resp.data[entryPerPage - 1].created_utc };
      }
      return fetchData(subreddit, entryPerPage, query, flair, oldestTimestamp, resp);
    });
  }

  function goToNextPage() {
    const { before } = postData || {};
    const params = new URLSearchParams(window.location.search);
    params.set('before', before);
    window.location.search = params.toString();
  }

  useEffect(() => {
    const url = new URL(window.location.href);
    const before = url.searchParams.get('before');
    fetchData('EpicSeven', 25, undefined, 'Art', before).then((result) => {
      const { data, oldestTimestamp } = result;
      setPostData({
        data,
        before: oldestTimestamp
      });
    });
  }, []);

  return (
    <>
      <div>
          Current: { current } / 
          <a href='#' style={{ color: 'green' }} onClick={goToNextPage}>
            Next: { postData?.before }
          </a>
      </div>
      <div style={{ display: 'grid', gridTemplateColumns: 'auto auto auto', gap: '50px 50px', width: '75%' }}>
        {
          postData?.data?.map((post, i) => {
            const { title, image_urls, video_urls, permalink } = post;
            return (
              <div key={i}>
                <div style={{ display: 'grid', gridTemplateRows: '75px', gridAutoRows: 'auto', height: '100%' }}>
                  <div>
                    <a href={`https://reddit.com/${permalink}`} style={{ color: 'white' }} target='_blank'>
                      {i}. {title}
                    </a>
                  </div>
                  <div style={{ display: 'grid', gridTemplateColumns: 'auto auto' }}>
                    {
                      image_urls.map((image, i) => {
                        return (
                          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }} key={i}>
                            <a href={image} target='_blank'>
                              <img src={image} style={{ width: '75%' }}></img>
                            </a>
                          </div>
                        );
                      })
                    }
                    {
                      video_urls.map((video, i) => {
                        return (
                          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }} key={i}>
                            <iframe src={video} style={{ width: '75%' }}></iframe>
                          </div>
                        );
                      })
                    }
                  </div>
                </div>
              </div>
            );
          })
        }
      </div>
      <div>
        Current: { current } / 
        <a href='#' style={{ color: 'green' }} onClick={goToNextPage}>
          Next: { postData?.before }
        </a>
      </div>
    </>
  );
}

export default PushShiftRedditBrowser;