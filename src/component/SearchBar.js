import { useEffect, useRef } from "react";

function SearchBar({ onSearch, isLoading, oldestItemTimestamp }) {
  const subredditInput = useRef(null);
  const queryInput = useRef(null);
  const dateInput = useRef(null);

  function onSubmit(event) {
    // Do not redirect on submission
    event.preventDefault();

    const subreddit = subredditInput.current.value;
    const query = queryInput.current.value;
    const timestamp = Math.round(new Date(dateInput.current.value || Date.now()).getTime() / 1000);

    // Custom search for flair
    if (query.includes('flair:')) {
      const flair = query.replace('flair:', '');
      onSearch(subreddit, '', timestamp, flair);
    } else {
      onSearch(subreddit, query, timestamp);
    }
  }

  function setBeforeToTimestamp(timestamp) {
    const { current } = dateInput;
    if (current) {
      const today = new Date(timestamp);
      today.setMinutes(today.getMinutes() - today.getTimezoneOffset());
      today.setSeconds(0);
      today.setMilliseconds(0);
      current.value = today.toISOString().slice(0, -1);
    }
  }

  setBeforeToTimestamp(oldestItemTimestamp);

  useEffect(function() {
    setBeforeToTimestamp(oldestItemTimestamp);
  }, [dateInput.current]);

  return (
    <form 
      className='searchbar'
      onSubmit={ onSubmit }
    >
      <input
        className='searchbar--subreddit-search' 
        placeholder='Search subreddit...' 
        type='search'
        ref={ subredditInput }
        required
      >
      </input>
      <input
        className='searchbar--keyword-search' 
        placeholder='Search keyword... (use flair:{flair_name} for flairs)'
        type='search'
        ref={ queryInput }
      >
      </input>
      <div
        className='searchbar--date-search'
      >
        <div className='searchbar--date-search-label'>
          Before
        </div>
        <input
          className='searchbar--date-search-input'
          type='datetime-local'
          ref={ dateInput }
        >
        </input>
      </div>
      <input 
        className={`searchbar--search-button ${isLoading ? 'grayscale' : ''}`}
        type='submit'
        value='Search'
        disabled={ isLoading }
      >
      </input>
    </form>
  );
}

export default SearchBar;