import { useEffect, useRef, useState } from 'react';
import Arrow from '../Arrow';
import CarouselItem from './CarouselItem';
import { clamp } from '../../../util/math';
import { getComputedStyleProps } from '../../../util/style';
import { getNumber } from '../../../util/regex';
import { useWindowSize } from '../../../util/hooks';
import { getPublicImageUrl } from '../../../util/asset';

function Carousel({ items, isLoading }) {
  const [ windowSize ] = useWindowSize();
  const contentDiv = useRef(null);
  const cursorPos = useRef(null);
  const scrollLevel = useRef(0);
  const isScrolling = useRef(false);

  function onTouchStart(event) {
    // Prevent multi-touch from messing with start position
    if (cursorPos.current) {
      return;
    }

    if (isScrolling.current) {
      cursorPos.current = null;
      return;
    }

    const { clientX, clientY } = event.touches[0];
    const { scrollLeft } = contentDiv.current;
    const { scrollTop } = document.documentElement;
    cursorPos.current = { clientX, clientY, scrollLeft, scrollTop };
  }

  function onTouchMove(event) {
    event.preventDefault();

    if (!cursorPos.current) return;

    const { clientX: startX, clientY: startY, scrollLeft, scrollTop } = cursorPos.current;
    const { clientX: currentX, clientY: currentY } = event.touches[0];

    const deltaX = currentX - startX;
    cursorPos.current.lastDeltaX = deltaX;
    contentDiv.current.scroll({ left: Math.max(scrollLeft - deltaX, 0) });

    const deltaY = currentY - startY;
    // Only scroll if user leans more towards vertical scrolling
    if (Math.abs(deltaY) > Math.abs(deltaX)) {
      window.scrollTo({ top: scrollTop - deltaY });
    }
  }

  function onTouchEnd() {
    if (!cursorPos.current) return;

    const { current } = contentDiv || {};
    if (current) {
      const { clientWidth } = current;
      const { lastDeltaX } = cursorPos.current;
      if (Math.abs(lastDeltaX) >= clientWidth * 0.2) {
        scrollContentBy(Math.sign(lastDeltaX) * -1 * clientWidth);
      } else {
        scrollContentBy(0);
      }
      cursorPos.current = null;
    }
  }

  function preventScrollOnWheelMove(event) {
    event.preventDefault();
  }

  function scrollLeft() {
    const { current } = contentDiv || {};
    if (current) {
      const { clientWidth } = current;
      scrollContentBy(-clientWidth);
    }
  }

  function scrollRight() {
    const { current } = contentDiv || {};
    if (current) {
      const { clientWidth } = current;
      scrollContentBy(clientWidth);
    }
  }

  function scrollContentBy(increment, animate = true) {
    const { current } = contentDiv || {};
    if (current) {
      scrollContentTo(scrollLevel.current + increment, animate);
    }
  }

  function scrollContentTo(left, animate = true) {
    if (isScrolling.current) return;

    const { current } = contentDiv || {};
    if (current) {
      const { scrollWidth, clientWidth } = current;
      const { paddingLeft, paddingRight } = getComputedStyleProps(current, 'paddingLeft', 'paddingRight');
      const leftBorder = getNumber(paddingLeft);
      const rightBorder = scrollWidth - getNumber(paddingRight);
      const firstSlide = leftBorder;
      const lastSlide = Math.max(rightBorder - clientWidth, 0);

      let scrollTo = left;
      if (scrollTo >= lastSlide + clientWidth) {
        scrollTo = firstSlide;
      } else if (scrollTo <= firstSlide - clientWidth) {
        scrollTo = lastSlide;
      }

      scrollTo = clamp(scrollTo, firstSlide, lastSlide);

      scrollLevel.current = scrollTo;
      if (animate) {
        current.scroll({ 
          left: scrollTo,
          behavior: 'smooth'
        });
        isScrolling.current = true;
        const scrollID = setInterval(function() {
          if (Math.abs(current.scrollLeft - scrollTo) <= 1) {
            isScrolling.current = false;
            clearInterval(scrollID);
          }
        }, 100);
      } else {
        current.scroll({
          left: scrollTo
        });
      }
    }
  }

  useEffect(function() {
    /**
     * Registers custom handlers for touch and mouse events
     * 
     * Used instead of using `on` callbacks directly due to react `on` callbacks being passive, thus
     * not allowing `preventDefault`
     */

    const { current } = contentDiv;
    current?.addEventListener('touchstart', onTouchStart);
    current?.addEventListener('touchmove', onTouchMove);
    current?.addEventListener('touchend', onTouchEnd);
    current?.addEventListener('wheel', preventScrollOnWheelMove);

    return function() {
      // Unregister on unmount
      current?.removeEventListener('touchstart', onTouchStart);
      current?.removeEventListener('touchmove', onTouchMove);
      current?.removeEventListener('touchend', onTouchEnd);
      current?.removeEventListener('wheel', preventScrollOnWheelMove);
    }
  }, [contentDiv.current]);

  useEffect(function() {
    // Add padding if content overflows
    const { current } = contentDiv;
    if (current) {
      const { clientWidth, scrollWidth } = current;
      const { paddingLeft, paddingRight } = getComputedStyleProps(current, 'paddingLeft', 'paddingRight');
      const totalWidth = scrollWidth - getNumber(paddingLeft) - getNumber(paddingRight);
      
      const isOverflow = clientWidth < totalWidth;
      current.classList.toggle('overflow', isOverflow);
    }
  }, [windowSize, contentDiv.current, items]);

  useEffect(function() {
    // To re-centralize content on resize
    scrollContentBy(0, false);
  }, [windowSize, contentDiv.current]);

  useEffect(function() {
    // Reset scroll on items refresh / new items
    scrollContentTo(0, false);
  }, [items]);

  return (
    <div className='carousel'>
      <div 
        className='carousel--content no-scrollbar'
        ref={ contentDiv }
      >
        {
          items &&
          items.map(function(item, index) {
            const { title, bgColor } = item;
            return (
              <CarouselItem
                data={ item }
                key={ index }
              >
              </CarouselItem>
            );
          })
        }
      </div>
      <div className='carousel--arrow-section left'>
        <Arrow
          orientation={ 'left' }
          onClick={ scrollLeft }
        >
        </Arrow>
      </div>
      <div className='carousel--arrow-section right'>
        <Arrow
          orientation={ 'right' }
          onClick={ scrollRight }
        >
        </Arrow>
      </div>
      {
        isLoading &&
        <div className='carousel--loading-screen'>
          <img 
            className='carousel--loading-spinner'
            src={getPublicImageUrl('spinner.gif')}
          >
          </img>
        </div>
      }
    </div>
  );
}

export default Carousel;