import { getPublicImageUrl } from '../../../util/asset';

function CarouselItem({ data }) {
  const { full_link, created_utc, author, subreddit, title, thumbnail } = data;

  const dateObj = new Date(created_utc * 1000);
  const year = dateObj.getFullYear();
  const month = dateObj.toLocaleString('en-US', { month: 'short' });
  const date = dateObj.getDate();
  const weekday = dateObj.toLocaleDateString('en-US', { weekday: 'short' });
  const time = dateObj.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

  return (
    <a href={ full_link } target='_blank' rel='noreferrer'>
      <div className='carousel--item'>
        <div className='carousel--item-header'>
          <img 
            className='carousel--item-thumbnail' 
            src={thumbnail || getPublicImageUrl('reddit.svg')}
            onError={function({ currentTarget }) {
              currentTarget.src = getPublicImageUrl('reddit.svg');
              currentTarget.classList.toggle('grayscale', true);
            }}
            onLoad={function({ currentTarget }) {
              if (currentTarget.src === thumbnail) {
                currentTarget.classList.toggle('grayscale', false);
              }
            }}
          ></img>
        </div>
        <div className='carousel--item-content'>
          <div className='carousel--item-content-left'>
            <div className='carousel--item-content-date'>
              <span className='carousel--item-content-date-month'>{ month }</span>
              <span className='carousel--item-content-date-day'>{ date }</span>
              <span className='carousel--item-content-date-year'>{ year }</span>
            </div>
          </div>
          <div className='carousel--item-content-right'>
            <div className='carousel--item-content-date-time'>
              { weekday }, { time }
            </div>
            <div className='carousel--item-content-title'>
              { author } on /r/{subreddit}
            </div>
            <div className='carousel--item-content-description'>
              { title }
            </div>
          </div>
        </div>
      </div>
    </a>
  );
}

export default CarouselItem;