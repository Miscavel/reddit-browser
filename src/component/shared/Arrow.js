function Arrow({ orientation, onClick }) {
  return (
    <div 
      className='arrow-wrapper no-select'
      onClick={ onClick }
    >
      <div className={`arrow ${orientation}`}></div>
    </div>
  );
}

export default Arrow;