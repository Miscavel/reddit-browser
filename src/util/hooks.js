import { useEffect, useState } from "react";

/**
 * Hook for window resize, used for components that require re-render on resize
 * @returns [ windowSize, setWindowSize ]
 */
export function useWindowSize() {
  const state = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });
  const [ _, setWindowSize ] = state;

  function onResize() {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  }

  useEffect(() => {
    window.addEventListener('resize', onResize);
  }, []); // Empty array ensures that effect is only run on mount

  return state;
}