export function getComputedStyleProps(element, ...props) {
  const result = {};
  const computedStyles = getComputedStyle(element);
  props.forEach(function(prop) {
    result[prop] = computedStyles[prop];
  });
  return result;
}