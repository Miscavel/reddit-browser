export function getNumber(str) {
  return Number(str.replace(/[^0-9]/g, ''));
}