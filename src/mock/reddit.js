const data = {
  "kind": "Listing",
  "data": {
    "modhash": "o1zh4n1ezleb6b6110e28877c68768eb7bbe288cfb848f1e3c",
    "dist": 25,
    "facets": {},
    "after": "t3_un60fl",
    "geo_filter": "",
    "children": [
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_e4bhuxtp",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Aria by me",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uptyxw",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.97,
          "author_flair_background_color": null,
          "ups": 297,
          "total_awards_received": 1,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 297,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/Svx4W6MoNGhqmhZ4BdnHoXjuFARKsxOo63dGcWgWgX8.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652573925,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/gc6d8uh29jz81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?auto=webp&amp;s=5bc0a5ce267fef7d72fb503aa278b69c4ecd23dc",
                  "width": 3428,
                  "height": 5100
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=6f64bdacaa0ab2c2cd80e3cdbb26fefa0fc6f1c9",
                    "width": 108,
                    "height": 160
                  },
                  {
                    "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=4349793935cd0712b14cd6949c2a008a60fb2d75",
                    "width": 216,
                    "height": 321
                  },
                  {
                    "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=5563dbb5c077fff8b2de743bf329feff95cb1239",
                    "width": 320,
                    "height": 476
                  },
                  {
                    "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=39b65caa730f229d93e5ad13d1bf5e8754490221",
                    "width": 640,
                    "height": 952
                  },
                  {
                    "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=815ad6e8389a194e98dbcd4c90d1ccab0ee6df32",
                    "width": 960,
                    "height": 1428
                  },
                  {
                    "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=aa805a8f08b7846da16e70aa710fa6c0d6c34add",
                    "width": 1080,
                    "height": 1606
                  }
                ],
                "variants": {},
                "id": "UJ3_YOusvHB321vvEuu3F1y7hMEqBxerxQq7Fn_31yI"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uptyxw",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "miusnama",
          "discussion_type": null,
          "num_comments": 5,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uptyxw/aria_by_me/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/gc6d8uh29jz81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652573925,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_omspe",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Catboy Iwazu x Fem Riolet (Art by me)",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uprj63",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.25,
          "author_flair_background_color": "#dadada",
          "ups": 0,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "8137c1c4-2563-11eb-a5b0-0e27777c28ef",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 0,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/rF93_650CcJVQoHeuxZQ3hE8SOaAjbTs7-Za6e169Xg.jpg",
          "edited": false,
          "author_flair_css_class": "four-Jenua",
          "author_flair_richtext": [
            {
              "e": "text",
              "t": "Writing an Inferno Khawazu dating sim"
            }
          ],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652565935,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/9b5xs41eliz81.png",
          "view_count": null,
          "archived": false,
          "no_follow": true,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/9b5xs41eliz81.png?auto=webp&amp;s=ac78687016f2aa08866c2c679d3f6c3a43ec05af",
                  "width": 2220,
                  "height": 3105
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/9b5xs41eliz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=79a201b8187a4c5dff8cf0b3f9b1c302bba33537",
                    "width": 108,
                    "height": 151
                  },
                  {
                    "url": "https://preview.redd.it/9b5xs41eliz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=6b552978a51671c4b6e725d1779385774b4b3d91",
                    "width": 216,
                    "height": 302
                  },
                  {
                    "url": "https://preview.redd.it/9b5xs41eliz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=addb80f66d7bfe86256af906a220174f7616ade6",
                    "width": 320,
                    "height": 447
                  },
                  {
                    "url": "https://preview.redd.it/9b5xs41eliz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=0cecb0eff22529a1333e2dbfd713e25889bf68a0",
                    "width": 640,
                    "height": 895
                  },
                  {
                    "url": "https://preview.redd.it/9b5xs41eliz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=9c0c5ad5846db1bc00ef4cf34e5b752c7b951abb",
                    "width": 960,
                    "height": 1342
                  },
                  {
                    "url": "https://preview.redd.it/9b5xs41eliz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=e94a6f6486ecd017c9ade6067949fd865ca58b0f",
                    "width": 1080,
                    "height": 1510
                  }
                ],
                "variants": {},
                "id": "VH4hDD9sQ4_MS5IckK5_oY8QElPWLRkx_UbGYADGYjc"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": "Writing an Inferno Khawazu dating sim",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uprj63",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "AwesomeTrinket",
          "discussion_type": null,
          "num_comments": 3,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/uprj63/catboy_iwazu_x_fem_riolet_art_by_me/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/9b5xs41eliz81.png",
          "subreddit_subscribers": 142702,
          "created_utc": 1652565935,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_113mgb",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Day 5.",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_upkwqc",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.93,
          "author_flair_background_color": null,
          "ups": 211,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 211,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/VPQabjxHXapcocQw4YLnpiNgfqP2RczWpA1uWIqHPe4.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652545936,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/6jwlwsjzxgz81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/6jwlwsjzxgz81.jpg?auto=webp&amp;s=a658b8d4b91aa6b5ed8fb1b7d17626421941ae56",
                  "width": 1080,
                  "height": 1920
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/6jwlwsjzxgz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=dc873ae56e3edc234b5c358d59a1df14e514ac04",
                    "width": 108,
                    "height": 192
                  },
                  {
                    "url": "https://preview.redd.it/6jwlwsjzxgz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=61d5bbceba34568b87c63cfaa9de29e87496e049",
                    "width": 216,
                    "height": 384
                  },
                  {
                    "url": "https://preview.redd.it/6jwlwsjzxgz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=c0c56f361e16ae42129c390276a339f76d2c469a",
                    "width": 320,
                    "height": 568
                  },
                  {
                    "url": "https://preview.redd.it/6jwlwsjzxgz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=dad9a885b1b8fd65e473fd101a4a87eba073cdeb",
                    "width": 640,
                    "height": 1137
                  },
                  {
                    "url": "https://preview.redd.it/6jwlwsjzxgz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=dc76122e04ef7d06456057b58a7c7c266299fe1b",
                    "width": 960,
                    "height": 1706
                  },
                  {
                    "url": "https://preview.redd.it/6jwlwsjzxgz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=378a634faac3f2a1fe6e4ca6328fe0823089b058",
                    "width": 1080,
                    "height": 1920
                  }
                ],
                "variants": {},
                "id": "NyuRAqSLBAXDkl8J3a9u21jYEMeYzNuCQ2kSl5eh3i4"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "upkwqc",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "ScumCommander",
          "discussion_type": null,
          "num_comments": 15,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/upkwqc/day_5/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/6jwlwsjzxgz81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652545936,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_11mysv",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Falconer Kluri [art by moon1920]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 128,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_upi5am",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.99,
          "author_flair_background_color": "#dadada",
          "ups": 660,
          "total_awards_received": 2,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "33ca2992-f53e-11e8-9b8e-0e463599d056",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": false,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 660,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/2BtcQprCemd8uvkd-9RGLeb6d3H9VDQ4pKz8hPz3QjI.jpg",
          "edited": false,
          "author_flair_css_class": "one-achates",
          "author_flair_richtext": [
            {
              "e": "text",
              "t": "Release Saskia and Paradoxia, SG!"
            }
          ],
          "gildings": {
            "gid_1": 2
          },
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652537722,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.imgur.com",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.imgur.com/4EX1io9.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://external-preview.redd.it/EEO6G9cnwFxzpHPIPKNXsnfcAfPaPvlwqt_7wgHo0ZU.jpg?auto=webp&amp;s=0f8ef14f526f7576c1bd31d54932f1286aaa0891",
                  "width": 1904,
                  "height": 1753
                },
                "resolutions": [
                  {
                    "url": "https://external-preview.redd.it/EEO6G9cnwFxzpHPIPKNXsnfcAfPaPvlwqt_7wgHo0ZU.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=1cec9e9c2ff4991cc4661258d9ebcefd8c1cec48",
                    "width": 108,
                    "height": 99
                  },
                  {
                    "url": "https://external-preview.redd.it/EEO6G9cnwFxzpHPIPKNXsnfcAfPaPvlwqt_7wgHo0ZU.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=032cd6db9022ac53ce6e8b957421c4ae0cd388f6",
                    "width": 216,
                    "height": 198
                  },
                  {
                    "url": "https://external-preview.redd.it/EEO6G9cnwFxzpHPIPKNXsnfcAfPaPvlwqt_7wgHo0ZU.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=4e38ed4f9d1e429d170d2e0757acc8f18233ec71",
                    "width": 320,
                    "height": 294
                  },
                  {
                    "url": "https://external-preview.redd.it/EEO6G9cnwFxzpHPIPKNXsnfcAfPaPvlwqt_7wgHo0ZU.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=47e3fd2bc3d0c3cc94250fb692407f9d842b0068",
                    "width": 640,
                    "height": 589
                  },
                  {
                    "url": "https://external-preview.redd.it/EEO6G9cnwFxzpHPIPKNXsnfcAfPaPvlwqt_7wgHo0ZU.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=877b8f8fbc99ba88c3a274287e2581d8e91dd7dd",
                    "width": 960,
                    "height": 883
                  },
                  {
                    "url": "https://external-preview.redd.it/EEO6G9cnwFxzpHPIPKNXsnfcAfPaPvlwqt_7wgHo0ZU.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=1737626796b2cade6a31f8f3a0b1be1f7eac6dcd",
                    "width": 1080,
                    "height": 994
                  }
                ],
                "variants": {},
                "id": "WvWCS5zZYI7rWFwy3XlBnfT5-EDmI-yQIEYKchwI8I0"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 100,
              "id": "gid_1",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 512,
              "static_icon_width": 512,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Shows the Silver Award... and that's it.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 2,
              "static_icon_height": 512,
              "name": "Silver",
              "resized_static_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 512,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": "Release Saskia and Paradoxia, SG!",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "upi5am",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "YoMikeeHey",
          "discussion_type": null,
          "num_comments": 8,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/upi5am/falconer_kluri_art_by_moon1920/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.imgur.com/4EX1io9.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652537722,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_pf62m",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Sez [art by Evan]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 85,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_upgbbj",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.99,
          "author_flair_background_color": null,
          "ups": 276,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 276,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/HItl83mi5F7rPWNPWsXPDchoP-ybB3-H05JgDkCaONg.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652531633,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/7tk8noc5rfz81.png",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/7tk8noc5rfz81.png?auto=webp&amp;s=a380820f7b7d3f6b459b891539f1a1145023c600",
                  "width": 3160,
                  "height": 1920
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/7tk8noc5rfz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=31bca7721156e73e49c93f396d95b611a073fc89",
                    "width": 108,
                    "height": 65
                  },
                  {
                    "url": "https://preview.redd.it/7tk8noc5rfz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=aeb26f2b8db0fbbc5be63c7753fcfed0da2a27fb",
                    "width": 216,
                    "height": 131
                  },
                  {
                    "url": "https://preview.redd.it/7tk8noc5rfz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=2695c97cd7e566110b853a47f9ca4eb9821fa5ff",
                    "width": 320,
                    "height": 194
                  },
                  {
                    "url": "https://preview.redd.it/7tk8noc5rfz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=9b9a01ac8cc2663973bf04fe916a6d6e86c576a3",
                    "width": 640,
                    "height": 388
                  },
                  {
                    "url": "https://preview.redd.it/7tk8noc5rfz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=20aceccf6938a0f63ff1e4e4b975c4546f8f81bb",
                    "width": 960,
                    "height": 583
                  },
                  {
                    "url": "https://preview.redd.it/7tk8noc5rfz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=4198459185221f79f334fa941e5bdc377a1ede05",
                    "width": 1080,
                    "height": 656
                  }
                ],
                "variants": {},
                "id": "VoEgPIwg0piSIPh0Lj9kl9m-9hJRcn8ge3zzkNRiu2U"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "upgbbj",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Triple_S_Rank",
          "discussion_type": null,
          "num_comments": 6,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/upgbbj/sez_art_by_evan/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/7tk8noc5rfz81.png",
          "subreddit_subscribers": 142702,
          "created_utc": 1652531633,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_e4bhuxtp",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Luluca &amp; Lilibet by me",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_up4drf",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.95,
          "author_flair_background_color": null,
          "ups": 647,
          "total_awards_received": 4,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 647,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "nsfw",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {
            "gid_1": 1
          },
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652484796,
          "link_flair_type": "richtext",
          "wls": 3,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/yaes0adpvbz81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": true,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/yaes0adpvbz81.jpg?auto=webp&amp;s=a473135fd9654ac3962859f525db5b996ac70837",
                  "width": 3428,
                  "height": 5100
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=c1304d696cc0808744bdcd64db2113085b6ed46b",
                    "width": 108,
                    "height": 160
                  },
                  {
                    "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=7e1af1ff02857188edde5479ca02234437f25ce2",
                    "width": 216,
                    "height": 321
                  },
                  {
                    "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=f126f3eda09c92d0eb10d49410497926ef9ef8d4",
                    "width": 320,
                    "height": 476
                  },
                  {
                    "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=41d57408ab36a07336b04cde2646513b95d4e640",
                    "width": 640,
                    "height": 952
                  },
                  {
                    "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=1a5e2b7f97aa195f1037e1f3b0cc50ef013cc664",
                    "width": 960,
                    "height": 1428
                  },
                  {
                    "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=04216b8c3bbf7aad889ae1d74693ca03140c7ee5",
                    "width": 1080,
                    "height": 1606
                  }
                ],
                "variants": {
                  "obfuscated": {
                    "source": {
                      "url": "https://preview.redd.it/yaes0adpvbz81.jpg?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=62afc633663bc14dc66d0b47c4e51210fb96f5ac",
                      "width": 3428,
                      "height": 5100
                    },
                    "resolutions": [
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=3fa2fc182748ee297dbd41fdd104747f0a7bc406",
                        "width": 108,
                        "height": 160
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=bc9528bbaa85d78139be6deb94fb12903aa79b75",
                        "width": 216,
                        "height": 321
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=a0d1ae1f5d9cd30b5785699b8d6c5d7f456ddefc",
                        "width": 320,
                        "height": 476
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=7012fbad8d08eeff9f1fe3b809348f2d83d7a310",
                        "width": 640,
                        "height": 952
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=960&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=aef1e8413577c362268f79a1874a2899d970b87d",
                        "width": 960,
                        "height": 1428
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=1080&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=855df118dd3bc480ecb5cfda4149ddc1120df982",
                        "width": 1080,
                        "height": 1606
                      }
                    ]
                  },
                  "nsfw": {
                    "source": {
                      "url": "https://preview.redd.it/yaes0adpvbz81.jpg?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=62afc633663bc14dc66d0b47c4e51210fb96f5ac",
                      "width": 3428,
                      "height": 5100
                    },
                    "resolutions": [
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=3fa2fc182748ee297dbd41fdd104747f0a7bc406",
                        "width": 108,
                        "height": 160
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=bc9528bbaa85d78139be6deb94fb12903aa79b75",
                        "width": 216,
                        "height": 321
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=a0d1ae1f5d9cd30b5785699b8d6c5d7f456ddefc",
                        "width": 320,
                        "height": 476
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=7012fbad8d08eeff9f1fe3b809348f2d83d7a310",
                        "width": 640,
                        "height": 952
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=960&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=aef1e8413577c362268f79a1874a2899d970b87d",
                        "width": 960,
                        "height": 1428
                      },
                      {
                        "url": "https://preview.redd.it/yaes0adpvbz81.jpg?width=1080&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=855df118dd3bc480ecb5cfda4149ddc1120df982",
                        "width": 1080,
                        "height": 1606
                      }
                    ]
                  }
                },
                "id": "LaOEpRMQ5iOYDjRE9VsxDl-7-w70vrM9tCrHvy_qeo0"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 100,
              "id": "gid_1",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 512,
              "static_icon_width": 512,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Shows the Silver Award... and that's it.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 512,
              "name": "Silver",
              "resized_static_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 512,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 150,
              "id": "award_f44611f1-b89e-46dc-97fe-892280b13b82",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Thank you stranger. Shows the award.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 2,
              "static_icon_height": 2048,
              "name": "Helpful",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "up4drf",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "miusnama",
          "discussion_type": null,
          "num_comments": 15,
          "send_replies": true,
          "whitelist_status": "promo_adult_nsfw",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/up4drf/luluca_lilibet_by_me/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/yaes0adpvbz81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652484796,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_kdmbe",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Iseria by Huge",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 123,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uov2z3",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.99,
          "author_flair_background_color": null,
          "ups": 1239,
          "total_awards_received": 3,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 1239,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/0kOSJCCFa9UeNK9Et6BPhNvpx_DT5OwZK2LZ4cKuFSA.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652457937,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/ag39f3sbo9z81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/ag39f3sbo9z81.jpg?auto=webp&amp;s=98cb3c616f4617c1429d2347fdc017e8ea334b66",
                  "width": 1200,
                  "height": 1057
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/ag39f3sbo9z81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=17e04bf8a68b1ebebb9462df5fb2e4d4cd0e1c20",
                    "width": 108,
                    "height": 95
                  },
                  {
                    "url": "https://preview.redd.it/ag39f3sbo9z81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=af62b8f698bbe4f54b7e5836fff1ee02fe5eebd4",
                    "width": 216,
                    "height": 190
                  },
                  {
                    "url": "https://preview.redd.it/ag39f3sbo9z81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=0353949a30d78020d991bed0bac3a08879e01b3e",
                    "width": 320,
                    "height": 281
                  },
                  {
                    "url": "https://preview.redd.it/ag39f3sbo9z81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=562eaf61af2f643f6676c95c3f5835976865d1aa",
                    "width": 640,
                    "height": 563
                  },
                  {
                    "url": "https://preview.redd.it/ag39f3sbo9z81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=b1883ffa0043a571fa152f096f511a0bbac5d969",
                    "width": 960,
                    "height": 845
                  },
                  {
                    "url": "https://preview.redd.it/ag39f3sbo9z81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=439851166821575a2a9100e900ca194933caf60c",
                    "width": 1080,
                    "height": 951
                  }
                ],
                "variants": {},
                "id": "rGo2_RFeOBwqFofbeDIOfb6LwrlamEnDyUGKWPOAxW0"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 3,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uov2z3",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "asaness",
          "discussion_type": null,
          "num_comments": 9,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uov2z3/iseria_by_huge/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/ag39f3sbo9z81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652457937,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_11mysv",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Luna [art by FAL_si0n]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uosqdf",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.97,
          "author_flair_background_color": "#dadada",
          "ups": 771,
          "total_awards_received": 2,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "33ca2992-f53e-11e8-9b8e-0e463599d056",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": false,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 771,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/nZDbvqCHteVWw39FzL89Oogw0yyoFx3CNan1HVbKOuE.jpg",
          "edited": false,
          "author_flair_css_class": "one-achates",
          "author_flair_richtext": [
            {
              "e": "text",
              "t": "Release Saskia and Paradoxia, SG!"
            }
          ],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652451352,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.imgur.com",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.imgur.com/KYhIbiL.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://external-preview.redd.it/uPt0O_XD-51vPgCGLyx4PSYqXON6ZgE49FaTiFXjMks.jpg?auto=webp&amp;s=bfc885e34dbb93af8d36fd009e01ef0a18251148",
                  "width": 1552,
                  "height": 2048
                },
                "resolutions": [
                  {
                    "url": "https://external-preview.redd.it/uPt0O_XD-51vPgCGLyx4PSYqXON6ZgE49FaTiFXjMks.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=a36c6c6751bf6b4547199f36c08dc60040f05735",
                    "width": 108,
                    "height": 142
                  },
                  {
                    "url": "https://external-preview.redd.it/uPt0O_XD-51vPgCGLyx4PSYqXON6ZgE49FaTiFXjMks.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=b7da0372c188884ecdc3c0f0a509423a37f27602",
                    "width": 216,
                    "height": 285
                  },
                  {
                    "url": "https://external-preview.redd.it/uPt0O_XD-51vPgCGLyx4PSYqXON6ZgE49FaTiFXjMks.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=46899f46b1a4a2742a8595ce83e5d209481c9865",
                    "width": 320,
                    "height": 422
                  },
                  {
                    "url": "https://external-preview.redd.it/uPt0O_XD-51vPgCGLyx4PSYqXON6ZgE49FaTiFXjMks.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=8f83df92c7990b8776d77532aba850edeb5b9421",
                    "width": 640,
                    "height": 844
                  },
                  {
                    "url": "https://external-preview.redd.it/uPt0O_XD-51vPgCGLyx4PSYqXON6ZgE49FaTiFXjMks.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=2d819f2e5819c02b7863b45e0d16e0d0e876ff24",
                    "width": 960,
                    "height": 1266
                  },
                  {
                    "url": "https://external-preview.redd.it/uPt0O_XD-51vPgCGLyx4PSYqXON6ZgE49FaTiFXjMks.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=19a3542d77f55477644f9ccd600ec327fba58c94",
                    "width": 1080,
                    "height": 1425
                  }
                ],
                "variants": {},
                "id": "x5HhkTdUAnp0J273HUz4Keg1fkWJQ1cp4Ydr2WmmXSI"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 150,
              "id": "award_f44611f1-b89e-46dc-97fe-892280b13b82",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Thank you stranger. Shows the award.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Helpful",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": "Release Saskia and Paradoxia, SG!",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uosqdf",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "YoMikeeHey",
          "discussion_type": null,
          "num_comments": 7,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/uosqdf/luna_art_by_fal_si0n/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.imgur.com/KYhIbiL.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652451352,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_pf62m",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Lionheart Cermia [art by I.A.N.]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uoqwdi",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.99,
          "author_flair_background_color": null,
          "ups": 690,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 690,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/7MyaE69pAcPbwrC5EC1Csdk51WWqn_4Zg8jBmxWSgS4.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652445845,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/3t1fcab7o8z81.png",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/3t1fcab7o8z81.png?auto=webp&amp;s=6e0a9443d2792215dbfda04b0f040040498baf3c",
                  "width": 4320,
                  "height": 6000
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/3t1fcab7o8z81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=c153d4173943def4ea0ceeaf2787d4edfcd4f2c4",
                    "width": 108,
                    "height": 150
                  },
                  {
                    "url": "https://preview.redd.it/3t1fcab7o8z81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=4fb7b77e4c7e71e149d780464d6c332c47542f38",
                    "width": 216,
                    "height": 300
                  },
                  {
                    "url": "https://preview.redd.it/3t1fcab7o8z81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=e74a817acf4cc7f9d6288a6d56dc9b59533376db",
                    "width": 320,
                    "height": 444
                  },
                  {
                    "url": "https://preview.redd.it/3t1fcab7o8z81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=dc69018b77831828290f1a3f2be886d7cdd04911",
                    "width": 640,
                    "height": 888
                  },
                  {
                    "url": "https://preview.redd.it/3t1fcab7o8z81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=f93b8c51c4edfc6d36caf3d6deb70d0c3179ff19",
                    "width": 960,
                    "height": 1333
                  },
                  {
                    "url": "https://preview.redd.it/3t1fcab7o8z81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=62fba50e334171552396cc7820e365a6caccb9b0",
                    "width": 1080,
                    "height": 1500
                  }
                ],
                "variants": {},
                "id": "oNyYMqjVfuw335ME3mgRd85SbUF3BGD8kFO_VqlXbvs"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uoqwdi",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Triple_S_Rank",
          "discussion_type": null,
          "num_comments": 10,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uoqwdi/lionheart_cermia_art_by_ian/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/3t1fcab7o8z81.png",
          "subreddit_subscribers": 142702,
          "created_utc": 1652445845,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_113mgb",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Plop. day 4. Should be an guess.",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uoqasj",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.92,
          "author_flair_background_color": null,
          "ups": 285,
          "domain": "i.redd.it",
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": "",
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 285,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/FjyJj0pyfsGvsU2Hj3MLYuMNq3HQj7R0YswQrCuKyx8.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652443809,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "total_awards_received": 2,
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/zy9o8u9bi8z81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/zy9o8u9bi8z81.jpg?auto=webp&amp;s=b91f27ea836db2f7872b402432ac6c66b65dc852",
                  "width": 1080,
                  "height": 1920
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/zy9o8u9bi8z81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=99a262c8c3458e5f59e40120d5cb79393e0d2ef1",
                    "width": 108,
                    "height": 192
                  },
                  {
                    "url": "https://preview.redd.it/zy9o8u9bi8z81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=517213333f1d9ecaf81745320c7c6ecad3694e35",
                    "width": 216,
                    "height": 384
                  },
                  {
                    "url": "https://preview.redd.it/zy9o8u9bi8z81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=11a8c5b7d58f12bedc452b1b602a67825ed7e131",
                    "width": 320,
                    "height": 568
                  },
                  {
                    "url": "https://preview.redd.it/zy9o8u9bi8z81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=7b6585f2b6ad282b4fb64a8df88695359eeaf118",
                    "width": 640,
                    "height": 1137
                  },
                  {
                    "url": "https://preview.redd.it/zy9o8u9bi8z81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=d79bdd2cca0457ff3690f5507248a7d67bb5930c",
                    "width": 960,
                    "height": 1706
                  },
                  {
                    "url": "https://preview.redd.it/zy9o8u9bi8z81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=0527dce211b0e08f6b80c07322eea0efdb47a00c",
                    "width": 1080,
                    "height": 1920
                  }
                ],
                "variants": {},
                "id": "Sai_0wI-JEENoCAqMJkHuSOjyLIdkzKuQg9xmaXoiWY"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 100,
              "id": "award_b4072731-c0fb-4440-adc7-1063d6a5e6a0",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=16&amp;height=16&amp;auto=webp&amp;s=8e644eb9ffccee5f11d72e759883a6c825f7d89e",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=32&amp;height=32&amp;auto=webp&amp;s=c49ad07c88610c7efe98a54453d9ce5ddf887a1d",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=48&amp;height=48&amp;auto=webp&amp;s=80eb767a877a78c181af1385c2ed98f067b38092",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=64&amp;height=64&amp;auto=webp&amp;s=b2b65ceeff9933f5e70387893e661b7e9f1f1556",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=128&amp;height=128&amp;auto=webp&amp;s=aec3cf53a1aeabe4c2ecc4ad83b2d0f2993d1afd",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "C'est magnifique",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Masterpiece",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=16&amp;height=16&amp;auto=webp&amp;s=8e644eb9ffccee5f11d72e759883a6c825f7d89e",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=32&amp;height=32&amp;auto=webp&amp;s=c49ad07c88610c7efe98a54453d9ce5ddf887a1d",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=48&amp;height=48&amp;auto=webp&amp;s=80eb767a877a78c181af1385c2ed98f067b38092",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=64&amp;height=64&amp;auto=webp&amp;s=b2b65ceeff9933f5e70387893e661b7e9f1f1556",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png?width=128&amp;height=128&amp;auto=webp&amp;s=aec3cf53a1aeabe4c2ecc4ad83b2d0f2993d1afd",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": "PNG",
              "icon_height": 2048,
              "penny_price": 0,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/2juh333m40n51_Masterpiece.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "call_to_action": "",
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uoqasj",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "ScumCommander",
          "discussion_type": null,
          "num_comments": 32,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uoqasj/plop_day_4_should_be_an_guess/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/zy9o8u9bi8z81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652443809,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_8kicdufx",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Spirit Eye Celine in swimsuit (by me)",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uoonth",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.94,
          "author_flair_background_color": null,
          "ups": 537,
          "total_awards_received": 2,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 537,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "nsfw",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652437674,
          "link_flair_type": "richtext",
          "wls": 3,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/bbdg0a5008z81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": true,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/bbdg0a5008z81.jpg?auto=webp&amp;s=7b4433639ad2327a9bbe9b4847cfcb4983c1ae17",
                  "width": 1900,
                  "height": 2990
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=b0b5a447b0a8d170a2bcdf1fc2e2cefdaace7d41",
                    "width": 108,
                    "height": 169
                  },
                  {
                    "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=46eb4c43c81f32f8240e5af02303997fe33a2aa0",
                    "width": 216,
                    "height": 339
                  },
                  {
                    "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=ccf418a3f5cc0e01e89321504277ae24e1e6c79c",
                    "width": 320,
                    "height": 503
                  },
                  {
                    "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=78f695663a355e78624b40db49eb9e1ea810dd9a",
                    "width": 640,
                    "height": 1007
                  },
                  {
                    "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=60710d859c4d6aa76310a4ae7f6a9854abea4e2a",
                    "width": 960,
                    "height": 1510
                  },
                  {
                    "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=acf7d5ef855ddd0ff029f34f56e88ab342d2164a",
                    "width": 1080,
                    "height": 1699
                  }
                ],
                "variants": {
                  "obfuscated": {
                    "source": {
                      "url": "https://preview.redd.it/bbdg0a5008z81.jpg?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=6ad9a9a8ae2e9520fa17ac4f3334dc3df3b20c13",
                      "width": 1900,
                      "height": 2990
                    },
                    "resolutions": [
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=2d97f0785050b6016d52dbe5b60e2e026557d8c1",
                        "width": 108,
                        "height": 169
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=660961c5c65893f52b19de7ca7333fb2dbac7289",
                        "width": 216,
                        "height": 339
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=08e2d89a4307c51853b16fc80275e805b7c69149",
                        "width": 320,
                        "height": 503
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=a6565b070f759fda57a8e5804db75e727eb2b468",
                        "width": 640,
                        "height": 1007
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=960&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=ea8cb537b03b147cf50b44d37e1e83a753f322fd",
                        "width": 960,
                        "height": 1510
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=1080&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=4a6a8e83342a254febd74a8028c3aadcc09b74de",
                        "width": 1080,
                        "height": 1699
                      }
                    ]
                  },
                  "nsfw": {
                    "source": {
                      "url": "https://preview.redd.it/bbdg0a5008z81.jpg?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=6ad9a9a8ae2e9520fa17ac4f3334dc3df3b20c13",
                      "width": 1900,
                      "height": 2990
                    },
                    "resolutions": [
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=2d97f0785050b6016d52dbe5b60e2e026557d8c1",
                        "width": 108,
                        "height": 169
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=660961c5c65893f52b19de7ca7333fb2dbac7289",
                        "width": 216,
                        "height": 339
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=08e2d89a4307c51853b16fc80275e805b7c69149",
                        "width": 320,
                        "height": 503
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=a6565b070f759fda57a8e5804db75e727eb2b468",
                        "width": 640,
                        "height": 1007
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=960&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=ea8cb537b03b147cf50b44d37e1e83a753f322fd",
                        "width": 960,
                        "height": 1510
                      },
                      {
                        "url": "https://preview.redd.it/bbdg0a5008z81.jpg?width=1080&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=4a6a8e83342a254febd74a8028c3aadcc09b74de",
                        "width": 1080,
                        "height": 1699
                      }
                    ]
                  }
                },
                "id": "GFEtEitwfQKikBnagx0rQ6kcQiK2DvXlfy-5q-D_iDI"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 150,
              "id": "award_f44611f1-b89e-46dc-97fe-892280b13b82",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Thank you stranger. Shows the award.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Helpful",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uoonth",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Relative-Rip3912",
          "discussion_type": null,
          "num_comments": 18,
          "send_replies": true,
          "whitelist_status": "promo_adult_nsfw",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uoonth/spirit_eye_celine_in_swimsuit_by_me/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/bbdg0a5008z81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652437674,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_h8odxq9u",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "finally after 1 year of E7 , with the pass, I bought the best skin of the game",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 71,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uoo581",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.93,
          "author_flair_background_color": null,
          "ups": 121,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 121,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/t9gzFEv2N1UCTTghkzpzQEML5f3qGNG9MZeTzr2zKv8.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652435424,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/ejf8idqdt7z81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/ejf8idqdt7z81.jpg?auto=webp&amp;s=d11da4d8bf71b9793c84275bcc8908e3f6d265da",
                  "width": 1875,
                  "height": 954
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/ejf8idqdt7z81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=dc47d24d5f7ca1ea2ca1f98ecc5dfbcf0df025c6",
                    "width": 108,
                    "height": 54
                  },
                  {
                    "url": "https://preview.redd.it/ejf8idqdt7z81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=c2d27826f87a6c9788b04a2cd46677a6569edb27",
                    "width": 216,
                    "height": 109
                  },
                  {
                    "url": "https://preview.redd.it/ejf8idqdt7z81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=b1eb7fffe0221f62ddd0c2c5ee7944b56834ef13",
                    "width": 320,
                    "height": 162
                  },
                  {
                    "url": "https://preview.redd.it/ejf8idqdt7z81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=4b8f8548dd23fe5fcadd36fc89d825b69a3718d2",
                    "width": 640,
                    "height": 325
                  },
                  {
                    "url": "https://preview.redd.it/ejf8idqdt7z81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=b9782753815cfc172f9e37d40f975d077960a83c",
                    "width": 960,
                    "height": 488
                  },
                  {
                    "url": "https://preview.redd.it/ejf8idqdt7z81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=a25284df8cc7985ac8d384a7c3cccd75cbb29d34",
                    "width": 1080,
                    "height": 549
                  }
                ],
                "variants": {},
                "id": "U78VXBCDeaUG9k8QTXwTMtfZDrKf5_wETXomahN8BSw"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uoo581",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Big-Drink-616",
          "discussion_type": null,
          "num_comments": 23,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uoo581/finally_after_1_year_of_e7_with_the_pass_i_bought/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/ejf8idqdt7z81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652435424,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_pf62m",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Beehoo [art by Chipon]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uo3kuj",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.99,
          "author_flair_background_color": null,
          "ups": 219,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 219,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/qWKuu5S4AGXbT2LdU7nRyg4phB0pMtrJbZuaS3JTKsg.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652369018,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/hxm0eab5b2z81.png",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/hxm0eab5b2z81.png?auto=webp&amp;s=711a9c788cbce0cd9c1f472a544b9a23765987f5",
                  "width": 600,
                  "height": 870
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/hxm0eab5b2z81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=a20d8ff98bdaee87821ea277cc24ce8eff171a39",
                    "width": 108,
                    "height": 156
                  },
                  {
                    "url": "https://preview.redd.it/hxm0eab5b2z81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=bee1e8f24ee3f7a625017ccb09b2c091e27f964d",
                    "width": 216,
                    "height": 313
                  },
                  {
                    "url": "https://preview.redd.it/hxm0eab5b2z81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=1956241626eee0405b45c4c10636af29996929f7",
                    "width": 320,
                    "height": 464
                  }
                ],
                "variants": {},
                "id": "k4K9CNx-07BQsCbOGo4qQ6a1Npp9hmJB8dndJdmnTN4"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uo3kuj",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Triple_S_Rank",
          "discussion_type": null,
          "num_comments": 6,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uo3kuj/beehoo_art_by_chipon/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/hxm0eab5b2z81.png",
          "subreddit_subscribers": 142702,
          "created_utc": 1652369018,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_vkkqb",
          "saved": true,
          "mod_reason_title": null,
          "gilded": 1,
          "clicked": false,
          "title": "Spirit Eye Celine Fanart",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uo3hb6",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.98,
          "author_flair_background_color": "#d3d6da",
          "ups": 1352,
          "total_awards_received": 9,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "50e03b78-f6e9-11e8-96a4-0e285ca61702",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 1352,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": true,
          "thumbnail": "https://b.thumbs.redditmedia.com/H0JNOY9XirC2TfXork-ye9QdMogMgFVSZbMwBv7zJfk.jpg",
          "edited": false,
          "author_flair_css_class": "two-lidica",
          "author_flair_richtext": [
            {
              "a": ":lidica:",
              "e": "emoji",
              "u": "https://emoji.redditmedia.com/hjrdm4g4h1s51_t5_nrn6j/lidica"
            }
          ],
          "gildings": {
            "gid_1": 3,
            "gid_2": 1
          },
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652368750,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/gxukdtm0b2z81.png",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/gxukdtm0b2z81.png?auto=webp&amp;s=08ffdf35d2fa34a035fe904fc2917356f33ec043",
                  "width": 1308,
                  "height": 2450
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/gxukdtm0b2z81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=60155b814b97718e693ed517d3932cb602e2d91b",
                    "width": 108,
                    "height": 202
                  },
                  {
                    "url": "https://preview.redd.it/gxukdtm0b2z81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=89ad8b92d29c23047cc5a53b1abf973709e1d532",
                    "width": 216,
                    "height": 404
                  },
                  {
                    "url": "https://preview.redd.it/gxukdtm0b2z81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=9a51966386472ec7cf14a3d30050f6e9fda7c05e",
                    "width": 320,
                    "height": 599
                  },
                  {
                    "url": "https://preview.redd.it/gxukdtm0b2z81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=6e8bb996445e8ccb26e8127447c3c15d9a2b2fcb",
                    "width": 640,
                    "height": 1198
                  },
                  {
                    "url": "https://preview.redd.it/gxukdtm0b2z81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=c82b1a03b743b5dd394f3f409608e4e9b5483ab3",
                    "width": 960,
                    "height": 1798
                  },
                  {
                    "url": "https://preview.redd.it/gxukdtm0b2z81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=7b11d004e1d3a83c9fce07afa8f10b18b8e68ca8",
                    "width": 1080,
                    "height": 2022
                  }
                ],
                "variants": {},
                "id": "YN3WSa1qLLhuMJwp1WBisvst16icWYBoHxxULCRAPhk"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 500,
              "id": "gid_2",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 100,
              "icon_url": "https://www.redditstatic.com/gold/awards/icon/gold_512.png",
              "days_of_premium": 7,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 512,
              "static_icon_width": 512,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Gives 100 Reddit Coins and a week of r/lounge access and ad-free browsing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 512,
              "name": "Gold",
              "resized_static_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/gold_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 512,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://www.redditstatic.com/gold/awards/icon/gold_512.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 150,
              "id": "award_f44611f1-b89e-46dc-97fe-892280b13b82",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Thank you stranger. Shows the award.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 3,
              "static_icon_height": 2048,
              "name": "Helpful",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 2,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 100,
              "id": "gid_1",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 512,
              "static_icon_width": 512,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Shows the Silver Award... and that's it.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 3,
              "static_icon_height": 512,
              "name": "Silver",
              "resized_static_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 512,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": ":lidica:",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uo3hb6",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "katzchi",
          "discussion_type": null,
          "num_comments": 18,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/uo3hb6/spirit_eye_celine_fanart/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/gxukdtm0b2z81.png",
          "subreddit_subscribers": 142702,
          "created_utc": 1652368750,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_11mysv",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Meru [art by 桃原らいる]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uo2a54",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.98,
          "author_flair_background_color": "#dadada",
          "ups": 562,
          "total_awards_received": 1,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "33ca2992-f53e-11e8-9b8e-0e463599d056",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": false,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 562,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/4YzGp5WhOFv_ZAJV_89VzaD2cUMhyNvivo585i9J76o.jpg",
          "edited": false,
          "author_flair_css_class": "one-achates",
          "author_flair_richtext": [
            {
              "e": "text",
              "t": "Release Saskia and Paradoxia, SG!"
            }
          ],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652365476,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.imgur.com",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.imgur.com/h4Nvzov.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://external-preview.redd.it/LQ5ukZM_MlOXnHqGSBUisamP4rN6O9sSCP6iDsMIg8I.jpg?auto=webp&amp;s=2685db482abd5f2578b2428b190d5fee49e71963",
                  "width": 3009,
                  "height": 4250
                },
                "resolutions": [
                  {
                    "url": "https://external-preview.redd.it/LQ5ukZM_MlOXnHqGSBUisamP4rN6O9sSCP6iDsMIg8I.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=67401656796c6b2d3976e121b110bf8f3af7a4f8",
                    "width": 108,
                    "height": 152
                  },
                  {
                    "url": "https://external-preview.redd.it/LQ5ukZM_MlOXnHqGSBUisamP4rN6O9sSCP6iDsMIg8I.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=bb84c3f28bba1de67f795914ea70c6c5b0472623",
                    "width": 216,
                    "height": 305
                  },
                  {
                    "url": "https://external-preview.redd.it/LQ5ukZM_MlOXnHqGSBUisamP4rN6O9sSCP6iDsMIg8I.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=f7cd83d29d003690b12e4276e9b7f593a1efab97",
                    "width": 320,
                    "height": 451
                  },
                  {
                    "url": "https://external-preview.redd.it/LQ5ukZM_MlOXnHqGSBUisamP4rN6O9sSCP6iDsMIg8I.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=0a7589b25d387b463128b2ca06594cddcb331ebd",
                    "width": 640,
                    "height": 903
                  },
                  {
                    "url": "https://external-preview.redd.it/LQ5ukZM_MlOXnHqGSBUisamP4rN6O9sSCP6iDsMIg8I.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=3cfa1632165d6354a12ce17d65c3e53fc77725e1",
                    "width": 960,
                    "height": 1355
                  },
                  {
                    "url": "https://external-preview.redd.it/LQ5ukZM_MlOXnHqGSBUisamP4rN6O9sSCP6iDsMIg8I.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=c5321fb2d78e19f1cbaa5e6a19bb3ede8ababca5",
                    "width": 1080,
                    "height": 1525
                  }
                ],
                "variants": {},
                "id": "dZ0b2R9T5t4FulILNNd92grH2u-3ZEGNTxD0d62WCbg"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": "Release Saskia and Paradoxia, SG!",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uo2a54",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "YoMikeeHey",
          "discussion_type": null,
          "num_comments": 7,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/uo2a54/meru_art_by_桃原らいる/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.imgur.com/h4Nvzov.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652365476,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_ihgnf",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Tried my hand at drawing Aria",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uo26lm",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.87,
          "author_flair_background_color": "#d3d6da",
          "ups": 114,
          "total_awards_received": 1,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "cf001b4a-f6e9-11e8-b293-0e18d7ddfb6c",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 114,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/Jz3aHQ5OX2btx9oa5dsUq1f52cF8iF8PEUu9yC5Hr1Q.jpg",
          "edited": false,
          "author_flair_css_class": "two-rueleoflight",
          "author_flair_richtext": [
            {
              "a": ":ruele_of_light:",
              "e": "emoji",
              "u": "https://emoji.redditmedia.com/rdwywpqzd1s51_t5_nrn6j/ruele_of_light"
            }
          ],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652365207,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/01b9y39wz1z81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/01b9y39wz1z81.jpg?auto=webp&amp;s=2dcec10b517e1ee34cf4f1992fdf912907faaaeb",
                  "width": 3577,
                  "height": 3872
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/01b9y39wz1z81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=0494c3fb6ad47a149f405601497639ac45cc633f",
                    "width": 108,
                    "height": 116
                  },
                  {
                    "url": "https://preview.redd.it/01b9y39wz1z81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=c2d0e07f1b0e2866bd312bebdb5444316b414eee",
                    "width": 216,
                    "height": 233
                  },
                  {
                    "url": "https://preview.redd.it/01b9y39wz1z81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=76fbc37217d561b7f555b5bfa569d2676824f089",
                    "width": 320,
                    "height": 346
                  },
                  {
                    "url": "https://preview.redd.it/01b9y39wz1z81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=a4fc1b9d28cc681f164e0f770b34217631e76c54",
                    "width": 640,
                    "height": 692
                  },
                  {
                    "url": "https://preview.redd.it/01b9y39wz1z81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=154e4722d2128067b4311e5b9781f1900b901df0",
                    "width": 960,
                    "height": 1039
                  },
                  {
                    "url": "https://preview.redd.it/01b9y39wz1z81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=b11293c1ac33a6f85a737fb4f3ce8ca882320793",
                    "width": 1080,
                    "height": 1169
                  }
                ],
                "variants": {},
                "id": "AdeN6zoNW8X6OLJTdo7VZ-bczgst4KVlvbFiBYvT7Oo"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": ":ruele_of_light:",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uo26lm",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Vedelith",
          "discussion_type": null,
          "num_comments": 13,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/uo26lm/tried_my_hand_at_drawing_aria/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/01b9y39wz1z81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652365207,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_113mgb",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Day 3 of a character a day using an AI.",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_uo0e3w",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.93,
          "author_flair_background_color": null,
          "ups": 287,
          "total_awards_received": 2,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 287,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/bcC6wF8ZlflNQMmzFmAAqSDeaDigP9j_sBMgr7e0xIs.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {
            "gid_1": 2
          },
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652360028,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/d1w8yep6l1z81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/d1w8yep6l1z81.jpg?auto=webp&amp;s=211c3a85a202f541b40523e5bcddbb97dfe566bb",
                  "width": 1080,
                  "height": 1920
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/d1w8yep6l1z81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=6547cf8dedb7afd0c4e7133164b936e21ee1e20c",
                    "width": 108,
                    "height": 192
                  },
                  {
                    "url": "https://preview.redd.it/d1w8yep6l1z81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=4b6b7df8bc94db6af5d31e0498899f6e085e9812",
                    "width": 216,
                    "height": 384
                  },
                  {
                    "url": "https://preview.redd.it/d1w8yep6l1z81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=06693de8f95957b908ad3db7738b5552f01b71ca",
                    "width": 320,
                    "height": 568
                  },
                  {
                    "url": "https://preview.redd.it/d1w8yep6l1z81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=99e19e8fab417a3d1133d2e7ba4933dfbc61acdb",
                    "width": 640,
                    "height": 1137
                  },
                  {
                    "url": "https://preview.redd.it/d1w8yep6l1z81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=f421acb4d135cd85f6ed1ab32957ca3aed7bff56",
                    "width": 960,
                    "height": 1706
                  },
                  {
                    "url": "https://preview.redd.it/d1w8yep6l1z81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=07150215eff70c43c1c2f76a27c1347f47740a6b",
                    "width": 1080,
                    "height": 1920
                  }
                ],
                "variants": {},
                "id": "7rsynLi-hVRn4wzXM9FFGAXT5TX3tKlFoKEmYfDejSI"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 100,
              "id": "gid_1",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 512,
              "static_icon_width": 512,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Shows the Silver Award... and that's it.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 2,
              "static_icon_height": 512,
              "name": "Silver",
              "resized_static_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 512,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "uo0e3w",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "ScumCommander",
          "discussion_type": null,
          "num_comments": 32,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/uo0e3w/day_3_of_a_character_a_day_using_an_ai/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/d1w8yep6l1z81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652360028,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_mx5r3v0c",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Top Model Luluca [Art by Me]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 86,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_unylb7",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.96,
          "author_flair_background_color": null,
          "ups": 246,
          "total_awards_received": 1,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 246,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/Dyoy077mFyBXv5kIaGwAVi827ksB4LTvkGVYma55EZw.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652353764,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/hsdk7ofi21z81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/hsdk7ofi21z81.jpg?auto=webp&amp;s=b578e258255e829087121d51bd3d5fa55963a287",
                  "width": 1280,
                  "height": 791
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/hsdk7ofi21z81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=aaea25f52763c2a9e3a2ca1fead016f9d1199c65",
                    "width": 108,
                    "height": 66
                  },
                  {
                    "url": "https://preview.redd.it/hsdk7ofi21z81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=cbe0ffa7f02ede4e5ade5ca9688c31ea74d784b9",
                    "width": 216,
                    "height": 133
                  },
                  {
                    "url": "https://preview.redd.it/hsdk7ofi21z81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=c8240fd54ce715984322e46c3b347bf27d964bce",
                    "width": 320,
                    "height": 197
                  },
                  {
                    "url": "https://preview.redd.it/hsdk7ofi21z81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=4abd5f7a0e054331e3440d40b000175ad9d4f540",
                    "width": 640,
                    "height": 395
                  },
                  {
                    "url": "https://preview.redd.it/hsdk7ofi21z81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=56215799aa3771ca79e500b176f7ec9b82f40417",
                    "width": 960,
                    "height": 593
                  },
                  {
                    "url": "https://preview.redd.it/hsdk7ofi21z81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=084643c85f48f439a7164bdcc00ed42652a13c26",
                    "width": 1080,
                    "height": 667
                  }
                ],
                "variants": {},
                "id": "03MjTTnrWI_9WZm8RJQuTTcFEgVnaDGF05Cw5dF7oEQ"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "unylb7",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Shu0ne",
          "discussion_type": null,
          "num_comments": 3,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/unylb7/top_model_luluca_art_by_me/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/hsdk7ofi21z81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652353764,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_3jahmeyk",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Aria Artwork. I really tried to find a way to put the Shadow penguin someone asked earlier but it was too big T^T Very Sorry!",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_unwwfc",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.95,
          "author_flair_background_color": null,
          "ups": 472,
          "total_awards_received": 2,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 472,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "nsfw",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652346544,
          "link_flair_type": "richtext",
          "wls": 3,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/7c588dnrg0z81.png",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": true,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/7c588dnrg0z81.png?auto=webp&amp;s=ea349b96200081f7a52ffcdedca499604bf3c91d",
                  "width": 900,
                  "height": 1337
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/7c588dnrg0z81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=41f8eb2f671e8f894931c01937d8928cd0583ecc",
                    "width": 108,
                    "height": 160
                  },
                  {
                    "url": "https://preview.redd.it/7c588dnrg0z81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=4100b9885f1de3321c72502291625e4384f9ac89",
                    "width": 216,
                    "height": 320
                  },
                  {
                    "url": "https://preview.redd.it/7c588dnrg0z81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=de9626c00510e0c9e174c273ea6f2b036da576f4",
                    "width": 320,
                    "height": 475
                  },
                  {
                    "url": "https://preview.redd.it/7c588dnrg0z81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=6acd97c666ff8382b833f6ec5530ee18a0bd745a",
                    "width": 640,
                    "height": 950
                  }
                ],
                "variants": {
                  "obfuscated": {
                    "source": {
                      "url": "https://preview.redd.it/7c588dnrg0z81.png?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=012488aa6e67d82bc311858c503166e14ff66793",
                      "width": 900,
                      "height": 1337
                    },
                    "resolutions": [
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=7840b814ff637f222ac549844bc9aa4e482aa788",
                        "width": 108,
                        "height": 160
                      },
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=4ee47db77f3249d69953fd79ef6c4d09c1a2be3d",
                        "width": 216,
                        "height": 320
                      },
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=ce9e866d76099c00180cdff087c8a9f719bcc77d",
                        "width": 320,
                        "height": 475
                      },
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=32587fed9291da11c71e8fabe14d08c9cef0e3b1",
                        "width": 640,
                        "height": 950
                      }
                    ]
                  },
                  "nsfw": {
                    "source": {
                      "url": "https://preview.redd.it/7c588dnrg0z81.png?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=012488aa6e67d82bc311858c503166e14ff66793",
                      "width": 900,
                      "height": 1337
                    },
                    "resolutions": [
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=7840b814ff637f222ac549844bc9aa4e482aa788",
                        "width": 108,
                        "height": 160
                      },
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=4ee47db77f3249d69953fd79ef6c4d09c1a2be3d",
                        "width": 216,
                        "height": 320
                      },
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=ce9e866d76099c00180cdff087c8a9f719bcc77d",
                        "width": 320,
                        "height": 475
                      },
                      {
                        "url": "https://preview.redd.it/7c588dnrg0z81.png?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=32587fed9291da11c71e8fabe14d08c9cef0e3b1",
                        "width": 640,
                        "height": 950
                      }
                    ]
                  }
                },
                "id": "VRAdu199vGTTo_6iu4J0AOpGX0StGltfZsSg09YUKZg"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 2,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "unwwfc",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Traditional_Suit5385",
          "discussion_type": null,
          "num_comments": 5,
          "send_replies": true,
          "whitelist_status": "promo_adult_nsfw",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/unwwfc/aria_artwork_i_really_tried_to_find_a_way_to_put/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/7c588dnrg0z81.png",
          "subreddit_subscribers": 142702,
          "created_utc": 1652346544,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_113mgb",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Day 2 of a piece a day until I decide to stop.",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_unperp",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.93,
          "author_flair_background_color": null,
          "ups": 261,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 261,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/SF1p9UmNRCd1x0D_EeQidGbFAAbxynwWD9KJtgn18L8.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652319175,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/omeva0rp7yy81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/omeva0rp7yy81.jpg?auto=webp&amp;s=a07a5d544f0608ee9c64318f51b0dffd22cc9664",
                  "width": 1080,
                  "height": 1920
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/omeva0rp7yy81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=51b8fbd6bab8faeae28b9641d87e2337d6265751",
                    "width": 108,
                    "height": 192
                  },
                  {
                    "url": "https://preview.redd.it/omeva0rp7yy81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=86a329194cabc8dee2c2ebd8151d3958b845bc9c",
                    "width": 216,
                    "height": 384
                  },
                  {
                    "url": "https://preview.redd.it/omeva0rp7yy81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=00159e424a54a4a765220dba2492d1bfe3b4ce21",
                    "width": 320,
                    "height": 568
                  },
                  {
                    "url": "https://preview.redd.it/omeva0rp7yy81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=d7c2cc04f8369be8b9f8ccd242b1d3f04056ea51",
                    "width": 640,
                    "height": 1137
                  },
                  {
                    "url": "https://preview.redd.it/omeva0rp7yy81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=b2daeeca818ea96c143d1c736911401a22fba401",
                    "width": 960,
                    "height": 1706
                  },
                  {
                    "url": "https://preview.redd.it/omeva0rp7yy81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=889a189059b97aff5e0caed37048d2c26ac94073",
                    "width": 1080,
                    "height": 1920
                  }
                ],
                "variants": {},
                "id": "K_9IuJ3TO8gonjcnesFZEgqIUaCfeLo3T3oGdt_Gg5A"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "unperp",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "ScumCommander",
          "discussion_type": null,
          "num_comments": 31,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/unperp/day_2_of_a_piece_a_day_until_i_decide_to_stop/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/omeva0rp7yy81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652319175,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_9plpe7qv",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Aramintha at the annual Merchant Leaders Gala",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 96,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_unjl4l",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.94,
          "author_flair_background_color": "#d3d6da",
          "ups": 156,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "d25a8d5c-f6e9-11e8-a560-0eeed47070fa",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 156,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/-L1nWUoS3to4r4Ef6JCB6vkKddK5T7v6XZBeOfj0zjs.jpg",
          "edited": false,
          "author_flair_css_class": "two-schuri ",
          "author_flair_richtext": [
            {
              "a": ":schuri:",
              "e": "emoji",
              "u": "https://emoji.redditmedia.com/t9y6osuzd1s51_t5_nrn6j/schuri"
            }
          ],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652302097,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/7e1k897tswy81.png",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/7e1k897tswy81.png?auto=webp&amp;s=023527c91fd726e242cd2a020cc8b2eab0f14dc5",
                  "width": 1417,
                  "height": 975
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/7e1k897tswy81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=0197ac49d3ea468cbc2d0c7f475cb9ce6ccfc612",
                    "width": 108,
                    "height": 74
                  },
                  {
                    "url": "https://preview.redd.it/7e1k897tswy81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=53cadf3334be6f3e8c7350725cd459fb2844a954",
                    "width": 216,
                    "height": 148
                  },
                  {
                    "url": "https://preview.redd.it/7e1k897tswy81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=300cb02f9edaf021c4742d3d0b9d24b368da4d92",
                    "width": 320,
                    "height": 220
                  },
                  {
                    "url": "https://preview.redd.it/7e1k897tswy81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=3350785e7c2b74d625177a57e2d141e75ec2fd1b",
                    "width": 640,
                    "height": 440
                  },
                  {
                    "url": "https://preview.redd.it/7e1k897tswy81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=2e69960aa1c17fea4089289a956156c8d19877fe",
                    "width": 960,
                    "height": 660
                  },
                  {
                    "url": "https://preview.redd.it/7e1k897tswy81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=e100a8ae0c15420dbc670e8e4533380607b60fbc",
                    "width": 1080,
                    "height": 743
                  }
                ],
                "variants": {},
                "id": "52eJ3Tv7jbwMSZ89EE4p8pNXcrDwbOwJM1a0XQCROoo"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": ":schuri:",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "unjl4l",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "sertralinehydrochlor",
          "discussion_type": null,
          "num_comments": 5,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/unjl4l/aramintha_at_the_annual_merchant_leaders_gala/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/7e1k897tswy81.png",
          "subreddit_subscribers": 142702,
          "created_utc": 1652302097,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_pf62m",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Inferno Khawazu [art by epic7ysg]",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 98,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_unavdb",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.98,
          "author_flair_background_color": null,
          "ups": 520,
          "total_awards_received": 1,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 520,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/0FHC_SkmMUqecdKNh06SbAQwMHDjTXclAid7iR01aQM.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {
            "gid_1": 1
          },
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652278607,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/aykb5602vuy81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/aykb5602vuy81.jpg?auto=webp&amp;s=b297acca88bdafc9da49996529fd52d9d5bac9f1",
                  "width": 1000,
                  "height": 707
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/aykb5602vuy81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=897a3c0534b6864ce4a8e0b539e1da8b5ff38665",
                    "width": 108,
                    "height": 76
                  },
                  {
                    "url": "https://preview.redd.it/aykb5602vuy81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=4b41b3ba4ae0553dd35d2024ecff35924b5ee872",
                    "width": 216,
                    "height": 152
                  },
                  {
                    "url": "https://preview.redd.it/aykb5602vuy81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=0c00f75b4f99d87195839bf5bc291e4634595fcd",
                    "width": 320,
                    "height": 226
                  },
                  {
                    "url": "https://preview.redd.it/aykb5602vuy81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=6850b4e432eed9a911d1f3c380ff726cba50da42",
                    "width": 640,
                    "height": 452
                  },
                  {
                    "url": "https://preview.redd.it/aykb5602vuy81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=9709e864059019f332d20ad5900afc775e305f91",
                    "width": 960,
                    "height": 678
                  }
                ],
                "variants": {},
                "id": "3I8czqD0PnhqFSV-EWYCa1c14wkmN5Cy6N37cOFXQ80"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 100,
              "id": "gid_1",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 512,
              "static_icon_width": 512,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Shows the Silver Award... and that's it.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 1,
              "static_icon_height": 512,
              "name": "Silver",
              "resized_static_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 512,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "unavdb",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Triple_S_Rank",
          "discussion_type": null,
          "num_comments": 13,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/unavdb/inferno_khawazu_art_by_epic7ysg/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/aykb5602vuy81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652278607,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_kdmbe",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Lionheart Cermia by Huge",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 115,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_unaldl",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.99,
          "author_flair_background_color": null,
          "ups": 1394,
          "total_awards_received": 9,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": null,
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 1394,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://b.thumbs.redditmedia.com/v2nqeF7GKi4zn4wV-62UqnpOxK7m2kfDve_jHagVxiE.jpg",
          "edited": false,
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "gildings": {
            "gid_1": 2
          },
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652277812,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "text",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/tf9isgzhsuy81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/tf9isgzhsuy81.jpg?auto=webp&amp;s=c72fc56cb80407a78a112ba3cde5d1d066b1312c",
                  "width": 1200,
                  "height": 988
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/tf9isgzhsuy81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=80c8ecc8595fe5c6f2f1b786ba85a3a9a61d9353",
                    "width": 108,
                    "height": 88
                  },
                  {
                    "url": "https://preview.redd.it/tf9isgzhsuy81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=0a999627fba166db1922360e2be8ffb1a4798843",
                    "width": 216,
                    "height": 177
                  },
                  {
                    "url": "https://preview.redd.it/tf9isgzhsuy81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=55e00b038230ce5d1c50ede667ba3da6053ed8eb",
                    "width": 320,
                    "height": 263
                  },
                  {
                    "url": "https://preview.redd.it/tf9isgzhsuy81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=6ba1c8eac85b0d52c118665114f8283bd8600798",
                    "width": 640,
                    "height": 526
                  },
                  {
                    "url": "https://preview.redd.it/tf9isgzhsuy81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=a10533f336f1a12dfe6cf04fd6c815b95bb1167d",
                    "width": 960,
                    "height": 790
                  },
                  {
                    "url": "https://preview.redd.it/tf9isgzhsuy81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=ad6de7c6c58eb500712f075961342e156a5e6679",
                    "width": 1080,
                    "height": 889
                  }
                ],
                "variants": {},
                "id": "9uwcWEtxE8n2xWT1-Rfj0GkVZ9x6ZdoLY0ZJg4zaAZQ"
              }
            ],
            "enabled": true
          },
          "all_awardings": [
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 100,
              "id": "gid_1",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 512,
              "static_icon_width": 512,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Shows the Silver Award... and that's it.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 2,
              "static_icon_height": 512,
              "name": "Silver",
              "resized_static_icons": [
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_16.png",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_32.png",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_48.png",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_64.png",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://www.redditstatic.com/gold/awards/icon/silver_128.png",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 512,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://www.redditstatic.com/gold/awards/icon/silver_512.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 150,
              "id": "award_f44611f1-b89e-46dc-97fe-892280b13b82",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "Thank you stranger. Shows the award.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 4,
              "static_icon_height": 2048,
              "name": "Helpful",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=16&amp;height=16&amp;auto=webp&amp;s=a5662dfbdb402bf67866c050aa76c31c147c2f45",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=32&amp;height=32&amp;auto=webp&amp;s=a6882eb3f380e8e88009789f4d0072e17b8c59f1",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=48&amp;height=48&amp;auto=webp&amp;s=e50064b090879e8a0b55e433f6ee61d5cb5fbe1d",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=64&amp;height=64&amp;auto=webp&amp;s=8e5bb2e76683cb6b161830bcdd9642049d6adc11",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png?width=128&amp;height=128&amp;auto=webp&amp;s=eda4a9246f95f42ee6940cc0ec65306fd20de878",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/klvxk1wggfd41_Helpful.png"
            },
            {
              "giver_coin_reward": null,
              "subreddit_id": null,
              "is_new": false,
              "days_of_drip_extension": null,
              "coin_price": 125,
              "id": "award_5f123e3d-4f48-42f4-9c11-e98b566d5897",
              "penny_donate": null,
              "award_sub_type": "GLOBAL",
              "coin_reward": 0,
              "icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png",
              "days_of_premium": null,
              "tiers_by_required_awardings": null,
              "resized_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_width": 2048,
              "static_icon_width": 2048,
              "start_date": null,
              "is_enabled": true,
              "awardings_required_to_grant_benefits": null,
              "description": "When you come across a feel-good thing.",
              "end_date": null,
              "subreddit_coin_reward": 0,
              "count": 3,
              "static_icon_height": 2048,
              "name": "Wholesome",
              "resized_static_icons": [
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=16&amp;height=16&amp;auto=webp&amp;s=92932f465d58e4c16b12b6eac4ca07d27e3d11c0",
                  "width": 16,
                  "height": 16
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=32&amp;height=32&amp;auto=webp&amp;s=d11484a208d68a318bf9d4fcf371171a1cb6a7ef",
                  "width": 32,
                  "height": 32
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=48&amp;height=48&amp;auto=webp&amp;s=febdf28b6f39f7da7eb1365325b85e0bb49a9f63",
                  "width": 48,
                  "height": 48
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=64&amp;height=64&amp;auto=webp&amp;s=b4406a2d88bf86fa3dc8a45aacf7e0c7bdccc4fb",
                  "width": 64,
                  "height": 64
                },
                {
                  "url": "https://preview.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png?width=128&amp;height=128&amp;auto=webp&amp;s=19555b13e3e196b62eeb9160d1ac1d1b372dcb0b",
                  "width": 128,
                  "height": 128
                }
              ],
              "icon_format": null,
              "icon_height": 2048,
              "penny_price": null,
              "award_type": "global",
              "static_icon_url": "https://i.redd.it/award_images/t5_22cerq/5izbv4fn0md41_Wholesome.png"
            }
          ],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": null,
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "unaldl",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "asaness",
          "discussion_type": null,
          "num_comments": 13,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": null,
          "permalink": "/r/EpicSeven/comments/unaldl/lionheart_cermia_by_huge/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/tf9isgzhsuy81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652277812,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_bzvdn62",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "title": "Aria WIP OC Fanart",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "name": "t3_un9a1n",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.77,
          "author_flair_background_color": "#dadada",
          "ups": 28,
          "total_awards_received": 0,
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "c9afcf56-2562-11eb-bdf1-0e2901ef8e9b",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": true,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 28,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/X-h_oybz_1_lIw_ooMPARf9WhFQXwA62YZcITyeYbr4.jpg",
          "edited": false,
          "author_flair_css_class": "four-CommandModelLaika",
          "author_flair_richtext": [
            {
              "a": ":CommandModelLaika:",
              "e": "emoji",
              "u": "https://emoji.redditmedia.com/h2oo8o4acxy51_t5_nrn6j/CommandModelLaika"
            }
          ],
          "gildings": {},
          "post_hint": "image",
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652273961,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "domain": "i.redd.it",
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://i.redd.it/57dqiir9huy81.jpg",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "preview": {
            "images": [
              {
                "source": {
                  "url": "https://preview.redd.it/57dqiir9huy81.jpg?auto=webp&amp;s=02729ae591324684e6f681093f6a1e063e207b87",
                  "width": 1080,
                  "height": 1137
                },
                "resolutions": [
                  {
                    "url": "https://preview.redd.it/57dqiir9huy81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=7949c0089f7600e8eff42981ee438f7fe6c7e076",
                    "width": 108,
                    "height": 113
                  },
                  {
                    "url": "https://preview.redd.it/57dqiir9huy81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=2f8dbb8e5e62bbbe418bc26c4fde3bdbdcb2f6ef",
                    "width": 216,
                    "height": 227
                  },
                  {
                    "url": "https://preview.redd.it/57dqiir9huy81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=d9417442c3128104121461948abb854dc6371f6f",
                    "width": 320,
                    "height": 336
                  },
                  {
                    "url": "https://preview.redd.it/57dqiir9huy81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=ea6dd4a63da23afae57943e4d0d405df00764c37",
                    "width": 640,
                    "height": 673
                  },
                  {
                    "url": "https://preview.redd.it/57dqiir9huy81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=b604e9880e406756df916433d9be516b95f08a47",
                    "width": 960,
                    "height": 1010
                  },
                  {
                    "url": "https://preview.redd.it/57dqiir9huy81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=5fdb6811ce73e6346765ce3b569aaa799bf8acad",
                    "width": 1080,
                    "height": 1137
                  }
                ],
                "variants": {},
                "id": "W1NEWfU4TyhnXNnmRsyrYrzYE9s14tRjFddPY_2sXIY"
              }
            ],
            "enabled": true
          },
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": ":CommandModelLaika:",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "un9a1n",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Alipopz",
          "discussion_type": null,
          "num_comments": 4,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/un9a1n/aria_wip_oc_fanart/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://i.redd.it/57dqiir9huy81.jpg",
          "subreddit_subscribers": 142702,
          "created_utc": 1652273961,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      },
      {
        "kind": "t3",
        "data": {
          "approved_at_utc": null,
          "subreddit": "EpicSeven",
          "selftext": "",
          "author_fullname": "t2_4ho7zgiz",
          "saved": false,
          "mod_reason_title": null,
          "gilded": 0,
          "clicked": false,
          "is_gallery": true,
          "title": "Magical Girl Diene Minimalist",
          "link_flair_richtext": [
            {
              "e": "text",
              "t": "Art"
            }
          ],
          "subreddit_name_prefixed": "r/EpicSeven",
          "hidden": false,
          "pwls": 7,
          "link_flair_css_class": "Art",
          "downs": 0,
          "thumbnail_height": 140,
          "top_awarded_type": null,
          "hide_score": false,
          "media_metadata": {
            "vm0hdhiqgty81": {
              "status": "valid",
              "e": "Image",
              "m": "image/png",
              "p": [
                {
                  "y": 162,
                  "x": 108,
                  "u": "https://preview.redd.it/vm0hdhiqgty81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=ace94d715f320582126a77a7d61171c3e94324f2"
                },
                {
                  "y": 324,
                  "x": 216,
                  "u": "https://preview.redd.it/vm0hdhiqgty81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=80e00fc992910dbacbc5c02528a827970f815a3c"
                },
                {
                  "y": 480,
                  "x": 320,
                  "u": "https://preview.redd.it/vm0hdhiqgty81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=2ffa89bad1a627e3705b49c0dabdb2f275dcee47"
                },
                {
                  "y": 960,
                  "x": 640,
                  "u": "https://preview.redd.it/vm0hdhiqgty81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=d8cd2633ca547c6725aa9f5b62beae4832dae283"
                },
                {
                  "y": 1440,
                  "x": 960,
                  "u": "https://preview.redd.it/vm0hdhiqgty81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=65325611f347c6c74bfc614b6315fae363e1aa4f"
                },
                {
                  "y": 1620,
                  "x": 1080,
                  "u": "https://preview.redd.it/vm0hdhiqgty81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=a82a3c61f2e920a01d32aadf2a6de2bf33518849"
                }
              ],
              "s": {
                "y": 3000,
                "x": 2000,
                "u": "https://preview.redd.it/vm0hdhiqgty81.png?width=2000&amp;format=png&amp;auto=webp&amp;s=6261249e18cee3508090740f35eced5f6704750e"
              },
              "id": "vm0hdhiqgty81"
            },
            "rcnphtvqgty81": {
              "status": "valid",
              "e": "Image",
              "m": "image/png",
              "p": [
                {
                  "y": 60,
                  "x": 108,
                  "u": "https://preview.redd.it/rcnphtvqgty81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=131844fafe05390bc63c365cf805385347381ea9"
                },
                {
                  "y": 121,
                  "x": 216,
                  "u": "https://preview.redd.it/rcnphtvqgty81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=954ee9d53a4e6188eebf19721b21bff9e0091ce8"
                },
                {
                  "y": 180,
                  "x": 320,
                  "u": "https://preview.redd.it/rcnphtvqgty81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=a59d904bbf2bf6b699beca1d30dcbc4d96d420e4"
                },
                {
                  "y": 360,
                  "x": 640,
                  "u": "https://preview.redd.it/rcnphtvqgty81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=0aaac316e4c7932690fd6de057b0c520171a3e42"
                },
                {
                  "y": 540,
                  "x": 960,
                  "u": "https://preview.redd.it/rcnphtvqgty81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=a60faffe59f9ade2290ef8f8e2da6f39db586ef5"
                },
                {
                  "y": 607,
                  "x": 1080,
                  "u": "https://preview.redd.it/rcnphtvqgty81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=77f9e76e3d85d6e6fc7181b1974c48fef34f23b6"
                }
              ],
              "s": {
                "y": 3000,
                "x": 5333,
                "u": "https://preview.redd.it/rcnphtvqgty81.png?width=5333&amp;format=png&amp;auto=webp&amp;s=ba0ec7224e0ce8ac285b18d84e26c34581bf5bf1"
              },
              "id": "rcnphtvqgty81"
            }
          },
          "name": "t3_un60fl",
          "quarantine": false,
          "link_flair_text_color": "light",
          "upvote_ratio": 0.95,
          "author_flair_background_color": "#d3d6da",
          "ups": 145,
          "domain": "reddit.com",
          "media_embed": {},
          "thumbnail_width": 140,
          "author_flair_template_id": "2043855e-3011-11e9-9d87-0e14669d6cae",
          "is_original_content": false,
          "user_reports": [],
          "secure_media": null,
          "is_reddit_media_domain": false,
          "is_meta": false,
          "category": null,
          "secure_media_embed": {},
          "gallery_data": {
            "items": [
              {
                "media_id": "vm0hdhiqgty81",
                "id": 141042478
              },
              {
                "media_id": "rcnphtvqgty81",
                "id": 141042479
              }
            ]
          },
          "link_flair_text": "Art",
          "can_mod_post": false,
          "score": 145,
          "approved_by": null,
          "is_created_from_ads_ui": false,
          "author_premium": false,
          "thumbnail": "https://a.thumbs.redditmedia.com/hjY4R8wIqnsIVH6EEBxqHlZBuYWg2obNPMLixEps6B0.jpg",
          "edited": false,
          "author_flair_css_class": "two-vildred",
          "author_flair_richtext": [
            {
              "a": ":vildred:",
              "e": "emoji",
              "u": "https://emoji.redditmedia.com/cchlwn31e1s51_t5_nrn6j/vildred"
            },
            {
              "a": ":Roana:",
              "e": "emoji",
              "u": "https://emoji.redditmedia.com/nwef15kbcxy51_t5_nrn6j/Roana"
            }
          ],
          "gildings": {},
          "content_categories": null,
          "is_self": false,
          "subreddit_type": "public",
          "created": 1652261772,
          "link_flair_type": "richtext",
          "wls": 7,
          "removed_by_category": null,
          "banned_by": null,
          "author_flair_type": "richtext",
          "total_awards_received": 0,
          "allow_live_comments": false,
          "selftext_html": null,
          "likes": null,
          "suggested_sort": null,
          "banned_at_utc": null,
          "url_overridden_by_dest": "https://www.reddit.com/gallery/un60fl",
          "view_count": null,
          "archived": false,
          "no_follow": false,
          "is_crosspostable": true,
          "pinned": false,
          "over_18": false,
          "all_awardings": [],
          "awarders": [],
          "media_only": false,
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "can_gild": true,
          "spoiler": false,
          "locked": false,
          "author_flair_text": ":vildred::Roana:",
          "treatment_tags": [],
          "visited": false,
          "removed_by": null,
          "mod_note": null,
          "distinguished": null,
          "subreddit_id": "t5_nrn6j",
          "author_is_blocked": false,
          "mod_reason_by": null,
          "num_reports": null,
          "removal_reason": null,
          "link_flair_background_color": "#b56e38",
          "id": "un60fl",
          "is_robot_indexable": true,
          "report_reasons": null,
          "author": "Jamadoh",
          "discussion_type": null,
          "num_comments": 0,
          "send_replies": true,
          "whitelist_status": "some_ads",
          "contest_mode": false,
          "mod_reports": [],
          "author_patreon_flair": false,
          "author_flair_text_color": "dark",
          "permalink": "/r/EpicSeven/comments/un60fl/magical_girl_diene_minimalist/",
          "parent_whitelist_status": "some_ads",
          "stickied": false,
          "url": "https://www.reddit.com/gallery/un60fl",
          "subreddit_subscribers": 142702,
          "created_utc": 1652261772,
          "num_crossposts": 0,
          "media": null,
          "is_video": false
        }
      }
    ],
    "before": null
  }
};