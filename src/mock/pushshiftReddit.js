export const PUSHSHIFT_REDDIT_MOCK_DATA = {
  "data": [
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Axel2222222222",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_1mh57nh7",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652773266,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urh74s/my_first_ever_5ml_hero_from_summoning_i_havent/",
          "gildings": {},
          "id": "urh74s",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urh74s/my_first_ever_5ml_hero_from_summoning_i_havent/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "zk7rNxOfGvd7DI_IOlxSYl0u45NpD94_38ZpC2Eu6ks",
                      "resolutions": [
                          {
                              "height": 59,
                              "url": "https://preview.redd.it/iyfv4jptpzz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=eb77e4e31f4d5c63a5a732977753b33efabb6994",
                              "width": 108
                          },
                          {
                              "height": 118,
                              "url": "https://preview.redd.it/iyfv4jptpzz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=5342235e284b0c6f34ca92c10acd9e9ee83572c2",
                              "width": 216
                          },
                          {
                              "height": 175,
                              "url": "https://preview.redd.it/iyfv4jptpzz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=0d1233706337174381f514b789dfd4ee8ecefc57",
                              "width": 320
                          },
                          {
                              "height": 350,
                              "url": "https://preview.redd.it/iyfv4jptpzz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=48bb85c3fc451ee28ed39cc313495b40a65634c3",
                              "width": 640
                          },
                          {
                              "height": 525,
                              "url": "https://preview.redd.it/iyfv4jptpzz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=9d4ec52d54fe3f944ce81902fa5959da9abc4aab",
                              "width": 960
                          },
                          {
                              "height": 590,
                              "url": "https://preview.redd.it/iyfv4jptpzz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=926da1b498ede23c08d5d0fcee0adaf6ffacceaa",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 888,
                          "url": "https://preview.redd.it/iyfv4jptpzz81.png?auto=webp&amp;s=3acfbe07284023ae9cdabaa95ae25e5aca9fb428",
                          "width": 1623
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652773276,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142753,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/Z9VsjnaOj3RGkV9H_LYkP9ZGYXhxMwgCu9C_eD4-cUA.jpg",
          "thumbnail_height": 76,
          "thumbnail_width": 140,
          "title": "My first ever 5*ML hero from summoning. I haven't even used him yet but I love him.",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/iyfv4jptpzz81.png",
          "url_overridden_by_dest": "https://i.redd.it/iyfv4jptpzz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Flowers_39",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_eae7pswn",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652772471,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urh0lp/dont_forget_your_guild_weeklies/",
          "gildings": {},
          "id": "urh0lp",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urh0lp/dont_forget_your_guild_weeklies/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "Btl35z5R9CPj1fcf09wvaj9dpuGiQWZzSG9pwv7JnCQ",
                      "resolutions": [
                          {
                              "height": 84,
                              "url": "https://preview.redd.it/boz4lx7lnzz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=d1acc77bea198fd8c37f8040b786530a0c3231f8",
                              "width": 108
                          },
                          {
                              "height": 168,
                              "url": "https://preview.redd.it/boz4lx7lnzz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=d566c6e06b7c51133b4e8e7ac4c3e8dc55c219d9",
                              "width": 216
                          },
                          {
                              "height": 249,
                              "url": "https://preview.redd.it/boz4lx7lnzz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=54474af26e0beed6dce7b9020d389fc8f37c5606",
                              "width": 320
                          },
                          {
                              "height": 499,
                              "url": "https://preview.redd.it/boz4lx7lnzz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=1e9fe21f6caaf940df8d1c80b36c024f7fe9fdb5",
                              "width": 640
                          },
                          {
                              "height": 749,
                              "url": "https://preview.redd.it/boz4lx7lnzz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=5be329addbdcd79327bd14f8c54178fb0d9f381c",
                              "width": 960
                          },
                          {
                              "height": 842,
                              "url": "https://preview.redd.it/boz4lx7lnzz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=9ce6d74d01408fb52f5b6a4d2845eea1aa734042",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/boz4lx7lnzz81.png?auto=webp&amp;s=bcad3e7de228fc89011f7580d79a55eff886e96f",
                          "width": 1384
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652772482,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142752,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/bGA1KMqhjN7gBQ-lDZJZHiZvPYfIE9q0HsGodUopNXs.jpg",
          "thumbnail_height": 109,
          "thumbnail_width": 140,
          "title": "Don't forget your Guild weeklies",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/boz4lx7lnzz81.png",
          "url_overridden_by_dest": "https://i.redd.it/boz4lx7lnzz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Efflctim",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_84re8ueq",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652768253,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urg1o8/should_i_keep_pulling_in_hopes_i_get_lucky_or_cut/",
          "gildings": {},
          "id": "urg1o8",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urg1o8/should_i_keep_pulling_in_hopes_i_get_lucky_or_cut/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "OkzpMZ6NTvSJ19UUinUCgfuP4se91Fa-lOH-7jtkpk0",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/j666adw1bzz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=697e723f5b72763ee62fd0e3740084946980d029",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/j666adw1bzz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=d0dee10ce72b74d4e7d820425c44c137d062d268",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/j666adw1bzz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=e484ba9a1769a8585736f9213149d3a73e467a16",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/j666adw1bzz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=9a5c818f1cb133288b829343eec765a2847beadc",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/j666adw1bzz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=e48a7889b84ab2d8a584e413faf66539a00ae3dc",
                              "width": 960
                          },
                          {
                              "height": 498,
                              "url": "https://preview.redd.it/j666adw1bzz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=df7baa634fb5dee3b9c5ea1a3c1f457c68638788",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1125,
                          "url": "https://preview.redd.it/j666adw1bzz81.jpg?auto=webp&amp;s=af65ce9a3be3cf17b28ab1b3b1c7c9e44b5fbfb8",
                          "width": 2436
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652768263,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142753,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/NDA7c6NrcR3wcJ8XfRFZOtJk5Y1OMvQp2Sueks1qfnM.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "Should I keep pulling in hopes I get lucky or cut my loses? I\u2019ve wanted BBK for a while but also FFC is FCC so I\u2019m torn",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/j666adw1bzz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/j666adw1bzz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Nows_",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_l6p938pn",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652763832,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ureynr/what_do_you_think/",
          "gildings": {},
          "id": "ureynr",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": false,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ureynr/what_do_you_think/",
          "pinned": false,
          "pwls": 7,
          "removed_by_category": "automod_filtered",
          "retrieved_on": 1652763842,
          "score": 1,
          "selftext": "[removed]",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142751,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "what do you think",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ureynr/what_do_you_think/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "the_zero_achiever",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_61tdztvp",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652763306,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uretly/are_the_big_boss_chapter_villains_still/",
          "gildings": {},
          "id": "uretly",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uretly/are_the_big_boss_chapter_villains_still/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652763317,
          "score": 1,
          "selftext": "Long time no see, E7 community. It has been a long since my last post here. Anyway, I'll go ahead and ask my question here. Belian, Straze, and Archdemon Shadow, are they still a top pick in arena/RTA?   \n\n\nThe release of the custom Mystic banner let me get the unit I previously missed, Straze. I was so happy I bring him in pretty much everywhere. But still, I didn't see him much in the arena or RTA. Heck, the last videos I searched of him on Youtube is 2-3 months ago. And the same applies to Archdemon Shadow. It's pretty rare to see them anywhere. At least, people are still talking about Belian, so that's a good thing.   \n\n\nI thought with the release of the custom Mystic banner, I'll see an increase in the number of their appearances, but I thought wrong. What is your consensus about this? Feel free to discuss.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142751,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Are the big boss Chapter villains still dominating PvP?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uretly/are_the_big_boss_chapter_villains_still/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Buue2",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "four-BriarWitchIseria",
          "author_flair_richtext": [
              {
                  "a": ":BriarWitchIseria:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/q10mm8u9cxy51_t5_nrn6j/BriarWitchIseria"
              }
          ],
          "author_flair_template_id": "96acf4d0-2562-11eb-9c1a-0e8454c2371f",
          "author_flair_text": ":BriarWitchIseria:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_11gnl9",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652762978,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ureqm8/sg_nerfs_are_a_last_resort_also_sg/",
          "gildings": {},
          "id": "ureqm8",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ureqm8/sg_nerfs_are_a_last_resort_also_sg/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "Gh7mxePH-zwR0PvmvOKgnCTIUsTZWHkcqmDJaq4-Taw",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/5tni6838vyz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=dd606f6b8853d9b1f45d9b096b41cba005e621c1",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/5tni6838vyz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=32a53991ff55b1811a0389ba6715f51067040fdb",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/5tni6838vyz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=e76fc0149045f176cb5dbd50749098a3803f70d3",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/5tni6838vyz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=8c3424a0c4ad081d313d96dcf5fa94e116fd9b17",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/5tni6838vyz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=75be90e896d5fb2269c16111a3c1c3a4924af3d8",
                              "width": 960
                          },
                          {
                              "height": 498,
                              "url": "https://preview.redd.it/5tni6838vyz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=2d5c7ecb1b8915ce4d9efe85494a35348d4da449",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/5tni6838vyz81.png?auto=webp&amp;s=2509b2edcd02d1f4059f62a9e4eb0900b0cc9d23",
                          "width": 2340
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652762988,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142751,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/pnAcHW-3Du1QC21oI6rTyiXKTp9yQ_2hw-KVBN9gsFM.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "SG: \"Nerfs are a last resort\", Also SG:",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/5tni6838vyz81.png",
          "url_overridden_by_dest": "https://i.redd.it/5tni6838vyz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "nathan_inugami",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_590bb5xk",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652762352,
          "domain": "youtu.be",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urekx9/senya_ml_cermia_the_dynamic_duo/",
          "gildings": {},
          "id": "urekx9",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media": {
              "oembed": {
                  "author_name": "[lel]Nathan",
                  "author_url": "https://www.youtube.com/channel/UCttgr_XnakRcHFF5LOWDtZw",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/oWWhOFr9hlk?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/oWWhOFr9hlk/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "[E7 RTA] senya + ml cermia (the dynamic duo)",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/oWWhOFr9hlk?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "scrolling": false,
              "width": 356
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urekx9/senya_ml_cermia_the_dynamic_duo/",
          "pinned": false,
          "post_hint": "rich:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "NbjNSFvDTlY0Fq4pr1AgdVOkiPry2w3ncU_8z1F0cyY",
                      "resolutions": [
                          {
                              "height": 81,
                              "url": "https://external-preview.redd.it/XhYShPAHGC1U9qhKRmX5Gehp8qLA9s0f2CzgoaZfRgY.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=cefdaa68242fcf07608bf59960ce244e29811bd0",
                              "width": 108
                          },
                          {
                              "height": 162,
                              "url": "https://external-preview.redd.it/XhYShPAHGC1U9qhKRmX5Gehp8qLA9s0f2CzgoaZfRgY.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=2596afb359a7a81aebcf29ff5b49d9b68f6faba5",
                              "width": 216
                          },
                          {
                              "height": 240,
                              "url": "https://external-preview.redd.it/XhYShPAHGC1U9qhKRmX5Gehp8qLA9s0f2CzgoaZfRgY.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=305927623e4bb8c5245c84b652b47ebfcd2e197d",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 360,
                          "url": "https://external-preview.redd.it/XhYShPAHGC1U9qhKRmX5Gehp8qLA9s0f2CzgoaZfRgY.jpg?auto=webp&amp;s=067dc71f1a97f0a6552a136131d3054c5d15769a",
                          "width": 480
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652762362,
          "score": 1,
          "secure_media": {
              "oembed": {
                  "author_name": "[lel]Nathan",
                  "author_url": "https://www.youtube.com/channel/UCttgr_XnakRcHFF5LOWDtZw",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/oWWhOFr9hlk?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/oWWhOFr9hlk/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "[E7 RTA] senya + ml cermia (the dynamic duo)",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "secure_media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/oWWhOFr9hlk?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "media_domain_url": "https://www.redditmedia.com/mediaembed/urekx9",
              "scrolling": false,
              "width": 356
          },
          "selftext": "",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142752,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/S23cxIU5c7T6L26qgybKSIg8gdden8FQ5UJKJcXizu4.jpg",
          "thumbnail_height": 105,
          "thumbnail_width": 140,
          "title": "senya + ml cermia (the dynamic duo)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://youtu.be/oWWhOFr9hlk",
          "url_overridden_by_dest": "https://youtu.be/oWWhOFr9hlk",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "GOODnEVIL_",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_cx4munve",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652761722,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ureexl/buying_a_account/",
          "gildings": {},
          "id": "ureexl",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ureexl/buying_a_account/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652761733,
          "score": 1,
          "selftext": "I\u2019m looking to buy a epic seven account \ud83d\udc4b",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142752,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Buying a account",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ureexl/buying_a_account/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "GOODnEVIL_",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_cx4munve",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652761626,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uree16/im_looking_for_an_account_to_buy/",
          "gildings": {},
          "id": "uree16",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": false,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 1,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uree16/im_looking_for_an_account_to_buy/",
          "pinned": false,
          "pwls": 7,
          "removed_by_category": "moderator",
          "retrieved_on": 1652761637,
          "score": 1,
          "selftext": "[removed]",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142752,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "I\u2019m looking for an account to buy",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uree16/im_looking_for_an_account_to_buy/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "NekoNel",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_4ftjukru",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652761485,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urecqx/eda_so_damn_fun_to_cleave_with/",
          "gildings": {},
          "id": "urecqx",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urecqx/eda_so_damn_fun_to_cleave_with/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "-S0hjoqRj5EiiHkG0I0Eh0xsPegsUoTbPw_uxoUqDl4",
                      "resolutions": [
                          {
                              "height": 48,
                              "url": "https://preview.redd.it/ptq2bhexqyz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=25769ab33334db3a63e85a3f7a4aece8e9c3d014",
                              "width": 108
                          },
                          {
                              "height": 97,
                              "url": "https://preview.redd.it/ptq2bhexqyz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=f79347cbecc13b3ab1ff972a71e4e6978ded11ad",
                              "width": 216
                          },
                          {
                              "height": 144,
                              "url": "https://preview.redd.it/ptq2bhexqyz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=a36caff7ffb16058c2c7afa7cd405e4fddd4acd7",
                              "width": 320
                          },
                          {
                              "height": 288,
                              "url": "https://preview.redd.it/ptq2bhexqyz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=8aa62648dda9815aa6579e6efc6ae1d723af84de",
                              "width": 640
                          },
                          {
                              "height": 432,
                              "url": "https://preview.redd.it/ptq2bhexqyz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=0cbc55eacdfa366843c6cf2113a728f086356d21",
                              "width": 960
                          },
                          {
                              "height": 486,
                              "url": "https://preview.redd.it/ptq2bhexqyz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=0f1bfc80d790ab37d9193184e608134067ff7446",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/ptq2bhexqyz81.jpg?auto=webp&amp;s=0148cf3b531fe3d16327c4c05acf1c1a97cf7a04",
                          "width": 2400
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652761496,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142752,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/NkkVEuKEiP-9Rs7KSKb9IbBNeHBMeSstdnEKQmyy2ZU.jpg",
          "thumbnail_height": 63,
          "thumbnail_width": 140,
          "title": "Eda so damn fun to cleave with",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/ptq2bhexqyz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/ptq2bhexqyz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Farmer4Fun",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "four-RemnantViolet",
          "author_flair_richtext": [
              {
                  "a": ":RemnantViolet:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/667oeyhbcxy51_t5_nrn6j/RemnantViolet"
              }
          ],
          "author_flair_template_id": "fa8609dc-2563-11eb-bd8a-0ecf964356ed",
          "author_flair_text": ":RemnantViolet:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_63or7e26",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652760794,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ure641/pets_can_drop_50_reforge_mats/",
          "gildings": {},
          "id": "ure641",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#041f33",
          "link_flair_css_class": "event",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Event / Update"
              }
          ],
          "link_flair_template_id": "9a1376f0-f53d-11e8-bef2-0e2b68f61f46",
          "link_flair_text": "Event / Update",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ure641/pets_can_drop_50_reforge_mats/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "XeL7ZbNQuH_b4CV53kBVqUZDeY-MKCOlEVmVrf4zOC8",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/31pst9fvoyz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=d4fa452cc22b40e797880cd8abd30ffe71f08168",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/31pst9fvoyz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=31ab9c832442e65909b06c49e08774dc10e093a0",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/31pst9fvoyz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=2bc258f2063983f6be2442f98555fb5c69b4f559",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/31pst9fvoyz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=e61553fc84a441744e28d29b9175b87e3baac333",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/31pst9fvoyz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=bc0422dd404a6450ecb99b30f7906cbfc64c3d4f",
                              "width": 960
                          },
                          {
                              "height": 498,
                              "url": "https://preview.redd.it/31pst9fvoyz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=f2f1f2c93cbb8547cf69a680f887ed0424a84a6c",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/31pst9fvoyz81.jpg?auto=webp&amp;s=35f04f57382bcb85364d4ab2ef5ff7c449895a4e",
                          "width": 2340
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652760805,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142752,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/ehFbethxlCiiUqOMegOKvUl6IVbkZlgnkp2R_Iyq2hU.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "pets can drop 50 reforge mats",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/31pst9fvoyz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/31pst9fvoyz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "QuirtyQwerty",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_cdf87zcb",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652754286,
          "domain": "youtu.be",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urcawi/little_queen_charlotte_one_shots_incomplete_fatsus/",
          "gildings": {},
          "id": "urcawi",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media": {
              "oembed": {
                  "author_name": "Fabio Hermis",
                  "author_url": "https://www.youtube.com/channel/UCVAbqpecyaIwZlpV5dbbfsQ",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/FxfDBQE5_cQ?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/FxfDBQE5_cQ/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Incomplete Fastus destroyed by Little Queen Charlotte\ufffc",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/FxfDBQE5_cQ?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "scrolling": false,
              "width": 356
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urcawi/little_queen_charlotte_one_shots_incomplete_fatsus/",
          "pinned": false,
          "post_hint": "rich:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "9DnPqeotbB3FkqzCu-HOTggixTGiDgKTvIFhzJ9kCVs",
                      "resolutions": [
                          {
                              "height": 81,
                              "url": "https://external-preview.redd.it/WRJJ-Tcdt4FhyqHqxSxhaZulzAwbLKKWhun2FidjlCE.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=83afe05b9af8821a5635182f43c004fd18f2b067",
                              "width": 108
                          },
                          {
                              "height": 162,
                              "url": "https://external-preview.redd.it/WRJJ-Tcdt4FhyqHqxSxhaZulzAwbLKKWhun2FidjlCE.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=e27429792df4080b5e8e26646ca5eac3ace9cd21",
                              "width": 216
                          },
                          {
                              "height": 240,
                              "url": "https://external-preview.redd.it/WRJJ-Tcdt4FhyqHqxSxhaZulzAwbLKKWhun2FidjlCE.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=d2c84ec4d0214ab8e4d2c30cc6e8911189a84435",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 360,
                          "url": "https://external-preview.redd.it/WRJJ-Tcdt4FhyqHqxSxhaZulzAwbLKKWhun2FidjlCE.jpg?auto=webp&amp;s=17413d114bfeb55ff68c59f7aba465b632c8449a",
                          "width": 480
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652754296,
          "score": 1,
          "secure_media": {
              "oembed": {
                  "author_name": "Fabio Hermis",
                  "author_url": "https://www.youtube.com/channel/UCVAbqpecyaIwZlpV5dbbfsQ",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/FxfDBQE5_cQ?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/FxfDBQE5_cQ/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Incomplete Fastus destroyed by Little Queen Charlotte\ufffc",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "secure_media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/FxfDBQE5_cQ?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "media_domain_url": "https://www.redditmedia.com/mediaembed/urcawi",
              "scrolling": false,
              "width": 356
          },
          "selftext": "",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142745,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/wyPjq06W3Ra2HXEBMGCbAJKeUzlyzluyT9QArCESsio.jpg",
          "thumbnail_height": 105,
          "thumbnail_width": 140,
          "title": "Little Queen Charlotte one shots Incomplete Fatsus",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://youtu.be/FxfDBQE5_cQ",
          "url_overridden_by_dest": "https://youtu.be/FxfDBQE5_cQ",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "mofuff",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_e4oiulq",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652752318,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urbp3x/this_is_beyond_frustrating/",
          "gildings": {},
          "id": "urbp3x",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urbp3x/this_is_beyond_frustrating/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "5opHl1ctW2zUGsQLiRFMJycwMI1rJ6o5aHq5zu9aV-E",
                      "resolutions": [
                          {
                              "height": 108,
                              "url": "https://preview.redd.it/fe9x224mzxz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=2919b962c89839f3373790627069808bd87da719",
                              "width": 108
                          },
                          {
                              "height": 217,
                              "url": "https://preview.redd.it/fe9x224mzxz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=612f7880b68b1bf518ea8d93754e6d635124b0ab",
                              "width": 216
                          },
                          {
                              "height": 322,
                              "url": "https://preview.redd.it/fe9x224mzxz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=b70629caad9b9bd947218b780c023cda2b0d6433",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 453,
                          "url": "https://preview.redd.it/fe9x224mzxz81.jpg?auto=webp&amp;s=82375d2193c62b802c2d07e0144d87e02f953398",
                          "width": 449
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652752328,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142745,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/I5lgAQv0kZ0RX1Jx40lCfJtUZGB9ePlZ5niI_gbUHGM.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "This is beyond frustrating.",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/fe9x224mzxz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/fe9x224mzxz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Chaoxytal",
          "author_flair_background_color": "#d3d6da",
          "author_flair_css_class": "two-kittyclarissa",
          "author_flair_richtext": [
              {
                  "a": ":kittyclarissa:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/zvj4nhj6d1s51_t5_nrn6j/kittyclarissa"
              }
          ],
          "author_flair_template_id": "c5f61452-f53e-11e8-b933-0e68cbf8db5a",
          "author_flair_text": ":kittyclarissa:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_jomr7",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652751427,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urbfk1/which_would_shake_up_the_game_more_team_size/",
          "gildings": {},
          "id": "urbfk1",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urbfk1/which_would_shake_up_the_game_more_team_size/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652751437,
          "score": 1,
          "selftext": "I don\u2019t work for SG. Just wondering. \ud83d\udc40",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142745,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Which would shake up the game more? Team size increased to 5? Or heroes can equip 2 artifacts? \ud83d\udc40",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/urbfk1/which_would_shake_up_the_game_more_team_size/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "emiracles",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_280g3a4g",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652751090,
          "domain": "i.imgur.com",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/urbbw6/lionheart_reverse_cleave_queen/",
          "gildings": {},
          "id": "urbbw6",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/urbbw6/lionheart_reverse_cleave_queen/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "wCQYnrbatjOQmrh7i-OAq101m4K1gDrsTQgXt4Ijl0Y",
                      "resolutions": [
                          {
                              "height": 69,
                              "url": "https://external-preview.redd.it/GiD6TtT-MdAhRuzawe_rEXGWJn2THqDgzkh3qw5oSGE.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=0017030db3e9388af6dad868e6987e9582bd0d3d",
                              "width": 108
                          },
                          {
                              "height": 138,
                              "url": "https://external-preview.redd.it/GiD6TtT-MdAhRuzawe_rEXGWJn2THqDgzkh3qw5oSGE.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=946325814f1782a60c9c43fbbda94d4129847d95",
                              "width": 216
                          },
                          {
                              "height": 205,
                              "url": "https://external-preview.redd.it/GiD6TtT-MdAhRuzawe_rEXGWJn2THqDgzkh3qw5oSGE.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=6b5a1421d6da0748b0c341e5d925b76b3986b661",
                              "width": 320
                          },
                          {
                              "height": 411,
                              "url": "https://external-preview.redd.it/GiD6TtT-MdAhRuzawe_rEXGWJn2THqDgzkh3qw5oSGE.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=2028b4c5f0491d2759af334ec78b5136f279645d",
                              "width": 640
                          },
                          {
                              "height": 617,
                              "url": "https://external-preview.redd.it/GiD6TtT-MdAhRuzawe_rEXGWJn2THqDgzkh3qw5oSGE.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=546d688c7905cab3a0ae3194dcb04b1c997fcb92",
                              "width": 960
                          },
                          {
                              "height": 694,
                              "url": "https://external-preview.redd.it/GiD6TtT-MdAhRuzawe_rEXGWJn2THqDgzkh3qw5oSGE.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=ae538140097de8f1e54572ef3aa5ec3c674f0678",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 812,
                          "url": "https://external-preview.redd.it/GiD6TtT-MdAhRuzawe_rEXGWJn2THqDgzkh3qw5oSGE.jpg?auto=webp&amp;s=d0ec84ecb303e31ebf0094ed319102473567a02f",
                          "width": 1263
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652751100,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142745,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/0jTBdr84X3wIKtarWEkhPEWnApL2D-ZBWUdmIC6kFTA.jpg",
          "thumbnail_height": 90,
          "thumbnail_width": 140,
          "title": "Lionheart - reverse cleave queen",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.imgur.com/klCjx9G.jpg",
          "url_overridden_by_dest": "https://i.imgur.com/klCjx9G.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Fayt117",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_3otqve78",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652747226,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ura5cu/abyss_66_using_sigret_bug/",
          "gildings": {},
          "id": "ura5cu",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ura5cu/abyss_66_using_sigret_bug/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652747236,
          "score": 1,
          "selftext": "Hello,\n\nI challenged Abyss 66 (the one with Arbiter Vildred)\n\nI managed to finish him off with Sigret's Guillotine, however he still revived to max health.\n\nIs this a bug ?",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142745,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Abyss 66 using Sigret bug ?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ura5cu/abyss_66_using_sigret_bug/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "HuyVinhNguyen",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_1lnflud2",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652746847,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ura19i/i_need_help_about_farming_catalyst/",
          "gildings": {},
          "id": "ura19i",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#cc66ff",
          "link_flair_css_class": "tip",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Tips"
              }
          ],
          "link_flair_template_id": "b093f3ee-17bd-11e9-ad8c-0eb3737ae650",
          "link_flair_text": "Tips",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ura19i/i_need_help_about_farming_catalyst/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652746858,
          "score": 1,
          "selftext": "I'm a new player and i saw a Google Sheet that show where to farm each catalyst, a lot of players still use it today but i find it kinda old, it only shows Unrecorded History. I wonder if other 'new' map like episode 2,3... has a higher drop rate or catalyst has a low drop rate in general. Thank you.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142745,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "I need help about farming catalyst",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ura19i/i_need_help_about_farming_catalyst/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Piscet",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_8gdghj1f",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652746046,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur9sd1/what_are_ways_to_deal_with_aravi/",
          "gildings": {},
          "id": "ur9sd1",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur9sd1/what_are_ways_to_deal_with_aravi/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652746056,
          "score": 1,
          "selftext": "I've been losing to her more often than I'd like to admit, since my only 100% answer to her happens to be one of the most meta units to ever meta. Everyone else I have built has some sort of weakness she can exploit, with rikoris being the sole exception since his damage scales purely off of his attack. And unfortunately, prebanning her isn't really an option since while she automatically puts me at a disadvantage, she isn't a straight up auto lose for me like aol or rimuru(I like buffs)",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142745,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "what are ways to deal with aravi?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ur9sd1/what_are_ways_to_deal_with_aravi/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "TheClincher7",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_3cx5nefb",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652740822,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur83xe/i_feel_so_lucky_todayi_went_for_a_random_single/",
          "gildings": {},
          "id": "ur83xe",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur83xe/i_feel_so_lucky_todayi_went_for_a_random_single/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "QbQ7Wf-dRgblW_UyZZnUrTFFUtOuZaSyLxQEoqqSvFQ",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/bhz2rjxg1xz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=a838ef61c2e9cd55bbef9880fa166df366511f78",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/bhz2rjxg1xz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=7d57fd1055ab2c5fd826ed3c2da3d3db5937d01f",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/bhz2rjxg1xz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=f8e840b08e28ecb09deca3e36d6b4c5f1d73c871",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/bhz2rjxg1xz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=c11976316d3184a14fed6ea1a6f2992ffc759f73",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/bhz2rjxg1xz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=8d6725dcb4443d4f30c312f951aff6469612a1e1",
                              "width": 960
                          },
                          {
                              "height": 499,
                              "url": "https://preview.redd.it/bhz2rjxg1xz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=c28b813f5d21b0326fa45ce647620d8778e2f6dd",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 828,
                          "url": "https://preview.redd.it/bhz2rjxg1xz81.jpg?auto=webp&amp;s=e499ccc298b67b3af63943d9ef693aa36f224fbb",
                          "width": 1792
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652740833,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142742,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/7ZA_r-CX-Gg-5W8-pibrf8pSaPuM5iVvxBQaReSCvAU.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "I feel so lucky today\u2026..I went for a random single mystic and got this dude..",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/bhz2rjxg1xz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/bhz2rjxg1xz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Psychological_Ad2393",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_7h24v0as",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652740425,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur7z0y/is_furious_good/",
          "gildings": {},
          "id": "ur7z0y",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur7z0y/is_furious_good/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652740435,
          "score": 1,
          "selftext": "I\u2019m building my team for wyvern 13 and heard that furious is good for that",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142741,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Is furious good",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ur7z0y/is_furious_good/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Ukemaster24",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_e846vels",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652738507,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur7avw/wyvern_tips/",
          "gildings": {},
          "id": "ur7avw",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur7avw/wyvern_tips/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652738518,
          "score": 1,
          "selftext": "Hi guys! I need some wyvern tips! I\u2019m using sigret, Angelica, furious and Alexa.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142738,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Wyvern tips?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ur7avw/wyvern_tips/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Beyondins1ght",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_bsexjaap",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652732912,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur58bf/im_trying_to_make_a_good_build_for_a_ravi_but_my/",
          "gildings": {},
          "id": "ur58bf",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur58bf/im_trying_to_make_a_good_build_for_a_ravi_but_my/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "PPMPs2ngYgpnMQ2p9diQysZiml9TP8Kxad3FXfjDZOk",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/tzbtrmrydwz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=da3e23f475e6812c0c815619364766bf8aae79ae",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/tzbtrmrydwz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=9094dc70093d1c9d77f5c21eb23dff82351ff7ee",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/tzbtrmrydwz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=3b8139e50bd8fcae77cb7922d4196567515ee1a6",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/tzbtrmrydwz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=5df7875e5358ec420ef8b5968c413e7fdc9270bf",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/tzbtrmrydwz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=d6d9175915ff332cc896b66a10841a33a3c2434f",
                              "width": 960
                          },
                          {
                              "height": 499,
                              "url": "https://preview.redd.it/tzbtrmrydwz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=280cd86a95a614425de43ff539cabce69e40c693",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1170,
                          "url": "https://preview.redd.it/tzbtrmrydwz81.jpg?auto=webp&amp;s=253d7c1fd75e9b438eb55b1b95c19a8c28da5175",
                          "width": 2532
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652732922,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142737,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/g0_W_EdcxKvUWmJpY45wUBkgt6pXSbOtn3TBnCZz7Qs.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "I\u2019m trying to make a good build for A Ravi but my damage output is about half of most A Ravis in arena , what am I doing wrong?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/tzbtrmrydwz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/tzbtrmrydwz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "lt519",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_w64wu",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652730678,
          "domain": "youtu.be",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur4e6e/hall_of_trials_incomplete_fastus_11m/",
          "gildings": {},
          "id": "ur4e6e",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#005000",
          "link_flair_css_class": "guide",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Guide / Tools"
              }
          ],
          "link_flair_template_id": "9e41cff6-f53d-11e8-8a5f-0eec5ae9331c",
          "link_flair_text": "Guide / Tools",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media": {
              "oembed": {
                  "author_name": "Raymond Arbusto",
                  "author_url": "https://www.youtube.com/channel/UCVJKi7xqF-i6p0rTVzC-nfQ",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/akMLIdmBXzE?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/akMLIdmBXzE/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Epic Seven - Hall of Trials - Incomplete Fastus - 11m+ - 5.16.22",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/akMLIdmBXzE?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "scrolling": false,
              "width": 356
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur4e6e/hall_of_trials_incomplete_fastus_11m/",
          "pinned": false,
          "post_hint": "rich:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "jbHU5OjAbDO6HLKQ7lLuPIPItoESLM1-XDR2kpqzT7M",
                      "resolutions": [
                          {
                              "height": 81,
                              "url": "https://external-preview.redd.it/KtFVjUBUTpl9OkLzeoHm0g6AmVBVLHBfW9xOG02nu8E.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=394001a25bc850a09f36ade6535322c0f2689148",
                              "width": 108
                          },
                          {
                              "height": 162,
                              "url": "https://external-preview.redd.it/KtFVjUBUTpl9OkLzeoHm0g6AmVBVLHBfW9xOG02nu8E.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=7e4c6978ea54fd269fcf1f16e8a6096b4b741f80",
                              "width": 216
                          },
                          {
                              "height": 240,
                              "url": "https://external-preview.redd.it/KtFVjUBUTpl9OkLzeoHm0g6AmVBVLHBfW9xOG02nu8E.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=40b4aa3a44a4d39bba1a8371a4ef24dfe187e5db",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 360,
                          "url": "https://external-preview.redd.it/KtFVjUBUTpl9OkLzeoHm0g6AmVBVLHBfW9xOG02nu8E.jpg?auto=webp&amp;s=e4728b39448b6d434259afd44407c651f6120894",
                          "width": 480
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652730688,
          "score": 1,
          "secure_media": {
              "oembed": {
                  "author_name": "Raymond Arbusto",
                  "author_url": "https://www.youtube.com/channel/UCVJKi7xqF-i6p0rTVzC-nfQ",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/akMLIdmBXzE?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/akMLIdmBXzE/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Epic Seven - Hall of Trials - Incomplete Fastus - 11m+ - 5.16.22",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "secure_media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/akMLIdmBXzE?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "media_domain_url": "https://www.redditmedia.com/mediaembed/ur4e6e",
              "scrolling": false,
              "width": 356
          },
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142735,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/2_mkN2lk_oURda-m7swUKGnIodxa1OR6Wf1jSvew09o.jpg",
          "thumbnail_height": 105,
          "thumbnail_width": 140,
          "title": "Hall of Trials - Incomplete Fastus - 11m+",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://youtu.be/akMLIdmBXzE",
          "url_overridden_by_dest": "https://youtu.be/akMLIdmBXzE",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "den_j",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_3kq5ll5d",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652729618,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur406x/data_issue_with_e7/",
          "gildings": {},
          "id": "ur406x",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#cc66ff",
          "link_flair_css_class": "tip",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Tips"
              }
          ],
          "link_flair_template_id": "b093f3ee-17bd-11e9-ad8c-0eb3737ae650",
          "link_flair_text": "Tips",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur406x/data_issue_with_e7/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652729629,
          "score": 1,
          "selftext": "Hi everyone,\n\nI tried to get an answer at the megathread but no responds so far :/\n\nI have an issue with e7 use an absurd amount of my mobile data.\n\nReinstalled today to test it again.. did 1 Rta match (maybe 5min in total) and e7 used 8mb of my data ..\n\nAsked support they couldn\u2019t help either and just said to reinstall game ..",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142736,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Data issue with e7",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ur406x/data_issue_with_e7/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "SpacePenguin227",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_b183ijht",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652726271,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur2s8y/pair_with_aravi/",
          "gildings": {},
          "id": "ur2s8y",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur2s8y/pair_with_aravi/",
          "pinned": false,
          "poll_data": {
              "is_prediction": false,
              "options": [
                  {
                      "id": "15898423",
                      "text": "Landy"
                  },
                  {
                      "id": "15898424",
                      "text": "Fallen Cecilia"
                  },
                  {
                      "id": "15898425",
                      "text": "Luna"
                  },
                  {
                      "id": "15898426",
                      "text": "Aria"
                  },
                  {
                      "id": "15898427",
                      "text": "Fairytale Tenebria"
                  }
              ],
              "prediction_status": null,
              "resolved_option_id": null,
              "total_stake_amount": null,
              "total_vote_count": 0,
              "tournament_id": null,
              "user_selection": null,
              "user_won_amount": null,
              "vote_updates_remained": null,
              "voting_end_timestamp": 1652985471590
          },
          "pwls": 7,
          "retrieved_on": 1652726282,
          "score": 1,
          "selftext": "My GW teams are super lacking, but I got an ARavi recently and am wondering who to best max out with her right now since I only have enough resources to max one other unit. If there\u2019s any other units that aren\u2019t listed (I probably don\u2019t have them lol), but are great with ARavi, feel free to tell me so I can prioritize their banners!\n\n[View Poll](https://www.reddit.com/poll/ur2s8y)",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142737,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Pair with ARavi?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ur2s8y/pair_with_aravi/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Tenshied",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_6e2siz3p",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652726185,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur2r5x/finally_got_to_challenger_rta_after_playing_for_7/",
          "gildings": {},
          "id": "ur2r5x",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur2r5x/finally_got_to_challenger_rta_after_playing_for_7/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "TGJOz0gk7F9XeY_H47LWKjMlCbnnGukFlDme16v6gjY",
                      "resolutions": [
                          {
                              "height": 48,
                              "url": "https://preview.redd.it/z3xcasiytvz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=3d5d7427212565bd92f273cddae42f8537764be9",
                              "width": 108
                          },
                          {
                              "height": 96,
                              "url": "https://preview.redd.it/z3xcasiytvz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=dba689a82c32f74863020ff0d7899eabbec4b2f4",
                              "width": 216
                          },
                          {
                              "height": 143,
                              "url": "https://preview.redd.it/z3xcasiytvz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=03852d6cae1b72299bae24956dfe275b243174a0",
                              "width": 320
                          },
                          {
                              "height": 287,
                              "url": "https://preview.redd.it/z3xcasiytvz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=8894a48ad68e37b380acfc04c7f2527e4745d726",
                              "width": 640
                          },
                          {
                              "height": 430,
                              "url": "https://preview.redd.it/z3xcasiytvz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=8bcdd51b23019990ac4652d496cc41588e75eba6",
                              "width": 960
                          },
                          {
                              "height": 484,
                              "url": "https://preview.redd.it/z3xcasiytvz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=4909513716464d3a9a3fef3b7cce2ffef2e13711",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/z3xcasiytvz81.jpg?auto=webp&amp;s=d44ec5b72f45d25545ce3e782287fba81f439971",
                          "width": 2408
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652726196,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142737,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/YrmiDJ2WvF_VoPWP_z10-FJTfGO0C4KphQ2FRkD3lDo.jpg",
          "thumbnail_height": 62,
          "thumbnail_width": 140,
          "title": "Finally got to challenger rta after playing for 7 months",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/z3xcasiytvz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/z3xcasiytvz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "SpacePenguin227",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_b183ijht",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652725967,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur2o7m/cant_decide_who_to_use_my_6_potion_on/",
          "gildings": {},
          "id": "ur2o7m",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": false,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 1,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur2o7m/cant_decide_who_to_use_my_6_potion_on/",
          "pinned": false,
          "pwls": 7,
          "removed_by_category": "automod_filtered",
          "retrieved_on": 1652725978,
          "score": 1,
          "selftext": "[removed]\n\n[View Poll](https://www.reddit.com/poll/ur2o7m)",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142737,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Cant decide who to use my 6\u2606 potion on!",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/ur2o7m/cant_decide_who_to_use_my_6_potion_on/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Relative-Rip3912",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_8kicdufx",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652723927,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur1x8p/ilynav_in_swimsuit_by_me/",
          "gildings": {},
          "id": "ur1x8p",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": true,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur1x8p/ilynav_in_swimsuit_by_me/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "Az2cDuQBQEjyqoAuVMWAw0yDKeWxerITj4LQ_pJhytg",
                      "resolutions": [
                          {
                              "height": 164,
                              "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=d837312261ba7b31966f6de90b385ad6a5832f86",
                              "width": 108
                          },
                          {
                              "height": 329,
                              "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=b8766ad55c04eca4d6092328c990e08cb35ad0c3",
                              "width": 216
                          },
                          {
                              "height": 487,
                              "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=7c835b8fac0ece9314db34472c37dd4ef005a733",
                              "width": 320
                          },
                          {
                              "height": 975,
                              "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=26271d6ebe05c53f64971ffc87d48aa08fb94196",
                              "width": 640
                          },
                          {
                              "height": 1463,
                              "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=c6cd1b6d79cbd0864e0a36098a0af31396545495",
                              "width": 960
                          },
                          {
                              "height": 1646,
                              "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=54cc809ca69620cb257ce5a24f5feec7fa6adefd",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 3000,
                          "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?auto=webp&amp;s=7f78deb2b893925bf4b61189104cc9ea78853f90",
                          "width": 1968
                      },
                      "variants": {
                          "nsfw": {
                              "resolutions": [
                                  {
                                      "height": 164,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=371cdd29010bd071b7f69f16b9216269e29e12c0",
                                      "width": 108
                                  },
                                  {
                                      "height": 329,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=2158ca2bf7e3b20327fe269fe8194d1fc06298fc",
                                      "width": 216
                                  },
                                  {
                                      "height": 487,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=5acf3db3b9b3a53052230cffe74022db2e619f3c",
                                      "width": 320
                                  },
                                  {
                                      "height": 975,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=7e6ce10a52d5c218828a382349324b0686c45d8f",
                                      "width": 640
                                  },
                                  {
                                      "height": 1463,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=960&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=663aab7e86d72eff241c297e4294669486f91638",
                                      "width": 960
                                  },
                                  {
                                      "height": 1646,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=1080&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=9a1941d582ed0c12dd938eec94504d7a7fd06265",
                                      "width": 1080
                                  }
                              ],
                              "source": {
                                  "height": 3000,
                                  "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=bd35a8570e5df3f0687fb5106d2995830a9f56d0",
                                  "width": 1968
                              }
                          },
                          "obfuscated": {
                              "resolutions": [
                                  {
                                      "height": 164,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=371cdd29010bd071b7f69f16b9216269e29e12c0",
                                      "width": 108
                                  },
                                  {
                                      "height": 329,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=2158ca2bf7e3b20327fe269fe8194d1fc06298fc",
                                      "width": 216
                                  },
                                  {
                                      "height": 487,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=5acf3db3b9b3a53052230cffe74022db2e619f3c",
                                      "width": 320
                                  },
                                  {
                                      "height": 975,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=7e6ce10a52d5c218828a382349324b0686c45d8f",
                                      "width": 640
                                  },
                                  {
                                      "height": 1463,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=960&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=663aab7e86d72eff241c297e4294669486f91638",
                                      "width": 960
                                  },
                                  {
                                      "height": 1646,
                                      "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?width=1080&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=9a1941d582ed0c12dd938eec94504d7a7fd06265",
                                      "width": 1080
                                  }
                              ],
                              "source": {
                                  "height": 3000,
                                  "url": "https://preview.redd.it/g1dovu1lmvz81.jpg?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=bd35a8570e5df3f0687fb5106d2995830a9f56d0",
                                  "width": 1968
                              }
                          }
                      }
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652723937,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142737,
          "subreddit_type": "public",
          "thumbnail": "nsfw",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Ilynav in swimsuit (by me)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/g1dovu1lmvz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/g1dovu1lmvz81.jpg",
          "whitelist_status": "promo_adult_nsfw",
          "wls": 3
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Tenshied",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_6e2siz3p",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652723881,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur1wmu/finally_got_to_challenger_rta_after_playing_for_7/",
          "gildings": {},
          "id": "ur1wmu",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur1wmu/finally_got_to_challenger_rta_after_playing_for_7/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "mw4QZmeD28sG_OIDePCFOIjAU6xaAqMbBP7oiRi8Tp4",
                      "resolutions": [
                          {
                              "height": 48,
                              "url": "https://preview.redd.it/ppmqmzu3nvz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=24cf81ac1e1f666ca7bfddf8b5d82b4a0f218e59",
                              "width": 108
                          },
                          {
                              "height": 96,
                              "url": "https://preview.redd.it/ppmqmzu3nvz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=d27a983b0a99eaa56c6fc959122ece158b3365af",
                              "width": 216
                          },
                          {
                              "height": 143,
                              "url": "https://preview.redd.it/ppmqmzu3nvz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=b46f54dee5376998058ceced329226dc5c339b8a",
                              "width": 320
                          },
                          {
                              "height": 287,
                              "url": "https://preview.redd.it/ppmqmzu3nvz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=cec2d611e7837e0e8fc7a4bf4bc01286793efec2",
                              "width": 640
                          },
                          {
                              "height": 430,
                              "url": "https://preview.redd.it/ppmqmzu3nvz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=78faf0b949a7c11c651760fe076114aa540431db",
                              "width": 960
                          },
                          {
                              "height": 484,
                              "url": "https://preview.redd.it/ppmqmzu3nvz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=c1f4cc36504eeb2de182f16a913837de68a51c1b",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/ppmqmzu3nvz81.jpg?auto=webp&amp;s=df43272f4fe0a86c74ecb79280c7addf9ffb4135",
                          "width": 2408
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652723891,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142737,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/f5Zs_qKWjWVFCKrA67lBeBaRwKIb-D0G5jR3hP6ERuM.jpg",
          "thumbnail_height": 62,
          "thumbnail_width": 140,
          "title": "Finally got to challenger rta after playing for 7 months",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/ppmqmzu3nvz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/ppmqmzu3nvz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "TsuKiyoMe",
          "author_flair_background_color": "#d3d6da",
          "author_flair_css_class": "one-karin",
          "author_flair_richtext": [
              {
                  "e": "text",
                  "t": "Youtube: im_Tsu"
              }
          ],
          "author_flair_template_id": "ba8d248e-f53e-11e8-a8d2-0ebef6f58ad2",
          "author_flair_text": "Youtube: im_Tsu",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_879b4",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652723228,
          "domain": "youtu.be",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/ur1o74/since_her_release_people_have_asked_me_for_a/",
          "gildings": {},
          "id": "ur1o74",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#005000",
          "link_flair_css_class": "guide",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Guide / Tools"
              }
          ],
          "link_flair_template_id": "9e41cff6-f53d-11e8-8a5f-0eec5ae9331c",
          "link_flair_text": "Guide / Tools",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media": {
              "oembed": {
                  "author_name": "im_Tsu",
                  "author_url": "https://www.youtube.com/c/TsuKiyoMe",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/tvhapgHoUhw?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/tvhapgHoUhw/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "[Epic Seven] How to Play: Hwayoung",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/tvhapgHoUhw?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "scrolling": false,
              "width": 356
          },
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/ur1o74/since_her_release_people_have_asked_me_for_a/",
          "pinned": false,
          "post_hint": "rich:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "aaRNYeJBHMsY5tfx98gYmOAReCUTwODapK7uin3dGdM",
                      "resolutions": [
                          {
                              "height": 81,
                              "url": "https://external-preview.redd.it/p42_XO52UcCR9zr7coBeuBabZTPBfcNjRR4dobGklWg.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=552f3a73269588da06ec19eb5619c728cd20250e",
                              "width": 108
                          },
                          {
                              "height": 162,
                              "url": "https://external-preview.redd.it/p42_XO52UcCR9zr7coBeuBabZTPBfcNjRR4dobGklWg.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=f628416ac49c13108638d92953581323d42d8b2d",
                              "width": 216
                          },
                          {
                              "height": 240,
                              "url": "https://external-preview.redd.it/p42_XO52UcCR9zr7coBeuBabZTPBfcNjRR4dobGklWg.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=24727ebe071f8c1948244954bac137b4155eae12",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 360,
                          "url": "https://external-preview.redd.it/p42_XO52UcCR9zr7coBeuBabZTPBfcNjRR4dobGklWg.jpg?auto=webp&amp;s=fa01727553c186d8b17f6dba68d53435e97910d5",
                          "width": 480
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652723239,
          "score": 1,
          "secure_media": {
              "oembed": {
                  "author_name": "im_Tsu",
                  "author_url": "https://www.youtube.com/c/TsuKiyoMe",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/tvhapgHoUhw?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/tvhapgHoUhw/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "[Epic Seven] How to Play: Hwayoung",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "secure_media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/tvhapgHoUhw?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "media_domain_url": "https://www.redditmedia.com/mediaembed/ur1o74",
              "scrolling": false,
              "width": 356
          },
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142736,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/2Q69Z8v_tJ5SSqxVXUSLufHNyRkZJg8MEI1s3UpBJzU.jpg",
          "thumbnail_height": 105,
          "thumbnail_width": 140,
          "title": "Since her release, people have asked me for a Hwayoung guide so here it is. Hopefully this is still relevant and helpful to some of you! (Bonus Points if you get the Thumbnail Reference)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://youtu.be/tvhapgHoUhw",
          "url_overridden_by_dest": "https://youtu.be/tvhapgHoUhw",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "tintonus",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_2n2hqh8b",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652716794,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqz87c/when_a_bunch_of_guilides_idle_at_starting_point/",
          "gildings": {},
          "id": "uqz87c",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqz87c/when_a_bunch_of_guilides_idle_at_starting_point/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "fzC8pc4vLalDBK7Xs_-JG5AotNn__0tHSrRVpj3K1DI",
                      "resolutions": [
                          {
                              "height": 60,
                              "url": "https://preview.redd.it/koo1znwy1vz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=4bc3a72d0a4f8054c3e2195f3aa0bc89d6675b40",
                              "width": 108
                          },
                          {
                              "height": 120,
                              "url": "https://preview.redd.it/koo1znwy1vz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=ec70bcbe763ad4c1d545c1101e9390690967df97",
                              "width": 216
                          },
                          {
                              "height": 178,
                              "url": "https://preview.redd.it/koo1znwy1vz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=58a4342deb3c751d25a6435f08f526add97bd940",
                              "width": 320
                          },
                          {
                              "height": 357,
                              "url": "https://preview.redd.it/koo1znwy1vz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=7f8851c3dbc4fd266afbfda0ec582f084c5fc2ba",
                              "width": 640
                          }
                      ],
                      "source": {
                          "height": 493,
                          "url": "https://preview.redd.it/koo1znwy1vz81.jpg?auto=webp&amp;s=a19f05858350aa34b8b4c1149c3a4efcb49a54cd",
                          "width": 883
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652716806,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142730,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/CxANmOCNDJYfEWq4Pr6VdRNYvQa4bfFYWliA-MFXfEQ.jpg",
          "thumbnail_height": 78,
          "thumbnail_width": 140,
          "title": "When a bunch of guilides idle at starting point and Smilegate trolls you with the stupid language filter that bans their own vocabulary",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/koo1znwy1vz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/koo1znwy1vz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Adrianbigyes",
          "author_flair_background_color": "#d3d6da",
          "author_flair_css_class": "one-ken",
          "author_flair_richtext": [
              {
                  "a": ":ken:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/liur5cb6d1s51_t5_nrn6j/ken"
              }
          ],
          "author_flair_template_id": "c042c08c-f53e-11e8-8b4d-0ea04920fc44",
          "author_flair_text": ":ken:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_129n5q",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652716180,
          "domain": "/r/EpicSeven/comments/uqz0b0/e7_gameplay_with_no_s3_animations_feat_g13_b13/",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqz0b0/e7_gameplay_with_no_s3_animations_feat_g13_b13/",
          "gildings": {},
          "id": "uqz0b0",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": true,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqz0b0/e7_gameplay_with_no_s3_animations_feat_g13_b13/",
          "pinned": false,
          "post_hint": "hosted:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "0cZi-XEt5kpSNVXQF8tLgVxALGKEki6ug6ujQc2CrDQ",
                      "resolutions": [
                          {
                              "height": 60,
                              "url": "https://external-preview.redd.it/IQGmdkbaET0ufqaOUfPUKW9Q5mwKJ4SiP4qk2kVmmuc.png?width=108&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=5f5635b898754b36350bc5fb70d3b8510bfda19d",
                              "width": 108
                          },
                          {
                              "height": 121,
                              "url": "https://external-preview.redd.it/IQGmdkbaET0ufqaOUfPUKW9Q5mwKJ4SiP4qk2kVmmuc.png?width=216&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=30a9aaa46ce22e59382ac0fad3c1137038a27abb",
                              "width": 216
                          },
                          {
                              "height": 180,
                              "url": "https://external-preview.redd.it/IQGmdkbaET0ufqaOUfPUKW9Q5mwKJ4SiP4qk2kVmmuc.png?width=320&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=ac38ca5653f73852896860ac1e58773347893f68",
                              "width": 320
                          },
                          {
                              "height": 360,
                              "url": "https://external-preview.redd.it/IQGmdkbaET0ufqaOUfPUKW9Q5mwKJ4SiP4qk2kVmmuc.png?width=640&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=8e399f61b5de419d9514dba3afbbfbd557b13b27",
                              "width": 640
                          },
                          {
                              "height": 540,
                              "url": "https://external-preview.redd.it/IQGmdkbaET0ufqaOUfPUKW9Q5mwKJ4SiP4qk2kVmmuc.png?width=960&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=afba22e5743f464330120f83a00fc56ae8ca0287",
                              "width": 960
                          },
                          {
                              "height": 607,
                              "url": "https://external-preview.redd.it/IQGmdkbaET0ufqaOUfPUKW9Q5mwKJ4SiP4qk2kVmmuc.png?width=1080&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=f45341c0210c241291195a278ca535b30fc9b635",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://external-preview.redd.it/IQGmdkbaET0ufqaOUfPUKW9Q5mwKJ4SiP4qk2kVmmuc.png?format=pjpg&amp;auto=webp&amp;s=6935c484456bcf91076cd6e8407b6f59ff119c94",
                          "width": 1920
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652716191,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142730,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/-JAZhly_E1fFP_cqQR_FKStuCAcZwwR3ucmnS6NfiDQ.jpg",
          "thumbnail_height": 78,
          "thumbnail_width": 140,
          "title": "E7 gameplay with no S3 animations feat. G13, B13 and W13",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://v.redd.it/49ywvragxuz81",
          "url_overridden_by_dest": "https://v.redd.it/49ywvragxuz81",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Triple_S_Rank",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_pf62m",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652708372,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqw7we/faithless_lidica_art_by_greenlegacy/",
          "gildings": {},
          "id": "uqw7we",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqw7we/faithless_lidica_art_by_greenlegacy/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "-4XZX1RvKGPpQ6-RhVV2aIHzlaAVwct2ZyKVECT59Zo",
                      "resolutions": [
                          {
                              "height": 126,
                              "url": "https://preview.redd.it/bwtzif9ucuz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=a6ecb0923a75687d49cd50cc0846af1430067d69",
                              "width": 108
                          },
                          {
                              "height": 252,
                              "url": "https://preview.redd.it/bwtzif9ucuz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=80f91be01481ad0698093e174c409af6835ee245",
                              "width": 216
                          },
                          {
                              "height": 373,
                              "url": "https://preview.redd.it/bwtzif9ucuz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=6a53a5e57cbaaeb132a834dc516fb863f0ba8616",
                              "width": 320
                          },
                          {
                              "height": 746,
                              "url": "https://preview.redd.it/bwtzif9ucuz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=1fe2b3722698984c8b03d8c2d00af78f8e2461b8",
                              "width": 640
                          },
                          {
                              "height": 1120,
                              "url": "https://preview.redd.it/bwtzif9ucuz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=5a031d19268bac5b8d4cb364d6b59f4689dd9bdc",
                              "width": 960
                          },
                          {
                              "height": 1260,
                              "url": "https://preview.redd.it/bwtzif9ucuz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=2f695c24d40f01999121060a5f94e6c892110a70",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 3500,
                          "url": "https://preview.redd.it/bwtzif9ucuz81.png?auto=webp&amp;s=20851f3412a6266eaf97d3dd6d03f2c5ef2f2fea",
                          "width": 3000
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652708383,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142731,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/yxO6q4k93oUV1SpgihJXJI9FudDuf7qUQGJi34wLTHw.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Faithless Lidica [art by GreenLegacy]",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/bwtzif9ucuz81.png",
          "url_overridden_by_dest": "https://i.redd.it/bwtzif9ucuz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Ukemaster24",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_e846vels",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652704462,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uquwys/why_do_you_guys_love_e7/",
          "gildings": {},
          "id": "uquwys",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uquwys/why_do_you_guys_love_e7/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652704472,
          "score": 1,
          "selftext": "Why do you guys love E7? For me it is the art style and voice acting. Also it is f2p friendly.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142732,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Why do you guys love E7?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uquwys/why_do_you_guys_love_e7/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "xxRebelstarxx",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_3f5d6mac",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652698088,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqt1zs/moroi_vs_ilynav/",
          "gildings": {},
          "id": "uqt1zs",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqt1zs/moroi_vs_ilynav/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652698099,
          "score": 1,
          "selftext": "I'm a returning player building a Moroi team.\n\nSo ARas finishes his speciality last night; as is typical E7 immediately taunts me by delivering Ilynav in the free summon this morning.  \n\nI have no other fire tanks and would rather do anything else than farm 400 runes for Ras (only to do it again for Carminrose).\n\nRas is clearly better for expeditions but is Ilynav good enough to get punched in the face by Moroi (and Lich), but worth it more generally? Injuries is cute though I have ARavi. I do PvP.\n\nThanks in advance.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142731,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Moroi vs Ilynav",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqt1zs/moroi_vs_ilynav/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Rinczmia",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "four-Flan",
          "author_flair_richtext": [
              {
                  "a": ":Flan:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/k2f1wugacxy51_t5_nrn6j/Flan"
              }
          ],
          "author_flair_template_id": "0d61f35a-2563-11eb-a19e-0ea82cef6357",
          "author_flair_text": ":Flan:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_2nlh2tqy",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652694910,
          "distinguished": "moderator",
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqs9jr/daily_questions_megathread_0516/",
          "gildings": {},
          "id": "uqs9jr",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#9abd19",
          "link_flair_css_class": "Megathread",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Megathread"
              }
          ],
          "link_flair_template_id": "dbef8262-f5f1-11e8-8f69-0e22455ee724",
          "link_flair_text": "Megathread",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqs9jr/daily_questions_megathread_0516/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652694920,
          "score": 1,
          "selftext": "Hello Heirs! This is the **Daily Questions Megathread**.\n\nYou are welcome to use the daily thread to ask general or personalized questions instead of creating a new thread.\n\nPlease ask all your beginner questions here as well. Help each other out and don't forget to thank/upvote fellow heirs!\n\n&amp;#x200B;\n\n**Useful Link(s):**\n\n* [Reddit Wiki](https://www.reddit.com/r/EpicSeven/wiki/index) \\- List of community resources\n* [Reddit Datamine Threads](https://www.reddit.com/user/YufineThrowaway/submitted/) \\- Spoiler warning\n* [Epic Seven News and Updates](https://page.onstove.com/epicseven/global/list/e7en001?listType=2&amp;direction=latest&amp;page=1) \\- (Official) link to Stove\n* [Hero/Artifact Spotlight](https://reddit.com/r/EpicSeven/about/sticky?num=2) \\- Ongoing discussion for banner featured this week\n* [Epic Seven Update History](https://docs.google.com/spreadsheets/d/15ac0nWz9lm3DRvmKD4OX_KBDpdO35WXNuj109qoQaIs/preview) \\- List of banners and updates since 2018\n\n&amp;#x200B;\n\n|**Other Megathreads**|\n|:-|\n|[Weekly Gacha &amp; Rant Megathread](https://www.reddit.com/r/EpicSeven/search?q=title%3A%22Rant+Megathread%22&amp;restrict_sr=on&amp;sort=relevance&amp;t=week)|\n|[Weekly Friend &amp; Guild Recruitment Megathread](https://www.reddit.com/r/EpicSeven/search?q=title%3A%22Recruitment+Megathread%22&amp;restrict_sr=on&amp;sort=new&amp;t=week)|",
          "send_replies": false,
          "spoiler": false,
          "stickied": true,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142729,
          "subreddit_type": "public",
          "suggested_sort": "new",
          "thumbnail": "self",
          "title": "Daily Questions Megathread (05/16)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqs9jr/daily_questions_megathread_0516/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "zoopido",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_tk6ly",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652688501,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqqtfv/dad_e7_crafting_update_save_the_kittens/",
          "gildings": {},
          "id": "uqqtfv",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#005000",
          "link_flair_css_class": "guide",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Guide / Tools"
              }
          ],
          "link_flair_template_id": "9e41cff6-f53d-11e8-8a5f-0eec5ae9331c",
          "link_flair_text": "Guide / Tools",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqqtfv/dad_e7_crafting_update_save_the_kittens/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652688511,
          "score": 1,
          "selftext": "There's someone to learn from everyone. Some random joker called my ARavi mid-game the other day but made a good point around knowing what you get yourself into when it comes to crafting. I personally wasted my materials again, so you hopefully don't have to. This time, I did 50K necklaces (I'll do boots at the end of the month). \n\nThe data:\n\n||Left Side (Helmet)|Right Side (Rings)|Right Side (Necklace)|\n|:-|:-|:-|:-|\n|Materials Used|50400|50050|50050|\n|Number of Gear Crafted|1200|910|910|\n|Gear Enhanced (Speed)|215|160|161|\n|Gear Enhanced (non-Speed)|18|2|3|\n|Gear Kept (+15 and Used)|8|2|3 (1 speed piece)|\n|Materials Used (per Speed roll)|234|313|311|\n|Materials Used (per Non-Speed Roll)|2800|25025|16683|\n|Materials Used (per Gear Kept)|6300|25025|16683|\n\nConclusions:\n\n* I left out the mileage pieces as they're statistically irrelevant - they don't magically make crafting right-side any better. (I got zero pieces from the 40 mileage necklaces, so I didn't want to be skew the results either)\n* The last 3 numbers are most important as they give you a direct comparison between the 3 based on number of materials used\n* Left-side is 4x more efficient than rings and 2.6x more efficient than necklaces. RNG is at it again but if you rule out the 1 speed piece I rolled (outlier), then it's exactly the same. Even if you're just chasing speed, left side is 33% more efficient (20 spd is the same no matter left or right side)\n* Feel free to refer to my gear guide for additional info on how to avoid right-side crafting ([https://zoopido.me/gear-guide](https://zoopido.me/gear-guide))\n* This question comes up every post so: If you need cores, remember to extract every chance you get including failed enhanced right side pieces (necklace, ring, boots).\n* Everyone time someone crafts right-side a kitten dies. I killed a few in the name of science and it hurts. Don't be like me, please save those kittens!\n\n  \nKittens harmed in this project were not in vain - rolled a 22spd CC neck!",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142726,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Dad E7 - Crafting Update - Save the Kittens",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqqtfv/dad_e7_crafting_update_save_the_kittens/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "esztersunday",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_11l82o",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652688473,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqqt7y/inheritance_new_object_manalift_we_dont_have_to/",
          "gildings": {},
          "id": "uqqt7y",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#cc66ff",
          "link_flair_css_class": "tip",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Tips"
              }
          ],
          "link_flair_template_id": "b093f3ee-17bd-11e9-ad8c-0eb3737ae650",
          "link_flair_text": "Tips",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqqt7y/inheritance_new_object_manalift_we_dont_have_to/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "AYn2Q6Zk9RUwM9GzQspvQe9r8uRflK03_8MRcXYcQHY",
                      "resolutions": [
                          {
                              "height": 51,
                              "url": "https://preview.redd.it/jw95xtdrpsz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=5f3c13317aaae4ffc75bfe74bd15b459f5f2d1ac",
                              "width": 108
                          },
                          {
                              "height": 102,
                              "url": "https://preview.redd.it/jw95xtdrpsz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=a043c37a88763c12bca4de25c37aace083105a97",
                              "width": 216
                          },
                          {
                              "height": 151,
                              "url": "https://preview.redd.it/jw95xtdrpsz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=b5b08c39e4b7945daf2fab4081d229ecbbe0eefb",
                              "width": 320
                          },
                          {
                              "height": 303,
                              "url": "https://preview.redd.it/jw95xtdrpsz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=b974cc0060373446bb2f2c44d515f90520ab7eba",
                              "width": 640
                          },
                          {
                              "height": 454,
                              "url": "https://preview.redd.it/jw95xtdrpsz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=73d5c930d0c317f02361798e5766d0364fc3e332",
                              "width": 960
                          },
                          {
                              "height": 511,
                              "url": "https://preview.redd.it/jw95xtdrpsz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=ff08f2d1aa4e637b24f959afcb736efca651cabc",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/jw95xtdrpsz81.png?auto=webp&amp;s=bc808ab0d132c532b1d9722eeef37183f76dc5a9",
                          "width": 2280
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652688483,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142726,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/QrgeUCs0rQU1Letm_Im3iefuD_oqd54zJO57aOnLWgk.jpg",
          "thumbnail_height": 66,
          "thumbnail_width": 140,
          "title": "Inheritance New object manalift! We don't have to kill monsters that block the path!",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 0.99,
          "url": "https://i.redd.it/jw95xtdrpsz81.png",
          "url_overridden_by_dest": "https://i.redd.it/jw95xtdrpsz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "bluewolfgd",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_2oyk4y6h",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652687343,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqqk02/one_of_the_best_gear_i_have_roll/",
          "gildings": {},
          "id": "uqqk02",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqqk02/one_of_the_best_gear_i_have_roll/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "gfWMcZJy3sgqFszduOmBuxqJz9CAEyemUxTiYGmH_q4",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/zb2s6tkgmsz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=c7943588b3f2de1b5c7aca8e7e3879b3c76ab75b",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/zb2s6tkgmsz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=1bb7962aca0aa76ca7f90ec97c4486211a27fcfe",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/zb2s6tkgmsz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=c802c27f133e857812352607b275c9839224b6b3",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/zb2s6tkgmsz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=97306a40aa5284046bf7c516dca712c618c545d8",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/zb2s6tkgmsz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=cee2239b989530b9be3c426a9b14ace33a7d929d",
                              "width": 960
                          },
                          {
                              "height": 498,
                              "url": "https://preview.redd.it/zb2s6tkgmsz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=f7ff4a8ad4c0d39a5debb8a157500ceee79506fa",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/zb2s6tkgmsz81.jpg?auto=webp&amp;s=04ccc3cbb11b4b0fe06dcfaa28c192610c5c0e57",
                          "width": 2340
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652687353,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142726,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/Xj7O6qHUIVQhzQUSGPvr3nvX-S9vkw4W88K4CGp_xlY.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "one of the best gear I have roll",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/zb2s6tkgmsz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/zb2s6tkgmsz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "esztersunday",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_11l82o",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652686481,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqqd2p/inheritance_what_are_these_things_i_tried_to_move/",
          "gildings": {},
          "id": "uqqd2p",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqqd2p/inheritance_what_are_these_things_i_tried_to_move/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "JNYfqHP5I8u8pEp7nHeR0dppX2mWTKDIcHZsuqPZDd0",
                      "resolutions": [
                          {
                              "height": 51,
                              "url": "https://preview.redd.it/ine6k0huisz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=f131d0f637b22af01455d6f47aad7346ad404b29",
                              "width": 108
                          },
                          {
                              "height": 102,
                              "url": "https://preview.redd.it/ine6k0huisz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=59640d078835484bb1caac3cc85349c37f32d99e",
                              "width": 216
                          },
                          {
                              "height": 151,
                              "url": "https://preview.redd.it/ine6k0huisz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=d28713c93ac2d63268c331f87b388f51ce3551a8",
                              "width": 320
                          },
                          {
                              "height": 303,
                              "url": "https://preview.redd.it/ine6k0huisz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=b4f96bdd3678f306b8d89403bcc32ea0c04453c6",
                              "width": 640
                          },
                          {
                              "height": 454,
                              "url": "https://preview.redd.it/ine6k0huisz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=d697669ce374c8a5cfb995a3244b0f7a6f3506ce",
                              "width": 960
                          },
                          {
                              "height": 511,
                              "url": "https://preview.redd.it/ine6k0huisz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=dae0971c972b475cba33f31393deedc7262d1d98",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/ine6k0huisz81.png?auto=webp&amp;s=cf712520d2e86a227636c32c03debb1dc1caa3a0",
                          "width": 2280
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652686492,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142725,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/uXptr-kguNukWABt9SvPOl83DGls8PZWWT9ll2V5F9k.jpg",
          "thumbnail_height": 66,
          "thumbnail_width": 140,
          "title": "Inheritance: What are these things? I tried to move there, thinking it was a teleport, but it says \"You cannot move to this region.\" How do I get to the other side?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 0.99,
          "url": "https://i.redd.it/ine6k0huisz81.png",
          "url_overridden_by_dest": "https://i.redd.it/ine6k0huisz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Turnipgreen2",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_wurr9",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652685044,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqq148/how_to_be_lucky_and_unlucky_at_the_same_time/",
          "gildings": {},
          "id": "uqq148",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqq148/how_to_be_lucky_and_unlucky_at_the_same_time/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "vKLbu2OiE-Gzuu8laqRzrEedSl2U37hRUKW0WG59iSQ",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/m04l2msmfsz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=3f6e930dd86a0b7c6139766c162c8fbaee43fe16",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/m04l2msmfsz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=9f4ed39b4beb88a520f4599259b9a84cab34c1c8",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/m04l2msmfsz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=10a3ce1b4e771b1d43b74c33ad56d9092714e581",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/m04l2msmfsz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=1278ee44f56cbaf5b111eda949b757424e7a08c6",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/m04l2msmfsz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=4ac54b64468c0450704e8fe99d811aa873b656aa",
                              "width": 960
                          },
                          {
                              "height": 498,
                              "url": "https://preview.redd.it/m04l2msmfsz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=8e97856ead32fdc9aaad2b82480e0733be786fc1",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1125,
                          "url": "https://preview.redd.it/m04l2msmfsz81.jpg?auto=webp&amp;s=8574a0bcc27e7b6a29da3ba800ed4ff55edb55cd",
                          "width": 2436
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652685054,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142726,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/mPHDFWpyiNbVfN-g6h7rA420_JN0WEgzz-e-8_KZ7Ug.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "How to be lucky and unlucky at the same time",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/m04l2msmfsz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/m04l2msmfsz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "cornerkeydoor",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_9in887t4",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652676172,
          "domain": "vm.tiktok.com",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqnu2l/lilibet_edit_sorry_a_little_rushed_to_finish/",
          "gildings": {},
          "id": "uqnu2l",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media": {
              "oembed": {
                  "description": "TikTok video from twush (@twush_): \"Lilibet edit - Pour Up #epicseven #epicsevenedit #e7 #lilibet #designerlilibet #edit #fyp\". original sound.",
                  "height": 700,
                  "html": "&lt;iframe class=\"embedly-embed\" src=\"https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.tiktok.com%2Fembed%2Fv2%2F7098012312237362478&amp;display_name=tiktok&amp;url=https%3A%2F%2Fwww.tiktok.com%2F%40twush_%2Fvideo%2F7098012312237362478&amp;image=https%3A%2F%2Fp16-sign.tiktokcdn-us.com%2Ftos-useast5-p-0068-tx%2Fb5ababe5095d40ea9093f1967b14423d_1652634777%7Etplv-tiktok-play.jpeg%3Fx-expires%3D1653278400%26x-signature%3DZvAwHCL8fQFLcsu2glTOGHF4yvU%253D&amp;key=ed8fa8699ce04833838e66ce79ba05f1&amp;type=text%2Fhtml&amp;schema=tiktok\" width=\"340\" height=\"700\" scrolling=\"no\" title=\"tiktok embed\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen=\"true\"&gt;&lt;/iframe&gt;",
                  "provider_name": "tiktok",
                  "provider_url": "http://www.tiktok.com",
                  "thumbnail_height": 540,
                  "thumbnail_url": "https://p16-sign.tiktokcdn-us.com/tos-useast5-p-0068-tx/b5ababe5095d40ea9093f1967b14423d_1652634777~tplv-tiktok-play.jpeg?x-expires=1653278400&amp;x-signature=ZvAwHCL8fQFLcsu2glTOGHF4yvU%3D",
                  "thumbnail_width": 540,
                  "title": "twush on TikTok",
                  "type": "rich",
                  "version": "1.0",
                  "width": 340
              },
              "type": "vm.tiktok.com"
          },
          "media_embed": {
              "content": "&lt;iframe class=\"embedly-embed\" src=\"https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.tiktok.com%2Fembed%2Fv2%2F7098012312237362478&amp;display_name=tiktok&amp;url=https%3A%2F%2Fwww.tiktok.com%2F%40twush_%2Fvideo%2F7098012312237362478&amp;image=https%3A%2F%2Fp16-sign.tiktokcdn-us.com%2Ftos-useast5-p-0068-tx%2Fb5ababe5095d40ea9093f1967b14423d_1652634777%7Etplv-tiktok-play.jpeg%3Fx-expires%3D1653278400%26x-signature%3DZvAwHCL8fQFLcsu2glTOGHF4yvU%253D&amp;key=ed8fa8699ce04833838e66ce79ba05f1&amp;type=text%2Fhtml&amp;schema=tiktok\" width=\"340\" height=\"700\" scrolling=\"no\" title=\"tiktok embed\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen=\"true\"&gt;&lt;/iframe&gt;",
              "height": 700,
              "scrolling": false,
              "width": 340
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqnu2l/lilibet_edit_sorry_a_little_rushed_to_finish/",
          "pinned": false,
          "post_hint": "link",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "HRyXUkpM0Z9NDStgQID0M81vfixbeJEllqHWR6syHWs",
                      "resolutions": [
                          {
                              "height": 108,
                              "url": "https://external-preview.redd.it/Ra1Y9oR4nxYi67qTXhap0kdoGFxuYoO2r2keggQZhMw.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=ab188e7baa922ce8e8550b8555135cb5e0331086",
                              "width": 108
                          },
                          {
                              "height": 216,
                              "url": "https://external-preview.redd.it/Ra1Y9oR4nxYi67qTXhap0kdoGFxuYoO2r2keggQZhMw.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=48b6bdb72b46585df814c19e07e8e6deefd8e311",
                              "width": 216
                          },
                          {
                              "height": 320,
                              "url": "https://external-preview.redd.it/Ra1Y9oR4nxYi67qTXhap0kdoGFxuYoO2r2keggQZhMw.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=2706bda691dd226398d3b1230ec634cc7a26f49e",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 540,
                          "url": "https://external-preview.redd.it/Ra1Y9oR4nxYi67qTXhap0kdoGFxuYoO2r2keggQZhMw.jpg?auto=webp&amp;s=75de834030d9cb7009eacbea90404f8f277cc8c4",
                          "width": 540
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652676182,
          "score": 1,
          "secure_media": {
              "oembed": {
                  "description": "TikTok video from twush (@twush_): \"Lilibet edit - Pour Up #epicseven #epicsevenedit #e7 #lilibet #designerlilibet #edit #fyp\". original sound.",
                  "height": 700,
                  "html": "&lt;iframe class=\"embedly-embed\" src=\"https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.tiktok.com%2Fembed%2Fv2%2F7098012312237362478&amp;display_name=tiktok&amp;url=https%3A%2F%2Fwww.tiktok.com%2F%40twush_%2Fvideo%2F7098012312237362478&amp;image=https%3A%2F%2Fp16-sign.tiktokcdn-us.com%2Ftos-useast5-p-0068-tx%2Fb5ababe5095d40ea9093f1967b14423d_1652634777%7Etplv-tiktok-play.jpeg%3Fx-expires%3D1653278400%26x-signature%3DZvAwHCL8fQFLcsu2glTOGHF4yvU%253D&amp;key=ed8fa8699ce04833838e66ce79ba05f1&amp;type=text%2Fhtml&amp;schema=tiktok\" width=\"340\" height=\"700\" scrolling=\"no\" title=\"tiktok embed\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen=\"true\"&gt;&lt;/iframe&gt;",
                  "provider_name": "tiktok",
                  "provider_url": "http://www.tiktok.com",
                  "thumbnail_height": 540,
                  "thumbnail_url": "https://p16-sign.tiktokcdn-us.com/tos-useast5-p-0068-tx/b5ababe5095d40ea9093f1967b14423d_1652634777~tplv-tiktok-play.jpeg?x-expires=1653278400&amp;x-signature=ZvAwHCL8fQFLcsu2glTOGHF4yvU%3D",
                  "thumbnail_width": 540,
                  "title": "twush on TikTok",
                  "type": "rich",
                  "version": "1.0",
                  "width": 340
              },
              "type": "vm.tiktok.com"
          },
          "secure_media_embed": {
              "content": "&lt;iframe class=\"embedly-embed\" src=\"https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.tiktok.com%2Fembed%2Fv2%2F7098012312237362478&amp;display_name=tiktok&amp;url=https%3A%2F%2Fwww.tiktok.com%2F%40twush_%2Fvideo%2F7098012312237362478&amp;image=https%3A%2F%2Fp16-sign.tiktokcdn-us.com%2Ftos-useast5-p-0068-tx%2Fb5ababe5095d40ea9093f1967b14423d_1652634777%7Etplv-tiktok-play.jpeg%3Fx-expires%3D1653278400%26x-signature%3DZvAwHCL8fQFLcsu2glTOGHF4yvU%253D&amp;key=ed8fa8699ce04833838e66ce79ba05f1&amp;type=text%2Fhtml&amp;schema=tiktok\" width=\"340\" height=\"700\" scrolling=\"no\" title=\"tiktok embed\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen=\"true\"&gt;&lt;/iframe&gt;",
              "height": 700,
              "media_domain_url": "https://www.redditmedia.com/mediaembed/uqnu2l",
              "scrolling": false,
              "width": 340
          },
          "selftext": "",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142725,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/L8RQuAdMx49Yj2o2CQpxjgllSY3osn346jlqTBoYOw4.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Lilibet Edit sorry a little rushed to finish",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://vm.tiktok.com/ZTdGdJmRC/?k=1",
          "url_overridden_by_dest": "https://vm.tiktok.com/ZTdGdJmRC/?k=1",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "OzieteRed",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_17736b",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652674214,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqnba2/i_might_use_this_for_copium_acidd/",
          "gildings": {},
          "id": "uqnba2",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqnba2/i_might_use_this_for_copium_acidd/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "qUw6zrqkV6T0yONJBDcmRXNlhfUY26KcpAj3XOoOKeg",
                      "resolutions": [
                          {
                              "height": 50,
                              "url": "https://preview.redd.it/zhtfpm37jrz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=4180e9eee79b0b46500886f1f95d494952df05c7",
                              "width": 108
                          },
                          {
                              "height": 101,
                              "url": "https://preview.redd.it/zhtfpm37jrz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=4e66eb49b0886f41b47c07b2fe5f2492de10b7c8",
                              "width": 216
                          },
                          {
                              "height": 150,
                              "url": "https://preview.redd.it/zhtfpm37jrz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=14681836dbf2113a651f7c89c0640560173ea887",
                              "width": 320
                          },
                          {
                              "height": 300,
                              "url": "https://preview.redd.it/zhtfpm37jrz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=2f7ac5c2cb502db1d0865200c2044e7dd9f6bdd1",
                              "width": 640
                          },
                          {
                              "height": 450,
                              "url": "https://preview.redd.it/zhtfpm37jrz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=4294634b1e5a2260e56a17485a0a32c50f56f22f",
                              "width": 960
                          },
                          {
                              "height": 506,
                              "url": "https://preview.redd.it/zhtfpm37jrz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=d5111346d9fa11d0155badad7084ac8539553e4e",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 600,
                          "url": "https://preview.redd.it/zhtfpm37jrz81.png?auto=webp&amp;s=41b7372c964549df31021eae97dc58a623208f7d",
                          "width": 1280
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652674225,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142721,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/YU23cpBH5MEqkrebacgQp-79DFnKa5_s_BIZnkqwtAI.jpg",
          "thumbnail_height": 65,
          "thumbnail_width": 140,
          "title": "I might use this for copium A.Cidd",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/zhtfpm37jrz81.png",
          "url_overridden_by_dest": "https://i.redd.it/zhtfpm37jrz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "YoMikeeHey",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "one-achates",
          "author_flair_richtext": [
              {
                  "e": "text",
                  "t": "Release Saskia and Paradoxia, SG!"
              }
          ],
          "author_flair_template_id": "33ca2992-f53e-11e8-9b8e-0e463599d056",
          "author_flair_text": "Release Saskia and Paradoxia, SG!",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_11mysv",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652670994,
          "domain": "i.imgur.com",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqmf8b/luluca_art_by_rin_ateria/",
          "gildings": {},
          "id": "uqmf8b",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqmf8b/luluca_art_by_rin_ateria/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "xGsm4QvKFt1gq6uDUwkljb70ximulO6cD8ulCXbyuwY",
                      "resolutions": [
                          {
                              "height": 152,
                              "url": "https://external-preview.redd.it/MYapc9xiYlbvQf4QUtC4YBi-oGKjPhxn27J15NO5G5U.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=ab8014ecb7e6bd40c18f46a0f37ce6ca3e3b3c2c",
                              "width": 108
                          },
                          {
                              "height": 304,
                              "url": "https://external-preview.redd.it/MYapc9xiYlbvQf4QUtC4YBi-oGKjPhxn27J15NO5G5U.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=2169fe7d26b6a9dfd7b78eadc8a10aef60f2fe2b",
                              "width": 216
                          },
                          {
                              "height": 451,
                              "url": "https://external-preview.redd.it/MYapc9xiYlbvQf4QUtC4YBi-oGKjPhxn27J15NO5G5U.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=dc8bea76776529c6f0721594fe4a776fa63e702b",
                              "width": 320
                          },
                          {
                              "height": 903,
                              "url": "https://external-preview.redd.it/MYapc9xiYlbvQf4QUtC4YBi-oGKjPhxn27J15NO5G5U.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=3e12f2132d3c423b4c525686163e2785cf725bca",
                              "width": 640
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://external-preview.redd.it/MYapc9xiYlbvQf4QUtC4YBi-oGKjPhxn27J15NO5G5U.jpg?auto=webp&amp;s=da984f803063bbe8c6cfedd31b574970b5ab4d20",
                          "width": 765
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652671004,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142722,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/SCYH0rnLlo9Ey5ygLQ2cZ2qFVLAagdmucexoAhDHQCU.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Luluca [art by rin_ateria]",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.imgur.com/97BmYET.jpg",
          "url_overridden_by_dest": "https://i.imgur.com/97BmYET.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "leedr95",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_z0ara",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": true,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652667144,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqlaeb/whats_the_best_artifact_for_my_vildred_also_build/",
          "gildings": {},
          "id": "uqlaeb",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqlaeb/whats_the_best_artifact_for_my_vildred_also_build/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "7C49EdDMd7Jme_-I-Vn1T-7JnDjsoe1Af9jRGPujaA8",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/gsprnpeeyqz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=060cfb1ffb637934b1c432c5886124ce88985a67",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/gsprnpeeyqz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=165c21624850f50829c87c0aefea4b1f865d053e",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/gsprnpeeyqz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=69c3e3ac88da555c32995ae9a8f86ec4513e040b",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/gsprnpeeyqz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=62d7e142c0db615ca0f369ad6c57824ce2ed441c",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/gsprnpeeyqz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=3f3dc27196faeb217a28ef0b346dbd0a1c1a949f",
                              "width": 960
                          },
                          {
                              "height": 499,
                              "url": "https://preview.redd.it/gsprnpeeyqz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=05cdf7e04dadd5ace4dc0634ce374d332d63758b",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1242,
                          "url": "https://preview.redd.it/gsprnpeeyqz81.jpg?auto=webp&amp;s=d01e2326178503b2bbdc61b8d7bc05f140cf2d31",
                          "width": 2688
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652667154,
          "score": 1,
          "selftext": "",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142720,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/_twBrmciEzmQMH-4uNWrnvyVcGYN09xEQFSpqfEKWug.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "What\u2019s the best artifact for my Vildred? Also build improvements are welcome!",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/gsprnpeeyqz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/gsprnpeeyqz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "fuzz_wolfe",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_80t74fdb",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652666272,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uql0tf/epic_7_need_advice/",
          "gildings": {},
          "id": "uql0tf",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uql0tf/epic_7_need_advice/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "lkQ9Gkci8YmfRjJyfYZb6Kaj522x24yn62vMi49GKPY",
                      "resolutions": [
                          {
                              "height": 60,
                              "url": "https://preview.redd.it/rmndwxpsvqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=6a2b4238ab4ede151aaee97d1f2fade451fbbe4d",
                              "width": 108
                          },
                          {
                              "height": 121,
                              "url": "https://preview.redd.it/rmndwxpsvqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=86100b79b36f666b84d3954aba7af0747329ac74",
                              "width": 216
                          },
                          {
                              "height": 180,
                              "url": "https://preview.redd.it/rmndwxpsvqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=549827504b1cf7664e189b389d5733a2788ef1bf",
                              "width": 320
                          },
                          {
                              "height": 360,
                              "url": "https://preview.redd.it/rmndwxpsvqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=7c111c8d16889dc435ded7268674f7433551ddec",
                              "width": 640
                          },
                          {
                              "height": 540,
                              "url": "https://preview.redd.it/rmndwxpsvqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=eafd51dd9c77d5ca1153965260c6f1e969bd69f8",
                              "width": 960
                          },
                          {
                              "height": 607,
                              "url": "https://preview.redd.it/rmndwxpsvqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=323afead1a3cec5a0c95c9d056a80d5a17f5491e",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/rmndwxpsvqz81.png?auto=webp&amp;s=0c8b391adc669e1eb3e3d92b1f2f21e37554830d",
                          "width": 1920
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652666283,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142721,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/trwLLUyQNjwovejCDEbVg_PPdXFnG6LLwg30P8s38E4.jpg",
          "thumbnail_height": 78,
          "thumbnail_width": 140,
          "title": "EPIC 7.... Need Advice",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/rmndwxpsvqz81.png",
          "url_overridden_by_dest": "https://i.redd.it/rmndwxpsvqz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Equivalent-Cattle-29",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_2xg6bsz9",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652665920,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqkwzq/speed_question/",
          "gildings": {},
          "id": "uqkwzq",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqkwzq/speed_question/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652665930,
          "score": 1,
          "selftext": "How would yall feel if sg put a cap on speed\nJust like Crit damage and Dual attack chance?",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142721,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Speed question",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqkwzq/speed_question/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "__Sn__",
          "author_flair_background_color": "#d3d6da",
          "author_flair_css_class": "two-mercedes",
          "author_flair_richtext": [
              {
                  "a": ":mercedes:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/m54j96eyd1s51_t5_nrn6j/mercedes"
              }
          ],
          "author_flair_template_id": "6938fd68-f6e9-11e8-b78f-0e16cd360ad0",
          "author_flair_text": ":mercedes:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_5fxmpf49",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652662851,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqk09s/you_peeking_at_karin/",
          "gildings": {},
          "id": "uqk09s",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqk09s/you_peeking_at_karin/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "QfQhKJkhxHnrQmQAjxKA6s45v6w4LpvDjU92TyBnjqk",
                      "resolutions": [
                          {
                              "height": 108,
                              "url": "https://preview.redd.it/jtppu4tjlqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=43b316a157e9ca7f45eaca772c4a2b2989da8ec3",
                              "width": 108
                          },
                          {
                              "height": 216,
                              "url": "https://preview.redd.it/jtppu4tjlqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=c79c19c80732061d13f721eba7387d6bf0ef8e74",
                              "width": 216
                          },
                          {
                              "height": 320,
                              "url": "https://preview.redd.it/jtppu4tjlqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=758ec4c227a7a7d9ec2bb698802e0448e5350a98",
                              "width": 320
                          },
                          {
                              "height": 640,
                              "url": "https://preview.redd.it/jtppu4tjlqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=85a201ae88131f1edcab1d49d5b505116954b5a8",
                              "width": 640
                          },
                          {
                              "height": 960,
                              "url": "https://preview.redd.it/jtppu4tjlqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=3ef38af9da53a15cf8c3ebd409dc5874e7a6b01d",
                              "width": 960
                          },
                          {
                              "height": 1080,
                              "url": "https://preview.redd.it/jtppu4tjlqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=7111b121b03154d9b1d8aaf8f51c88a785ca6fb7",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 3900,
                          "url": "https://preview.redd.it/jtppu4tjlqz81.png?auto=webp&amp;s=2fae5eb0ea1465a99fac21a997e1eeee638bb9b4",
                          "width": 3900
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652662862,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142722,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/iYNIHqbTV1xYtKRTPcjCSGE_VRcDsg_IeCFk6ny9g6U.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "You peeking at Karin",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/jtppu4tjlqz81.png",
          "url_overridden_by_dest": "https://i.redd.it/jtppu4tjlqz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "JamesWarlord",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_15c6dad",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652661898,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqjq5w/at_last_i_can_sit_in_the_chair_epic7_vr/",
          "gildings": {},
          "id": "uqjq5w",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqjq5w/at_last_i_can_sit_in_the_chair_epic7_vr/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "JnDsnXTcpsrn1p7PAPniTaP2F8b7ngotW7krTvkIZh0",
                      "resolutions": [
                          {
                              "height": 67,
                              "url": "https://preview.redd.it/y2nfljfiiqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=de1d0284e52d9534fc9fd867defedade16b1ee72",
                              "width": 108
                          },
                          {
                              "height": 135,
                              "url": "https://preview.redd.it/y2nfljfiiqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=4f36b26edf1bd99c1b384ff0c4f0c0fe0f2e7345",
                              "width": 216
                          },
                          {
                              "height": 200,
                              "url": "https://preview.redd.it/y2nfljfiiqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=a0ffcd3520909cde75186d4aa949bfac9d15ffcd",
                              "width": 320
                          },
                          {
                              "height": 401,
                              "url": "https://preview.redd.it/y2nfljfiiqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=f3e5471b677632942f6a3fcf2f500480c2b0fe25",
                              "width": 640
                          },
                          {
                              "height": 602,
                              "url": "https://preview.redd.it/y2nfljfiiqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=c3b8ae871a63420d8438579d16feb87bf407c728",
                              "width": 960
                          }
                      ],
                      "source": {
                          "height": 618,
                          "url": "https://preview.redd.it/y2nfljfiiqz81.png?auto=webp&amp;s=14c65038a4a0efbb9a696ff5ee6d8b913692a8e1",
                          "width": 984
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652661909,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142722,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/wa-tBt9oUh-7fHxzqJ8ksS1p4B0ImSIFGA5sOVph9gg.jpg",
          "thumbnail_height": 87,
          "thumbnail_width": 140,
          "title": "At last, I can sit in the chair [Epic7 VR]",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/y2nfljfiiqz81.png",
          "url_overridden_by_dest": "https://i.redd.it/y2nfljfiiqz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Ukemaster24",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_e846vels",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652659885,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqj54l/noob_tips/",
          "gildings": {},
          "id": "uqj54l",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqj54l/noob_tips/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652659896,
          "score": 1,
          "selftext": "Hi! I have played before but just came back to this game! I love the animations and the style of it! I would love some tips for returning players!",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142723,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Noob tips!",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqj54l/noob_tips/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Terminal_Ethos",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_b8lfzeb2",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652659491,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqj0uu/helpresources_desperately_needed/",
          "gildings": {},
          "id": "uqj0uu",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_metadata": {
              "4542z9ozaqz81": {
                  "e": "Image",
                  "id": "4542z9ozaqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/4542z9ozaqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=77ea7837c90e3b87fbf4c60a53daab08e7110d26",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/4542z9ozaqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=13a4294e68c088d107630d2c99f1c1df8112e287",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/4542z9ozaqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=032d1296e49cf76f9b85a7f91d74f1549ea0b560",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/4542z9ozaqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=4477a086ec49b4f7db25198fe1e1989c603e86d2",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/4542z9ozaqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=580d87621931b203a9a691be1fa333f39b263c94",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/4542z9ozaqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=1a13db5f69b7defd212e56b530eefe7312e78f12",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/4542z9ozaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=6534466ff4f7790d616b2256a6cc5d354ebbbc4b",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "7fk15qthaqz81": {
                  "e": "Image",
                  "id": "7fk15qthaqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/7fk15qthaqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=953db9b1305c7d909959923f83015db5106be028",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/7fk15qthaqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=bc11d417fc4b833d8a42516ed6f658952e97e0ee",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/7fk15qthaqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=ddce1b5d8594a969fdcd614b1204e5ac12e1afd4",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/7fk15qthaqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=21ce60465fe6dd79969a92327807d9dafa043025",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/7fk15qthaqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=8b5ef9de4662e51891dd3f2d73af0d7d47ce0ad0",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/7fk15qthaqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=bcbffd0b019943420a0df8b2500a7fbbecad584c",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/7fk15qthaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=f8660a226bad4985300c66e4537e79a4c9b8d74c",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "c1ljffc2bqz81": {
                  "e": "Image",
                  "id": "c1ljffc2bqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/c1ljffc2bqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=573735f349bcd2d417bb7fafce996188cdcebf03",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/c1ljffc2bqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=60e5e451469ef8ef4da9b48cbb511fbda9365ca7",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/c1ljffc2bqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=7ee68a74033795b473fed4cc90918b491b36612a",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/c1ljffc2bqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=86708668e57c766c03aaa01b4b7fb4234f5fa2aa",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/c1ljffc2bqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=5041391bba7a0a982b60795c1760c43e9b67766c",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/c1ljffc2bqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=711a37f548abf54923a44dd45b767dfbf6d45952",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/c1ljffc2bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=2f295656ac2e36ec41e454efdf3c663a93042017",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "frl97v97bqz81": {
                  "e": "Image",
                  "id": "frl97v97bqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/frl97v97bqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=a6f65c5fc59d650700cb588a771515d9a5ac15ba",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/frl97v97bqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=6bf30400b4463085c41e17bc5006033af80efd5d",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/frl97v97bqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=0addf032fb16dd039a3653c538ee42be09ee2f04",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/frl97v97bqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=392d44e54254a5e0abdf90ed1ca67fcca6d68d87",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/frl97v97bqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=4ab5d490782bff24f9a64163ca17a3bab98a04e1",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/frl97v97bqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=a2de45884f2e4b9b2cce1f48538facec4693c849",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/frl97v97bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=15f7b1f9839409a105a8f04e1df324e8c9ad4225",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "iup6gag5bqz81": {
                  "e": "Image",
                  "id": "iup6gag5bqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/iup6gag5bqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=1f6518242e88065002ee96bdc9c0b1e76e5ca925",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/iup6gag5bqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=46832865bd5b22346e1b24804f8312b2cc8a768e",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/iup6gag5bqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=19df6c08764cf190291238bd57e8eb7b549954e7",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/iup6gag5bqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=901620d82f1ceb5e2252481f129bfc29034e05fe",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/iup6gag5bqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=4d8cebcae51e25beac2593b7dac031f26214cf5f",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/iup6gag5bqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=a5c2b126371b3e6eaed9c02d5a74c29a8040370f",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/iup6gag5bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=2ace40f39d4fcdf7a82792b2759bfdea364e7cec",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "kecht9ldaqz81": {
                  "e": "Image",
                  "id": "kecht9ldaqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/kecht9ldaqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=8088f3872efe381152a6f194188a905f2e291021",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/kecht9ldaqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=40b3c395939f2b40c5e3caa7e082c0fbd3cc5747",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/kecht9ldaqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=80febd16fea19ce20ccf3b35f6e89244727b38f4",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/kecht9ldaqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=9f0da37bd559a5b95d768ec269706d49a2a78d2a",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/kecht9ldaqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=c769f06267df869d6978d0b2070063964631cf61",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/kecht9ldaqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=daf61b60ce5b47aae9e7e415f8caf8f996057f9a",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/kecht9ldaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=56b0c76e7c867a1b8b89c19547bdbaec164fdd77",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "m6h4x4osaqz81": {
                  "e": "Image",
                  "id": "m6h4x4osaqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/m6h4x4osaqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=9ec0bc8e97ebfc2e97eb9c772e68bb52b3fe8a38",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/m6h4x4osaqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=f3184fd9a1799916cf6cff90dc0fffa2af87d678",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/m6h4x4osaqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=91793f9ab0817fa14e68d8ee6e4e03c77f763d8e",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/m6h4x4osaqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=6fa190d79ffbc7c0b2d7ab20a564a0f4a7f84d0c",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/m6h4x4osaqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=d136f0069086c92452221809c1fdff5949273436",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/m6h4x4osaqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=51182292b7e296e63ecaafd406eee9e17a9bcb00",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/m6h4x4osaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=2c6d6cd70c82c9be7d6284c9e14349f0d2e78124",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "mpu20jivaqz81": {
                  "e": "Image",
                  "id": "mpu20jivaqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/mpu20jivaqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=8369b69c85f131ee683c8d106449d790d76680c1",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/mpu20jivaqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=217dc739d671b2c24189c9b3eec325c8b80a2186",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/mpu20jivaqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=9488c362ed6617b3ab20a624300334b7d2fdd8a4",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/mpu20jivaqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=44c9b13a0285e9df4e23f6d5d453792e1816378b",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/mpu20jivaqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=bfba779193e175ea3945cadf30d15fa8b0ae465a",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/mpu20jivaqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=7978e4050022b542cccdb049033945c10569e47a",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/mpu20jivaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=34b1a715fcc98a71f74946043d89a9787fe8ead5",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "rd7dako3bqz81": {
                  "e": "Image",
                  "id": "rd7dako3bqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/rd7dako3bqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=d2244a5e227b73ee68de7c84d65acef09478173d",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/rd7dako3bqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=463e82f4bf6ec703b4bb026414c7421afcfb2624",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/rd7dako3bqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=7a2f87308764b37161b27f35ca0f7bf3b3cd4c05",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/rd7dako3bqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=21ed8f64155dc2ceebf163b933735bc5b55dafe5",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/rd7dako3bqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=bf7bec37526655d585e50bfdabdc2bac05ac0121",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/rd7dako3bqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=0c77b2a9e61f75a1e9996ab2f89277188efc6238",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/rd7dako3bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=be8b6aa5a44c3d253e5359becb07b541953d9781",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              },
              "y3su5icyaqz81": {
                  "e": "Image",
                  "id": "y3su5icyaqz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/y3su5icyaqz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=49ae5e870488b564aafeb04dc64e9e689f0f27f3",
                          "x": 108,
                          "y": 61
                      },
                      {
                          "u": "https://preview.redd.it/y3su5icyaqz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=4cbbbdb5eebbc888c72328e0b010f58ae8940a25",
                          "x": 216,
                          "y": 123
                      },
                      {
                          "u": "https://preview.redd.it/y3su5icyaqz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=119fdfde15f899b086601fa274b1f581734e43dc",
                          "x": 320,
                          "y": 183
                      },
                      {
                          "u": "https://preview.redd.it/y3su5icyaqz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=6143162c5c765ce71d2d1893f10748a6eef5789d",
                          "x": 640,
                          "y": 366
                      },
                      {
                          "u": "https://preview.redd.it/y3su5icyaqz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=ef934ff7ba98b702d0734976f8698bd513f80b03",
                          "x": 960,
                          "y": 549
                      },
                      {
                          "u": "https://preview.redd.it/y3su5icyaqz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=6d834a63bbd8246c503ba3e347005a694b75db6b",
                          "x": 1080,
                          "y": 618
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/y3su5icyaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=b347381feefd9dfe05b51a8c0301aa29e44f6ae9",
                      "x": 1453,
                      "y": 832
                  },
                  "status": "valid"
              }
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqj0uu/helpresources_desperately_needed/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652659502,
          "score": 1,
          "selftext": "Hello. This is my first time posting a thread here. I apologize if I've broken any rules, or community standards.\n\nAnyway, I've been playing E7 since it came out. I've been quite casual, and have taken several long breaks. Most of what I've done is just log in daily to get my free pull. Maybe do some daily quests and that's about it.\n\nThis time, though, I've been wanting to do more with the game and I'm completely overwhelmed. There are so many systems I don't understand, and I've had bad luck finding resources that are up to date and reliable (and understandable). \n\nI've been completely F2P. I haven't spent a single cent on this game.   \nI am trying to build teams for Wyvern and Banshee (I keep reading those are the two hunts I should farm?) I feel like I have a pretty good assortment of characters available, currently at 151/254. I just finished 2-10 and haven't started on Episode 3 yet.\n\nAnyway, enough preamble (sorry I know I'm rambling). Can I get some team suggestions, please? Also how are people getting such good high level gear? I have no idea how to get level 90 stuff. I feel so lost.\n\nany help you guys can suggest I would be grateful for. Thank you in advance.\n\nI am missing a handful of 3-star characters:  \n\n\nhttps://preview.redd.it/kecht9ldaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=56b0c76e7c867a1b8b89c19547bdbaec164fdd77\n\nhttps://preview.redd.it/7fk15qthaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=f8660a226bad4985300c66e4537e79a4c9b8d74c\n\nI own all the rest.  \n\n\nThese are all the 4 and 5 star characters I own:  \n\n\nhttps://preview.redd.it/m6h4x4osaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=2c6d6cd70c82c9be7d6284c9e14349f0d2e78124\n\nhttps://preview.redd.it/mpu20jivaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=34b1a715fcc98a71f74946043d89a9787fe8ead5\n\nhttps://preview.redd.it/y3su5icyaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=b347381feefd9dfe05b51a8c0301aa29e44f6ae9\n\nhttps://preview.redd.it/4542z9ozaqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=6534466ff4f7790d616b2256a6cc5d354ebbbc4b\n\nhttps://preview.redd.it/c1ljffc2bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=2f295656ac2e36ec41e454efdf3c663a93042017\n\nhttps://preview.redd.it/rd7dako3bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=be8b6aa5a44c3d253e5359becb07b541953d9781\n\nhttps://preview.redd.it/iup6gag5bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=2ace40f39d4fcdf7a82792b2759bfdea364e7cec\n\nhttps://preview.redd.it/frl97v97bqz81.png?width=1453&amp;format=png&amp;auto=webp&amp;s=15f7b1f9839409a105a8f04e1df324e8c9ad4225",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142723,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/EUz-OAgwbH7cZzfbzNF4pdNQDj4hQLtK4K4-8_0-N-o.jpg",
          "thumbnail_height": 80,
          "thumbnail_width": 140,
          "title": "Help/Resources desperately needed",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqj0uu/helpresources_desperately_needed/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "eboonge",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_ft33ate6",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652657469,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqif09/custom_mystic_summon_problems/",
          "gildings": {},
          "id": "uqif09",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": false,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqif09/custom_mystic_summon_problems/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "Ox8pT_qDbDlqfbF2TbJnTkVPoVO20MizmB0c3JJ7QrI",
                      "resolutions": [
                          {
                              "height": 216,
                              "url": "https://preview.redd.it/7upc1wgc5qz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=4875b3f6796e462e1c85b61f278fa78f8fc75c26",
                              "width": 108
                          },
                          {
                              "height": 432,
                              "url": "https://preview.redd.it/7upc1wgc5qz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=d60ba3b9181dd85f7adc38b0906602842b126357",
                              "width": 216
                          },
                          {
                              "height": 640,
                              "url": "https://preview.redd.it/7upc1wgc5qz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=a63a2ad2ac3c6afcebb1cf3e5a1537aef4925d4b",
                              "width": 320
                          },
                          {
                              "height": 1280,
                              "url": "https://preview.redd.it/7upc1wgc5qz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=4c7a2131e83c55105be7258bab613fbf6217a89f",
                              "width": 640
                          }
                      ],
                      "source": {
                          "height": 8362,
                          "url": "https://preview.redd.it/7upc1wgc5qz81.jpg?auto=webp&amp;s=980601da7d64321206ec9e2b7af428f1bd3e36f2",
                          "width": 868
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "removed_by_category": "automod_filtered",
          "retrieved_on": 1652657480,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142724,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/QKS-CKOl6B-yZKVM8P5o8Ai5ZCjAez-TLAWJd54ewyA.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Custom Mystic Summon problems",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/7upc1wgc5qz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/7upc1wgc5qz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Mysto101",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_eoa5nglc",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652656649,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqi605/scripted/",
          "gildings": {},
          "id": "uqi605",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqi605/scripted/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "-W9xCIOX-9saarDTltOOsARRBFjW8goMeRRYL2WtKFQ",
                      "resolutions": [
                          {
                              "height": 144,
                              "url": "https://preview.redd.it/qc1vrauv2qz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=ff11a0c41760e0ed964246f59ef8323cf87fdb43",
                              "width": 108
                          },
                          {
                              "height": 289,
                              "url": "https://preview.redd.it/qc1vrauv2qz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=1381cf5cbea6fb59c826e656efeca31ee39588ea",
                              "width": 216
                          },
                          {
                              "height": 429,
                              "url": "https://preview.redd.it/qc1vrauv2qz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=3c888730045ed6b2d849f0607bd5fe2bae6d8fb3",
                              "width": 320
                          },
                          {
                              "height": 858,
                              "url": "https://preview.redd.it/qc1vrauv2qz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=9d24ceef741db2791aca586cdb775e22513f192c",
                              "width": 640
                          }
                      ],
                      "source": {
                          "height": 1079,
                          "url": "https://preview.redd.it/qc1vrauv2qz81.png?auto=webp&amp;s=17632c7be883d864fb97dc38f4bd39e6100b79e6",
                          "width": 804
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652656660,
          "score": 1,
          "selftext": "",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142724,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/3roPHIErOJcDSEC_ssZefDgT7lEm9NKDIEJJ1mE_ZJU.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Scripted",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 0.99,
          "url": "https://i.redd.it/qc1vrauv2qz81.png",
          "url_overridden_by_dest": "https://i.redd.it/qc1vrauv2qz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "WhyYouMadBro_",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_b4za6gk9",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652656464,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqi3y5/im_unlucky_with_summons_so_im_pushing_to_champion/",
          "gildings": {},
          "id": "uqi3y5",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqi3y5/im_unlucky_with_summons_so_im_pushing_to_champion/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "3hqnjHXsf_oBHGXqwyKavxnYHoY8bjdu5VXs98s6DOc",
                      "resolutions": [
                          {
                              "height": 48,
                              "url": "https://preview.redd.it/wed3sjbn2qz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=961397fc55004b0b646e45912061cdf53f6c0d02",
                              "width": 108
                          },
                          {
                              "height": 97,
                              "url": "https://preview.redd.it/wed3sjbn2qz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=5afa95f62b22f5c89f71989582bded5ff9006e72",
                              "width": 216
                          },
                          {
                              "height": 144,
                              "url": "https://preview.redd.it/wed3sjbn2qz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=5bbf67fb3d1299c5c8bbc2dd1972bc68d60a594e",
                              "width": 320
                          },
                          {
                              "height": 288,
                              "url": "https://preview.redd.it/wed3sjbn2qz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=2bab765b141915ab4945908a719ed234a7afd5f9",
                              "width": 640
                          },
                          {
                              "height": 432,
                              "url": "https://preview.redd.it/wed3sjbn2qz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=096fb3da5919ca12f8fb4eb9e38894e5a7757557",
                              "width": 960
                          },
                          {
                              "height": 486,
                              "url": "https://preview.redd.it/wed3sjbn2qz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=76f97e12e77db8a75d8d926a1e5e69c3396d5fbc",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/wed3sjbn2qz81.jpg?auto=webp&amp;s=67d9e8f45cd24b9a2753c53c8033acffd8ca3adc",
                          "width": 2400
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652656475,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142725,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/vPv4CZtMgl8zD-wqazObmbELaeu0r4xbUnpbkQGliN4.jpg",
          "thumbnail_height": 63,
          "thumbnail_width": 140,
          "title": "I'm unlucky with summons so I'm pushing to Champion with my 3* FTP party. 11 win streak, got broken though..",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/wed3sjbn2qz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/wed3sjbn2qz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Bulzeebb",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_ahzzgkqk",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652655410,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqhsaj/seiyuu_in_epic_seven/",
          "gildings": {},
          "id": "uqhsaj",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqhsaj/seiyuu_in_epic_seven/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652655420,
          "score": 1,
          "selftext": "I usually play with JP audio because ~~I'm a weeb~~ it sounds better, and I also like to know if I recognize the seiyuu (JP voice actors) from an anime that I like, etc. However, it's so hard to find Epic Seven seiyuu, I know there is a list here on this sub, but it hasn't been updated. Do you guys know where I can find information on E7 seiyuu? (specifically JP)",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142725,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Seiyuu in Epic Seven",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqhsaj/seiyuu_in_epic_seven/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "GBNDias",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_5ffjhasm",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652655326,
          "domain": "/r/EpicSeven/comments/uqhrf1/what_are_the_odds/",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqhrf1/what_are_the_odds/",
          "gildings": {},
          "id": "uqhrf1",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": true,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqhrf1/what_are_the_odds/",
          "pinned": false,
          "post_hint": "hosted:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "pOu_igZjdxnoBtV89i9yF2uLBDFtIKrTHDzXMQKyWHw",
                      "resolutions": [
                          {
                              "height": 62,
                              "url": "https://external-preview.redd.it/tR23EWSBNRgV7_ILpKh3gvtivkPfSWuvy2io9JA5VM0.png?width=108&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=c011f78166a4305d02061e1340240edd3654b8d7",
                              "width": 108
                          },
                          {
                              "height": 125,
                              "url": "https://external-preview.redd.it/tR23EWSBNRgV7_ILpKh3gvtivkPfSWuvy2io9JA5VM0.png?width=216&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=fac81f5552bb14253690a860ba76761ea6a460df",
                              "width": 216
                          },
                          {
                              "height": 186,
                              "url": "https://external-preview.redd.it/tR23EWSBNRgV7_ILpKh3gvtivkPfSWuvy2io9JA5VM0.png?width=320&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=cfdf23cec925e6b41271836728aa7f72228106fc",
                              "width": 320
                          },
                          {
                              "height": 372,
                              "url": "https://external-preview.redd.it/tR23EWSBNRgV7_ILpKh3gvtivkPfSWuvy2io9JA5VM0.png?width=640&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=b3d51fab462c125f326a01bbd6e972f53eba32ad",
                              "width": 640
                          },
                          {
                              "height": 558,
                              "url": "https://external-preview.redd.it/tR23EWSBNRgV7_ILpKh3gvtivkPfSWuvy2io9JA5VM0.png?width=960&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=eaf2af920abe45600d1e008d9de861a4b930b5b3",
                              "width": 960
                          },
                          {
                              "height": 628,
                              "url": "https://external-preview.redd.it/tR23EWSBNRgV7_ILpKh3gvtivkPfSWuvy2io9JA5VM0.png?width=1080&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=c3ea61cb01338c426f540e6eaf3bb8aa9c79aebc",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://external-preview.redd.it/tR23EWSBNRgV7_ILpKh3gvtivkPfSWuvy2io9JA5VM0.png?format=pjpg&amp;auto=webp&amp;s=5d670d0dc88e7888638ea9eed37ee4530e9a4f59",
                          "width": 1856
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652655337,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142725,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/5kuarLJr3rS-9G9iIpEZDjTxy2LCUaZo_ukEwqeDzlA.jpg",
          "thumbnail_height": 81,
          "thumbnail_width": 140,
          "title": "What are the odds?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://v.redd.it/s9k1urcrypz81",
          "url_overridden_by_dest": "https://v.redd.it/s9k1urcrypz81",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "GBNDias",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_5ffjhasm",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652654960,
          "domain": "/r/EpicSeven/comments/uqhnb8/_/",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqhnb8/_/",
          "gildings": {},
          "id": "uqhnb8",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": true,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqhnb8/_/",
          "pinned": false,
          "post_hint": "hosted:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "C20fMfLv2zDho2bbHLf8FBd-2t1-EA7GrdEYDSBJZg4",
                      "resolutions": [
                          {
                              "height": 62,
                              "url": "https://external-preview.redd.it/Ab7vAZy9ihlvWwWWbqIMbVmEb8VogBPMZVzUf7769mc.png?width=108&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=c4180c69f52304c50ca035d6f92ff5f9833bf987",
                              "width": 108
                          },
                          {
                              "height": 125,
                              "url": "https://external-preview.redd.it/Ab7vAZy9ihlvWwWWbqIMbVmEb8VogBPMZVzUf7769mc.png?width=216&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=1f1f0835ff502df30794095f1caf5f05e8e9a5e6",
                              "width": 216
                          },
                          {
                              "height": 186,
                              "url": "https://external-preview.redd.it/Ab7vAZy9ihlvWwWWbqIMbVmEb8VogBPMZVzUf7769mc.png?width=320&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=b18affa3fbfa227b5605a26866a27812288e680f",
                              "width": 320
                          },
                          {
                              "height": 372,
                              "url": "https://external-preview.redd.it/Ab7vAZy9ihlvWwWWbqIMbVmEb8VogBPMZVzUf7769mc.png?width=640&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=d141b9dbb062cd288c96bd01fa27c75257bcd836",
                              "width": 640
                          },
                          {
                              "height": 558,
                              "url": "https://external-preview.redd.it/Ab7vAZy9ihlvWwWWbqIMbVmEb8VogBPMZVzUf7769mc.png?width=960&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=fafcc292bfff2f0b8176bff11cad5893da10e83c",
                              "width": 960
                          },
                          {
                              "height": 628,
                              "url": "https://external-preview.redd.it/Ab7vAZy9ihlvWwWWbqIMbVmEb8VogBPMZVzUf7769mc.png?width=1080&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=4c2b16619d0673a96084bde3b347a7fcb8ddfb48",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://external-preview.redd.it/Ab7vAZy9ihlvWwWWbqIMbVmEb8VogBPMZVzUf7769mc.png?format=pjpg&amp;auto=webp&amp;s=5eafa18bafe212f91a5fe3473fe7a556c191e38b",
                          "width": 1856
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652654970,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142725,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/RxfFAYqmQKO6oi3kZLXfV3GcJb55vIPe4KA5cCIrwk4.jpg",
          "thumbnail_height": 81,
          "thumbnail_width": 140,
          "title": "....",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://v.redd.it/i1x6xgkzxpz81",
          "url_overridden_by_dest": "https://v.redd.it/i1x6xgkzxpz81",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Legitimate-Chef6677",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_kznjtk4l",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652653548,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqh6ub/build_pls/",
          "gildings": {},
          "id": "uqh6ub",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqh6ub/build_pls/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652653559,
          "score": 1,
          "selftext": "what stats are good for a w13 team with momo, ssb, Muwi and Sigret ?",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142723,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Build pls",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqh6ub/build_pls/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Legitimate-Chef6677",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_kznjtk4l",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652653398,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqh52t/what_stats_are_good_for_a_w13_team_with_momo_ssb/",
          "gildings": {},
          "id": "uqh52t",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": false,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 1,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqh52t/what_stats_are_good_for_a_w13_team_with_momo_ssb/",
          "pinned": false,
          "pwls": 7,
          "removed_by_category": "moderator",
          "retrieved_on": 1652653409,
          "score": 1,
          "selftext": "[removed]",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142723,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "what stats are good for a w13 team with momo, ssb, Muwi and Sigret ?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqh52t/what_stats_are_good_for_a_w13_team_with_momo_ssb/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Unaliver",
          "author_flair_background_color": "#d3d6da",
          "author_flair_css_class": "one-assassincoli",
          "author_flair_richtext": [
              {
                  "a": ":assassin_coli:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/0qzsxqg2d1s51_t5_nrn6j/assassin_coli"
              }
          ],
          "author_flair_template_id": "4e7b101c-f53e-11e8-b9e3-0e1923757b82",
          "author_flair_text": ":assassin_coli:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_p9g62",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652645447,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqee2k/ancient_inheritance_opens_tomorrow_what_are_some/",
          "gildings": {},
          "id": "uqee2k",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqee2k/ancient_inheritance_opens_tomorrow_what_are_some/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652645457,
          "score": 1,
          "selftext": "Last time my guild barely finished floor 3 out of 4 from what I understood:\n\n\\-lvl up warrior class as much as possible\n\n\\-head for the wardens ASAP\n\n\\-dont spend all energy chasing chests everywhere\n\nis there anything else you guys would recommend? The necklace is such an op piece of gear to get I would rly like to get it this time.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142717,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Ancient inheritance opens tomorrow, what are some tips to finish it and get the necklace?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqee2k/ancient_inheritance_opens_tomorrow_what_are_some/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "butterballbuns",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_3g1e3gct",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652643235,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqdm56/here_we_go_again_from_the_latest_chapter/",
          "gildings": {},
          "id": "uqdm56",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqdm56/here_we_go_again_from_the_latest_chapter/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "UDZx45ad1D3TGJzGtNs5RMRivpW8dQZs7W8qZ5bmn2c",
                      "resolutions": [
                          {
                              "height": 121,
                              "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=34ea4907c39e8045c81b3723dbe82f8c957e417b",
                              "width": 108
                          },
                          {
                              "height": 243,
                              "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=90a7e9452d78326f082d8ff43e528c210b49dc9b",
                              "width": 216
                          },
                          {
                              "height": 360,
                              "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=6dfbf7a63e18407de11ad79f56fefabd9708cbd2",
                              "width": 320
                          },
                          {
                              "height": 720,
                              "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=aa164d590c01e47fe99e2dedd38d659f7577db90",
                              "width": 640
                          }
                      ],
                      "source": {
                          "height": 811,
                          "url": "https://preview.redd.it/loynb0q0zoz81.jpg?auto=webp&amp;s=ebc189a499e6c2e9cadde68c786b24c8c690cf8d",
                          "width": 720
                      },
                      "variants": {
                          "obfuscated": {
                              "resolutions": [
                                  {
                                      "height": 121,
                                      "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=108&amp;crop=smart&amp;blur=10&amp;format=pjpg&amp;auto=webp&amp;s=190a26941e828571f64b0ae63760307e22ce6e70",
                                      "width": 108
                                  },
                                  {
                                      "height": 243,
                                      "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=216&amp;crop=smart&amp;blur=21&amp;format=pjpg&amp;auto=webp&amp;s=97cdd20df23823a2488b4ff5d5d1c2f7b011339c",
                                      "width": 216
                                  },
                                  {
                                      "height": 360,
                                      "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=320&amp;crop=smart&amp;blur=32&amp;format=pjpg&amp;auto=webp&amp;s=223626cec5ca97a9d0332e6b5b5074a6cf09d11a",
                                      "width": 320
                                  },
                                  {
                                      "height": 720,
                                      "url": "https://preview.redd.it/loynb0q0zoz81.jpg?width=640&amp;crop=smart&amp;blur=40&amp;format=pjpg&amp;auto=webp&amp;s=eed8b548654c86f4ecafc49e6682ec8e962ba725",
                                      "width": 640
                                  }
                              ],
                              "source": {
                                  "height": 811,
                                  "url": "https://preview.redd.it/loynb0q0zoz81.jpg?blur=40&amp;format=pjpg&amp;auto=webp&amp;s=740cba28c67ec88771d8a1934f0cd9276c67bf57",
                                  "width": 720
                              }
                          }
                      }
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652643246,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": true,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142715,
          "subreddit_type": "public",
          "thumbnail": "spoiler",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Here we go again, from the latest chapter.",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/loynb0q0zoz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/loynb0q0zoz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Horneur",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_7r9oonh7",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652640755,
          "domain": "reddit.com",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqcqe9/hello_so_im_really_lost_in_the_game_after_some/",
          "gallery_data": {
              "items": [
                  {
                      "id": 142518067,
                      "media_id": "3megexguroz81"
                  },
                  {
                      "id": 142518068,
                      "media_id": "oa4ievlvroz81"
                  },
                  {
                      "id": 142518069,
                      "media_id": "p4f1wmdwroz81"
                  },
                  {
                      "id": 142518070,
                      "media_id": "gz6sem4xroz81"
                  }
              ]
          },
          "gildings": {},
          "id": "uqcqe9",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_gallery": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#cc66ff",
          "link_flair_css_class": "tip",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Tips"
              }
          ],
          "link_flair_template_id": "b093f3ee-17bd-11e9-ad8c-0eb3737ae650",
          "link_flair_text": "Tips",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_metadata": {
              "3megexguroz81": {
                  "e": "Image",
                  "id": "3megexguroz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/3megexguroz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=014341b8ff8541476eb1b777b021d5c29461ae6c",
                          "x": 108,
                          "y": 50
                      },
                      {
                          "u": "https://preview.redd.it/3megexguroz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=d695b34755e883582fa1b15ac0a75674569afecd",
                          "x": 216,
                          "y": 100
                      },
                      {
                          "u": "https://preview.redd.it/3megexguroz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=afc9bebe50dd5393ff6e5db820817137f2cfd595",
                          "x": 320,
                          "y": 149
                      },
                      {
                          "u": "https://preview.redd.it/3megexguroz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=539be77bafd93234ca87ad0f8f5375eb6903f7c7",
                          "x": 640,
                          "y": 298
                      },
                      {
                          "u": "https://preview.redd.it/3megexguroz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=7a05e6a8976d5dc9502fbc8a444f238d5715beef",
                          "x": 960,
                          "y": 448
                      },
                      {
                          "u": "https://preview.redd.it/3megexguroz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=25df5f65ff6c523256781ad4b7d7f32a0f4a2b9b",
                          "x": 1080,
                          "y": 504
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/3megexguroz81.jpg?width=2312&amp;format=pjpg&amp;auto=webp&amp;s=2012131095946a0841000481af0fec65bbf51905",
                      "x": 2312,
                      "y": 1080
                  },
                  "status": "valid"
              },
              "gz6sem4xroz81": {
                  "e": "Image",
                  "id": "gz6sem4xroz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/gz6sem4xroz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=c4d2b38503cdd1e2f6f175ee08dd0fdc4d5a2c36",
                          "x": 108,
                          "y": 50
                      },
                      {
                          "u": "https://preview.redd.it/gz6sem4xroz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=556c0381d887781c51f3e78d2adc3ce002971833",
                          "x": 216,
                          "y": 100
                      },
                      {
                          "u": "https://preview.redd.it/gz6sem4xroz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=4c99218e4b418364d96976621b36c6ab33c193e3",
                          "x": 320,
                          "y": 149
                      },
                      {
                          "u": "https://preview.redd.it/gz6sem4xroz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=d5996940ba2174e4283653076127cce2a46fb6bd",
                          "x": 640,
                          "y": 298
                      },
                      {
                          "u": "https://preview.redd.it/gz6sem4xroz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=9bbb37f2f153ff655e7c99faba3ffc6491c5de1d",
                          "x": 960,
                          "y": 448
                      },
                      {
                          "u": "https://preview.redd.it/gz6sem4xroz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=8b3ca2ff011d02a611de73fbe42cb0234f72acfa",
                          "x": 1080,
                          "y": 504
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/gz6sem4xroz81.jpg?width=2312&amp;format=pjpg&amp;auto=webp&amp;s=3619d5b7e8c94c584752266bd635fd0e91766a04",
                      "x": 2312,
                      "y": 1080
                  },
                  "status": "valid"
              },
              "oa4ievlvroz81": {
                  "e": "Image",
                  "id": "oa4ievlvroz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/oa4ievlvroz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=78db9c44ac367d2190e8b82ae677ba1abfc18a10",
                          "x": 108,
                          "y": 50
                      },
                      {
                          "u": "https://preview.redd.it/oa4ievlvroz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=6e50bc810a122bba03befa141a17de6602e58e21",
                          "x": 216,
                          "y": 100
                      },
                      {
                          "u": "https://preview.redd.it/oa4ievlvroz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=8fd0aa0295edde94bfda8ec0d64c36c94d73a02d",
                          "x": 320,
                          "y": 149
                      },
                      {
                          "u": "https://preview.redd.it/oa4ievlvroz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=ae265c06988f3076d17dca55863c7cdd1a4f3b44",
                          "x": 640,
                          "y": 298
                      },
                      {
                          "u": "https://preview.redd.it/oa4ievlvroz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=7f62b59d7c2448cc6cd382a071393cc9c71851ab",
                          "x": 960,
                          "y": 448
                      },
                      {
                          "u": "https://preview.redd.it/oa4ievlvroz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=099e4bf1a4eb5dd2f9f5542baf80f2675c37de1b",
                          "x": 1080,
                          "y": 504
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/oa4ievlvroz81.jpg?width=2312&amp;format=pjpg&amp;auto=webp&amp;s=a9c8369566e27b99400e0a3d8f5bf0eadae0560f",
                      "x": 2312,
                      "y": 1080
                  },
                  "status": "valid"
              },
              "p4f1wmdwroz81": {
                  "e": "Image",
                  "id": "p4f1wmdwroz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/p4f1wmdwroz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=a6e6248c32eeddf725977a3f3434bfb2bee18851",
                          "x": 108,
                          "y": 50
                      },
                      {
                          "u": "https://preview.redd.it/p4f1wmdwroz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=1193d06c88f25b057d6ebafc652b8711f89efd0b",
                          "x": 216,
                          "y": 100
                      },
                      {
                          "u": "https://preview.redd.it/p4f1wmdwroz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=db3e596b413a9a35f0e6690e2289cb581857bacf",
                          "x": 320,
                          "y": 149
                      },
                      {
                          "u": "https://preview.redd.it/p4f1wmdwroz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=59db9aa0bcfca544900ad46ece6d2811fbc44639",
                          "x": 640,
                          "y": 298
                      },
                      {
                          "u": "https://preview.redd.it/p4f1wmdwroz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=fe28e3cdd2eb7309608973c722598bf67b41120e",
                          "x": 960,
                          "y": 448
                      },
                      {
                          "u": "https://preview.redd.it/p4f1wmdwroz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=d6abb21ae4bcd00d55c48ba51855191ece7a9a8b",
                          "x": 1080,
                          "y": 504
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/p4f1wmdwroz81.jpg?width=2312&amp;format=pjpg&amp;auto=webp&amp;s=35761e3777d0bfaee16d4e72d8afa417dac1907e",
                      "x": 2312,
                      "y": 1080
                  },
                  "status": "valid"
              }
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqcqe9/hello_so_im_really_lost_in_the_game_after_some/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652640765,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142714,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/eZvLKCHNM6NQBVyKFatKoek9Yfg4xKgkXfHyJrHxbLc.jpg",
          "thumbnail_height": 65,
          "thumbnail_width": 140,
          "title": "hello, so I'm really lost in the game after some month of playing. here all my character (don't ask Mr why I have so shit stuff). I wanted to know, if anybody would help me in the game? like, what the best team I can do? And wich item on character should I put (I mean what set) ?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/gallery/uqcqe9",
          "url_overridden_by_dest": "https://www.reddit.com/gallery/uqcqe9",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "TtaytheLore",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_50109ihf",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652640051,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqchf5/im_giving_up_on_recovering_my_old_account/",
          "gildings": {},
          "id": "uqchf5",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqchf5/im_giving_up_on_recovering_my_old_account/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652640062,
          "score": 1,
          "selftext": "Today i saw that my account was logged into a month ago, i haven\u2019t been able to log into it in probably a year. With that being said i give up on getting it back as the customer service is completely unhelpful and unresponsive. Been trying to get it back for months now so i decided to just give up. Idk if i should start from scratch or just scrap E7 all together\u2026 I\u2019m just really bummed out. I didn\u2019t even get level 50 yet.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142714,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "I\u2019m giving up on recovering my old account",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqchf5/im_giving_up_on_recovering_my_old_account/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "-VelvetCrowe-",
          "author_flair_background_color": "#d3d6da",
          "author_flair_css_class": "one-bellona",
          "author_flair_richtext": [
              {
                  "a": ":bellona:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/24f601z2d1s51_t5_nrn6j/bellona"
              }
          ],
          "author_flair_template_id": "5f59f1b4-f53e-11e8-a3e0-0e8a28cb03ae",
          "author_flair_text": ":bellona:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_4aq9mux1",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652639194,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqc6n8/tell_us_not_to_use_cermia_without_telling_us_not/",
          "gildings": {},
          "id": "uqc6n8",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqc6n8/tell_us_not_to_use_cermia_without_telling_us_not/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "E82U9PdwJ_KYM5dA0S98PkYG6_dZgQiy6wlT2Q9Y88s",
                      "resolutions": [
                          {
                              "height": 146,
                              "url": "https://preview.redd.it/zs08kroanoz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=ea83501b523f2edc167c988a0777c847ed7172a0",
                              "width": 108
                          },
                          {
                              "height": 292,
                              "url": "https://preview.redd.it/zs08kroanoz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=dbb5baa1372cda791f601e798302bd4f22ad785b",
                              "width": 216
                          },
                          {
                              "height": 433,
                              "url": "https://preview.redd.it/zs08kroanoz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=535e6dd1ac486519623f6824ab76e0e4f586e274",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 770,
                          "url": "https://preview.redd.it/zs08kroanoz81.jpg?auto=webp&amp;s=d1eea5127d85d44d4521090502229bb4cbc5d2d4",
                          "width": 568
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652639206,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142712,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/qUZgmzMCLOM8q4CiCuZB37GksaI158XXdcy_6ewgI7I.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Tell us not to use Cermia without telling us not to use Cermia.",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/zs08kroanoz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/zs08kroanoz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "FeanorKC",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_1hhwujd1",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652638553,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqbyj3/could_we_all_please_agree_that_flidica_needs_a/",
          "gildings": {},
          "id": "uqbyj3",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqbyj3/could_we_all_please_agree_that_flidica_needs_a/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652638564,
          "score": 1,
          "selftext": "Considering how meta is rn and how spdbase 115 is ridiculous nowadays, i assume it's obvious that flidica is one of the worst ml5, deserves a buff and ofc a high spd buff tbh. \n\nShe's one of my favorite units from this game by far, and every time i look at her base spd it gives me more pain than pulling 3 ml bassar.\n\nIt's almost a petition, please SG buff her, we don't wanna see anymore peyra and Rhan flying around ffs.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142712,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Could we all please agree that Flidica needs a speed buff at this point?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqbyj3/could_we_all_please_agree_that_flidica_needs_a/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Pachavel",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_5q0n38fp",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652637928,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqbqgj/arena_gear/",
          "gildings": {},
          "id": "uqbqgj",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqbqgj/arena_gear/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652637939,
          "score": 1,
          "selftext": "Arena gear substats always end up worse than unreforged equipment, starting to think that they were made as another resource sink.",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142711,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Arena Gear",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uqbqgj/arena_gear/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Triple_S_Rank",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_pf62m",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652637928,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqbqgi/dark_tyrant_tenebria_art_by_lunar_estia/",
          "gildings": {},
          "id": "uqbqgi",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 1,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqbqgi/dark_tyrant_tenebria_art_by_lunar_estia/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "y6pA9LjWaBk67hnJzXkhbouMCMQZj2dZbod2xAHDPTI",
                      "resolutions": [
                          {
                              "height": 152,
                              "url": "https://preview.redd.it/3yxfqmr0joz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=8bc2e0aefe43333336911b3c27086098fd4b8490",
                              "width": 108
                          },
                          {
                              "height": 305,
                              "url": "https://preview.redd.it/3yxfqmr0joz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=8674f78634de8783904719cccb7459e13a074201",
                              "width": 216
                          },
                          {
                              "height": 452,
                              "url": "https://preview.redd.it/3yxfqmr0joz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=74489c79eb94ac05af154616b04d242a679fdb01",
                              "width": 320
                          },
                          {
                              "height": 904,
                              "url": "https://preview.redd.it/3yxfqmr0joz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=43e7a57f27f62cc4d5a137795a9e5df689bf8403",
                              "width": 640
                          },
                          {
                              "height": 1357,
                              "url": "https://preview.redd.it/3yxfqmr0joz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=6302810f6d5063bf47d69be1941d69afa60af1b4",
                              "width": 960
                          },
                          {
                              "height": 1526,
                              "url": "https://preview.redd.it/3yxfqmr0joz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=24fb8787906ed4c10d33d8099bcab65bd430f657",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1681,
                          "url": "https://preview.redd.it/3yxfqmr0joz81.jpg?auto=webp&amp;s=efca1a6fb99f9f337d1e55cb802acfbe2efc691e",
                          "width": 1189
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652637939,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142711,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/_B-lzKk77ZDMFX1pkIb1hqdJvO-ECQSkBl0TNVTg-70.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Dark Tyrant Tenebria [art by Lunar Estia]",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/3yxfqmr0joz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/3yxfqmr0joz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "ScumCommander",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_113mgb",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652635000,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqappb/day_6_ai_generated_art_of_e7_characters/",
          "gildings": {},
          "id": "uqappb",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqappb/day_6_ai_generated_art_of_e7_characters/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "GrAwptmJRxpom4GB1ghb0U6AuK4deb0FK68AxZDwlv4",
                      "resolutions": [
                          {
                              "height": 192,
                              "url": "https://preview.redd.it/eh4j6vmtaoz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=1ce8314ae429b1dc5bf68b1f5a9e7089a778f1d5",
                              "width": 108
                          },
                          {
                              "height": 384,
                              "url": "https://preview.redd.it/eh4j6vmtaoz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=cd395298731aa191b6826ee5bca05ce23a2706a4",
                              "width": 216
                          },
                          {
                              "height": 568,
                              "url": "https://preview.redd.it/eh4j6vmtaoz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=324e658c6430cf0b75a25f11a5e405f2c84b7991",
                              "width": 320
                          },
                          {
                              "height": 1137,
                              "url": "https://preview.redd.it/eh4j6vmtaoz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=549066ffad88a18ad92c1945085b8e9ff00fd97d",
                              "width": 640
                          },
                          {
                              "height": 1706,
                              "url": "https://preview.redd.it/eh4j6vmtaoz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=409554ddd393c6f7a71a5ab1cf764e53bd8ce133",
                              "width": 960
                          },
                          {
                              "height": 1920,
                              "url": "https://preview.redd.it/eh4j6vmtaoz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=5c6b7af8315eb3cf9b554fd96e7480c57f2c380b",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1920,
                          "url": "https://preview.redd.it/eh4j6vmtaoz81.jpg?auto=webp&amp;s=86526369fb95b77bdb2fe617d8ad5581f9c7ea45",
                          "width": 1080
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652635010,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142711,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/G9Vz4z22C_iSlLdSx-FUwpLZwBtaz5m2N3q7PWx0zjg.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Day 6, AI generated art of E7 characters",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/eh4j6vmtaoz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/eh4j6vmtaoz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "EnvironmentalPoem28",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_dousz9tv",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652634900,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uqaog6/i_need_this_krau_skin/",
          "gildings": {},
          "id": "uqaog6",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uqaog6/i_need_this_krau_skin/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "Q1ZEjM2WlrA9oYLfeWBRR4fuYfbKjJNrz0lnR6fBNXI",
                      "resolutions": [
                          {
                              "height": 107,
                              "url": "https://preview.redd.it/fccgjp0jaoz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=9f13a3945f4f6f361d89b74a8aa6596637624504",
                              "width": 108
                          },
                          {
                              "height": 215,
                              "url": "https://preview.redd.it/fccgjp0jaoz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=74dff1771464bca3219334e45ec35ba1b8aec321",
                              "width": 216
                          },
                          {
                              "height": 319,
                              "url": "https://preview.redd.it/fccgjp0jaoz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=004547d7cfd07786c3263cf5167b64b8b1889123",
                              "width": 320
                          },
                          {
                              "height": 638,
                              "url": "https://preview.redd.it/fccgjp0jaoz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=6301439ba207683e0044079bd72bb0d730ecea02",
                              "width": 640
                          }
                      ],
                      "source": {
                          "height": 826,
                          "url": "https://preview.redd.it/fccgjp0jaoz81.jpg?auto=webp&amp;s=ac5d8c0b941422f5215f3c450ff8553c34cff6ff",
                          "width": 828
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652634911,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142711,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/7Z9RwB7rPPb2z6VIKDQcwOygEijQM73F_q06cJul0Ac.jpg",
          "thumbnail_height": 139,
          "thumbnail_width": 140,
          "title": "I NEED this Krau skin",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/fccgjp0jaoz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/fccgjp0jaoz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "2became1",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_8kyczh2j",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652628971,
          "domain": "youtu.be",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq8lvr/how_can_rem_miss_def_break_with_demon_mode/",
          "gildings": {},
          "id": "uq8lvr",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media": {
              "oembed": {
                  "author_name": "Fabio Hermis",
                  "author_url": "https://www.youtube.com/channel/UCVAbqpecyaIwZlpV5dbbfsQ",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/BVSxKrwxl5c?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/BVSxKrwxl5c/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Rem with Demon Mode \u201cmisses\u201d Defense Break.",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/BVSxKrwxl5c?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "scrolling": false,
              "width": 356
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq8lvr/how_can_rem_miss_def_break_with_demon_mode/",
          "pinned": false,
          "post_hint": "rich:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "tVcsupnFXjlkeNN3DZpyg-qhfg0vluiY2SiMzv2qmmk",
                      "resolutions": [
                          {
                              "height": 81,
                              "url": "https://external-preview.redd.it/wQBqtDSS6f-84mwEPHyM1_z84Cbyy3ijqqt86NJkWWQ.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=b2229d7b4caa0f2d4c675f752fd636dc63076a26",
                              "width": 108
                          },
                          {
                              "height": 162,
                              "url": "https://external-preview.redd.it/wQBqtDSS6f-84mwEPHyM1_z84Cbyy3ijqqt86NJkWWQ.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=5713582f08b933d1b5cccbddd86ec06835908fe1",
                              "width": 216
                          },
                          {
                              "height": 240,
                              "url": "https://external-preview.redd.it/wQBqtDSS6f-84mwEPHyM1_z84Cbyy3ijqqt86NJkWWQ.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=ed9f20d74435a069aaa557242f4b76c58043806c",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 360,
                          "url": "https://external-preview.redd.it/wQBqtDSS6f-84mwEPHyM1_z84Cbyy3ijqqt86NJkWWQ.jpg?auto=webp&amp;s=ca3d0965d6ac18a4002e002d11d8fc3637429156",
                          "width": 480
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652628982,
          "score": 1,
          "secure_media": {
              "oembed": {
                  "author_name": "Fabio Hermis",
                  "author_url": "https://www.youtube.com/channel/UCVAbqpecyaIwZlpV5dbbfsQ",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/BVSxKrwxl5c?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/BVSxKrwxl5c/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Rem with Demon Mode \u201cmisses\u201d Defense Break.",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "secure_media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/BVSxKrwxl5c?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "media_domain_url": "https://www.redditmedia.com/mediaembed/uq8lvr",
              "scrolling": false,
              "width": 356
          },
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142709,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/mm1bfugjRmHxPjJqrkIs-v6YJd0ONHcJ3wFfw75_t8g.jpg",
          "thumbnail_height": 105,
          "thumbnail_width": 140,
          "title": "How can Rem miss Def Break with Demon Mode Activated? Doesn\u2019t it \u201cIgnore Effect Resistance\u201d?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://youtu.be/BVSxKrwxl5c",
          "url_overridden_by_dest": "https://youtu.be/BVSxKrwxl5c",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "eboonge",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_ft33ate6",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652624534,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq75ki/custom_mystic_summon_problems/",
          "gildings": {},
          "id": "uq75ki",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": false,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq75ki/custom_mystic_summon_problems/",
          "pinned": false,
          "pwls": 7,
          "removed_by_category": "automod_filtered",
          "retrieved_on": 1652624544,
          "score": 1,
          "selftext": "[removed]",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142710,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Custom Mystic Summon problems",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq75ki/custom_mystic_summon_problems/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "nihilninja",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_ru3pl",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652624323,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq7357/looking_for_help/",
          "gildings": {},
          "id": "uq7357",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq7357/looking_for_help/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652624334,
          "score": 1,
          "selftext": "i am a returning player and i am looking for help with the pet system. ive seen some guides but none of them ive seen mention generations? when i try to combine pets it says i dont have the same generation? do i need to just keep summoning new pets and combining those? any info would be greatly appreciated or a possible link to a good in depth guide",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142710,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "looking for help",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq7357/looking_for_help/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Pizza_Extreme",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_7kidpmzc",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652623497,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq6tv6/reroll_part_2/",
          "gildings": {},
          "id": "uq6tv6",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq6tv6/reroll_part_2/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652623508,
          "score": 1,
          "selftext": "So I have made a post asking people if I should reroll my designer lilibet and other chars acc and I have concluded to reroll. \n\nSo my new acc has Sigret tamarinne archdemon eda stene and others so I don't know if it was better but yes \nThank you for your time reading this random piece of text",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142709,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Reroll part 2",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq6tv6/reroll_part_2/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "asaness",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_kdmbe",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652621821,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq6ari/specter_tenebria_by_\u3075\u3041\u308b\u5f0f/",
          "gildings": {},
          "id": "uq6ari",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq6ari/specter_tenebria_by_\u3075\u3041\u308b\u5f0f/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "mKNtU9tNP6g2_S69X5Jb2mUwfmG7Ns7zbRAOKObC4_w",
                      "resolutions": [
                          {
                              "height": 76,
                              "url": "https://preview.redd.it/wnoa8o0l7nz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=d589e81f17ed630d680befc8f9f98514db2c0b80",
                              "width": 108
                          },
                          {
                              "height": 153,
                              "url": "https://preview.redd.it/wnoa8o0l7nz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=1b4b03bd64b3ba796d40df1ce5967355d3acac29",
                              "width": 216
                          },
                          {
                              "height": 227,
                              "url": "https://preview.redd.it/wnoa8o0l7nz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=a73c56bdfdb58cd26403b7e34bdfcec667d29be7",
                              "width": 320
                          },
                          {
                              "height": 454,
                              "url": "https://preview.redd.it/wnoa8o0l7nz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=b6de0819c5264166aa82c895210b52cf11ac91b2",
                              "width": 640
                          },
                          {
                              "height": 681,
                              "url": "https://preview.redd.it/wnoa8o0l7nz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=85d8d1772e07c7f2d2c7b61e8ab8267cd191679f",
                              "width": 960
                          },
                          {
                              "height": 766,
                              "url": "https://preview.redd.it/wnoa8o0l7nz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=5e56c12cd1f9d273ff1157d8a5f7f40e942ec1f2",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 2513,
                          "url": "https://preview.redd.it/wnoa8o0l7nz81.png?auto=webp&amp;s=c12070deb06923313b2045b7f0264dfce253a289",
                          "width": 3541
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652621832,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142709,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/YrDySx9dJ_XRmhtntZ7-RK_66IcKT21L5VaF4zh4VlQ.jpg",
          "thumbnail_height": 99,
          "thumbnail_width": 140,
          "title": "Specter Tenebria by \u3075\u3041\u308b\u5f0f",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/wnoa8o0l7nz81.png",
          "url_overridden_by_dest": "https://i.redd.it/wnoa8o0l7nz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "CrimsonFury",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "three-cermia",
          "author_flair_richtext": [
              {
                  "a": ":cermia:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/lhnpa9h3d1s51_t5_nrn6j/cermia"
              }
          ],
          "author_flair_template_id": "2351710a-fc64-11e9-8744-0ede73c8fdf2",
          "author_flair_text": ":cermia:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_956ji",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652619648,
          "domain": "reddit.com",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq5mev/should_i_switch_my_belian_to_any_of_these_3/",
          "gallery_data": {
              "items": [
                  {
                      "id": 142416370,
                      "media_id": "manpp1xo0nz81"
                  },
                  {
                      "id": 142416371,
                      "media_id": "yfaj6tap0nz81"
                  },
                  {
                      "id": 142416372,
                      "media_id": "8jnq3vmp0nz81"
                  }
              ]
          },
          "gildings": {},
          "id": "uq5mev",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_gallery": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_metadata": {
              "8jnq3vmp0nz81": {
                  "e": "Image",
                  "id": "8jnq3vmp0nz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/8jnq3vmp0nz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=fae2e375f559dfd56ee24ca2dc42b70fb2a49a62",
                          "x": 108,
                          "y": 102
                      },
                      {
                          "u": "https://preview.redd.it/8jnq3vmp0nz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=574da5883837add13637dec7daa23ac5bdbd4bfd",
                          "x": 216,
                          "y": 205
                      },
                      {
                          "u": "https://preview.redd.it/8jnq3vmp0nz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=357924be20b38b551f572d8f59f32c9051e2301a",
                          "x": 320,
                          "y": 304
                      },
                      {
                          "u": "https://preview.redd.it/8jnq3vmp0nz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=667cc7fb7250ef713230685025e469d459536d42",
                          "x": 640,
                          "y": 609
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/8jnq3vmp0nz81.jpg?width=847&amp;format=pjpg&amp;auto=webp&amp;s=71f430f5f3e47da888344428b334a7e8d68fc7d3",
                      "x": 847,
                      "y": 806
                  },
                  "status": "valid"
              },
              "manpp1xo0nz81": {
                  "e": "Image",
                  "id": "manpp1xo0nz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/manpp1xo0nz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=a3fc7466ee957fd547878c67940ca4aa55587436",
                          "x": 108,
                          "y": 103
                      },
                      {
                          "u": "https://preview.redd.it/manpp1xo0nz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=afc408de1e548a0337458cec1ee1fe72bd021948",
                          "x": 216,
                          "y": 207
                      },
                      {
                          "u": "https://preview.redd.it/manpp1xo0nz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=34d4b20a8e95fcf2ebe67b8071ad66543ab55f95",
                          "x": 320,
                          "y": 307
                      },
                      {
                          "u": "https://preview.redd.it/manpp1xo0nz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=b095baebf8aa959892c03cbc0d0b392860ead62b",
                          "x": 640,
                          "y": 615
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/manpp1xo0nz81.jpg?width=840&amp;format=pjpg&amp;auto=webp&amp;s=e7586ba4790ef2d8bd186c2fbdcfd98f620c53a8",
                      "x": 840,
                      "y": 808
                  },
                  "status": "valid"
              },
              "yfaj6tap0nz81": {
                  "e": "Image",
                  "id": "yfaj6tap0nz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/yfaj6tap0nz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=452b734eef4aefb01a53eeb2aadee0212400698e",
                          "x": 108,
                          "y": 102
                      },
                      {
                          "u": "https://preview.redd.it/yfaj6tap0nz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=bf74f9b610e9381ba94418c890bf304aa2ed2ead",
                          "x": 216,
                          "y": 204
                      },
                      {
                          "u": "https://preview.redd.it/yfaj6tap0nz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=7005d704a0b219ff685b3b40070648a9034af072",
                          "x": 320,
                          "y": 303
                      },
                      {
                          "u": "https://preview.redd.it/yfaj6tap0nz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=889243f9a7d9cfd4e38b66509ade3fc844ac83cc",
                          "x": 640,
                          "y": 606
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/yfaj6tap0nz81.jpg?width=849&amp;format=pjpg&amp;auto=webp&amp;s=2cde4caf7a55a7e6d61457cec57921bb10910c2e",
                      "x": 849,
                      "y": 804
                  },
                  "status": "valid"
              }
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq5mev/should_i_switch_my_belian_to_any_of_these_3/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652619658,
          "score": 1,
          "selftext": "",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142706,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/dO8_swhtGHNpwX_gIBSelKcs7_C6sJt9ojzYodMYZHw.jpg",
          "thumbnail_height": 134,
          "thumbnail_width": 140,
          "title": "Should I switch my Belian to any of these 3 builds? I only have free gear injury pieces.",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/gallery/uq5mev",
          "url_overridden_by_dest": "https://www.reddit.com/gallery/uq5mev",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "ThatWeakGuy",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_ksqe8",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652618776,
          "domain": "i.imgur.com",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq5dbu/finally_finished_my_apocalypse_ravi_what_do_you/",
          "gildings": {},
          "id": "uq5dbu",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq5dbu/finally_finished_my_apocalypse_ravi_what_do_you/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "IChJ8N9DBb8Vq5yC_b39UW-wdcM9Q_8Zp1dkGw17gW4",
                      "resolutions": [
                          {
                              "height": 48,
                              "url": "https://external-preview.redd.it/aYFcJG5bSwK_rhtHYkdTc1BgF-nhzsHPSo5FXsJlJHs.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=506cbc792283f090260c14910ba7703d64e3eee6",
                              "width": 108
                          },
                          {
                              "height": 97,
                              "url": "https://external-preview.redd.it/aYFcJG5bSwK_rhtHYkdTc1BgF-nhzsHPSo5FXsJlJHs.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=f62b0ee6d942fcc5422535a1240245ab5cbff931",
                              "width": 216
                          },
                          {
                              "height": 144,
                              "url": "https://external-preview.redd.it/aYFcJG5bSwK_rhtHYkdTc1BgF-nhzsHPSo5FXsJlJHs.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=891ca29ffcbdad4a20d9d4989f1de73ee69dc572",
                              "width": 320
                          },
                          {
                              "height": 288,
                              "url": "https://external-preview.redd.it/aYFcJG5bSwK_rhtHYkdTc1BgF-nhzsHPSo5FXsJlJHs.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=cd81ffb26e73af9581f2ae6afa0cdd9b4ba7c1f0",
                              "width": 640
                          },
                          {
                              "height": 432,
                              "url": "https://external-preview.redd.it/aYFcJG5bSwK_rhtHYkdTc1BgF-nhzsHPSo5FXsJlJHs.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=19908fb8e7183bfd1d61951aaa39bad7b6b40fc9",
                              "width": 960
                          },
                          {
                              "height": 486,
                              "url": "https://external-preview.redd.it/aYFcJG5bSwK_rhtHYkdTc1BgF-nhzsHPSo5FXsJlJHs.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=6ad90ca325674f29158bdf5fa2ef903779a7aee6",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 720,
                          "url": "https://external-preview.redd.it/aYFcJG5bSwK_rhtHYkdTc1BgF-nhzsHPSo5FXsJlJHs.jpg?auto=webp&amp;s=967bd66b605f62222f0b7d0512c29eb1283efaeb",
                          "width": 1600
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652618786,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142705,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/HL7hpDKVn1KJXkzM6HDnAI98z5ASYSiBDB2_n4hGznM.jpg",
          "thumbnail_height": 63,
          "thumbnail_width": 140,
          "title": "Finally finished my Apocalypse Ravi. What do you guys think?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.imgur.com/TBGwRED.jpg",
          "url_overridden_by_dest": "https://i.imgur.com/TBGwRED.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Sensitive_Bug3211",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_841k2tv7",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652618424,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq59n9/the_seline_experience_at_least_for_me/",
          "gildings": {},
          "id": "uq59n9",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq59n9/the_seline_experience_at_least_for_me/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "lIZ0PO3zsLfFz8o-QsL5vFLYoJTZtUaX4q79I_gVqG8",
                      "resolutions": [
                          {
                              "height": 191,
                              "url": "https://preview.redd.it/039pjm1dxmz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=ffca4656694b9ac47e8824e7cf3ff8b9ce9f499b",
                              "width": 108
                          },
                          {
                              "height": 383,
                              "url": "https://preview.redd.it/039pjm1dxmz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=ac32c87301a46bd39238bd24079bf9450d362930",
                              "width": 216
                          }
                      ],
                      "source": {
                          "height": 471,
                          "url": "https://preview.redd.it/039pjm1dxmz81.png?auto=webp&amp;s=a5c80cc7b429de0b26285ceb2296630f4f9b6695",
                          "width": 265
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652618435,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142705,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/dhy9LGYZMPMbp8OZ0ANdlehrwLqZKk6YCFngqt0dEHo.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "The Seline experience (At least for me)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/039pjm1dxmz81.png",
          "url_overridden_by_dest": "https://i.redd.it/039pjm1dxmz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "NewAnimeEveryday",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_wovhd",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652617376,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq4yxb/finally_after_months_of_grinding_i_have_a_decent/",
          "gildings": {},
          "id": "uq4yxb",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq4yxb/finally_after_months_of_grinding_i_have_a_decent/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "l5r_01ifKi4DUUmjjnv9eOxtBFfdtroDyjAJhzD5sj8",
                      "resolutions": [
                          {
                              "height": 86,
                              "url": "https://preview.redd.it/275qk43fumz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=8b8a7a0675e43f6d75a50bee5004050b0c831387",
                              "width": 108
                          },
                          {
                              "height": 172,
                              "url": "https://preview.redd.it/275qk43fumz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=cfd8b5545cb047316893e352b1a2b1954a8077de",
                              "width": 216
                          },
                          {
                              "height": 256,
                              "url": "https://preview.redd.it/275qk43fumz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=3079e3ca3337b96ca293a7c70733822b296da20a",
                              "width": 320
                          },
                          {
                              "height": 512,
                              "url": "https://preview.redd.it/275qk43fumz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=d7a0cfc8b3be0fe9678d2330edd993f6a080741a",
                              "width": 640
                          },
                          {
                              "height": 768,
                              "url": "https://preview.redd.it/275qk43fumz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=965abe2f462a1cca1828e62787f817752f6e8881",
                              "width": 960
                          },
                          {
                              "height": 864,
                              "url": "https://preview.redd.it/275qk43fumz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=5d12f064f0772f1aaf3e8d7cae98edbfb0beb1b0",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1768,
                          "url": "https://preview.redd.it/275qk43fumz81.jpg?auto=webp&amp;s=4281b213357f7956cd489d0b34f3e66c20f69cc4",
                          "width": 2208
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652617386,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142704,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/LaHY9dopLFKYVXfaPZO6idIL_hks0BbZqHvpVybxACw.jpg",
          "thumbnail_height": 112,
          "thumbnail_width": 140,
          "title": "Finally, after months of grinding, I have a decent team!",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/275qk43fumz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/275qk43fumz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Ransu_0000",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_ddpzzcdc",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652617091,
          "domain": "/r/EpicSeven/comments/uq4w1p/this_is_the_first_time_i_encountered_this/",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq4w1p/this_is_the_first_time_i_encountered_this/",
          "gildings": {},
          "id": "uq4w1p",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": true,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq4w1p/this_is_the_first_time_i_encountered_this/",
          "pinned": false,
          "post_hint": "hosted:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "wQ4DIg_SKEERtYTjpSx7i0QUJCMRJ8sIgxxZ4fY8JpA",
                      "resolutions": [
                          {
                              "height": 48,
                              "url": "https://external-preview.redd.it/5GQLzZVAeLGx_WltfazeymJ9r1b1sG8y1LyaF7qEBlk.png?width=108&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=24fc10fe8c291aae356668ad9586ff18d41cff1c",
                              "width": 108
                          },
                          {
                              "height": 97,
                              "url": "https://external-preview.redd.it/5GQLzZVAeLGx_WltfazeymJ9r1b1sG8y1LyaF7qEBlk.png?width=216&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=f5ad3d6c3cae4200471200e807950dc877cbd6a9",
                              "width": 216
                          },
                          {
                              "height": 144,
                              "url": "https://external-preview.redd.it/5GQLzZVAeLGx_WltfazeymJ9r1b1sG8y1LyaF7qEBlk.png?width=320&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=2212d6fa85e429440893db3a117a9fdc87c84edc",
                              "width": 320
                          },
                          {
                              "height": 288,
                              "url": "https://external-preview.redd.it/5GQLzZVAeLGx_WltfazeymJ9r1b1sG8y1LyaF7qEBlk.png?width=640&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=87426cd70fa4b347a37b2d6b2485177559458746",
                              "width": 640
                          },
                          {
                              "height": 432,
                              "url": "https://external-preview.redd.it/5GQLzZVAeLGx_WltfazeymJ9r1b1sG8y1LyaF7qEBlk.png?width=960&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=afa43979052ee47b77466afd298138ff710bb123",
                              "width": 960
                          },
                          {
                              "height": 486,
                              "url": "https://external-preview.redd.it/5GQLzZVAeLGx_WltfazeymJ9r1b1sG8y1LyaF7qEBlk.png?width=1080&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=9ae68890f017cc2d648c317807d6ff7b9be23004",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 720,
                          "url": "https://external-preview.redd.it/5GQLzZVAeLGx_WltfazeymJ9r1b1sG8y1LyaF7qEBlk.png?format=pjpg&amp;auto=webp&amp;s=6b66c6f7978ee1c87403911e2ec538bc9defc8c5",
                          "width": 1600
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652617101,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142704,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/rWV8x9LYreyhnwa5YmQxcfkVmL9yimsFAXfmSN9MxW4.jpg",
          "thumbnail_height": 63,
          "thumbnail_width": 140,
          "title": "This is the first time i encountered this",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://v.redd.it/zpjhodhbtmz81",
          "url_overridden_by_dest": "https://v.redd.it/zpjhodhbtmz81",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "buskmoose",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_4pf8qi67",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652616781,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq4szn/hunt_challenge_event/",
          "gildings": {},
          "id": "uq4szn",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq4szn/hunt_challenge_event/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652616791,
          "score": 1,
          "selftext": "Just completed the wyvern challenges on stage 2 and notice you can complete all 3 hunt challenges to get cermia Vivian and sigret I feel dumb",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142703,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Hunt challenge event",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq4szn/hunt_challenge_event/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "TheKinkyGuy",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "four-Ilynav",
          "author_flair_richtext": [
              {
                  "a": ":Ilynav:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/2gcibetacxy51_t5_nrn6j/Ilynav"
              }
          ],
          "author_flair_template_id": "45d3d366-2563-11eb-b87b-0e25c5ed035b",
          "author_flair_text": ":Ilynav:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_tk1634s",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652613691,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq405b/they_changed_how_the_watch_app_function_works/",
          "gildings": {},
          "id": "uq405b",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq405b/they_changed_how_the_watch_app_function_works/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652613701,
          "score": 1,
          "selftext": "Before today if you watched the ad for 20 energy, after 15-30 sec, you could click close and go to the next ad. Today I can not close the ad and it automatically forces me into the gplay store (if it is an app ad).  \n\n\nThis is intrusive and unacceptable. SG fix adn remove this shit (forceful GPlay store openning).",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142703,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "They changed how the watch app function works",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq405b/they_changed_how_the_watch_app_function_works/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "NewAnimeEveryday",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_wovhd",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652611049,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq3dln/i_firmly_believe_someone_should_rot_in_hell_for/",
          "gildings": {},
          "id": "uq3dln",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq3dln/i_firmly_believe_someone_should_rot_in_hell_for/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "P_nGOuuczaPOPUYUQ3bDpSlfP6ehOIlnd2JTKWsOB48",
                      "resolutions": [
                          {
                              "height": 86,
                              "url": "https://preview.redd.it/s2iddmslbmz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=aa532241cbabe609c85cba775b81435b0b461b7d",
                              "width": 108
                          },
                          {
                              "height": 172,
                              "url": "https://preview.redd.it/s2iddmslbmz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=ca8b7e8d5aca54638f7d06ec45f4899d3aeef992",
                              "width": 216
                          },
                          {
                              "height": 256,
                              "url": "https://preview.redd.it/s2iddmslbmz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=cea8efb421fb0ee829eeefc541f17d06cfa4447b",
                              "width": 320
                          },
                          {
                              "height": 512,
                              "url": "https://preview.redd.it/s2iddmslbmz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=b91871102a30d188ec5e8c0798f5848ec45cab0c",
                              "width": 640
                          },
                          {
                              "height": 768,
                              "url": "https://preview.redd.it/s2iddmslbmz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=db0240669bad850080ddf791633fb10b127f972d",
                              "width": 960
                          },
                          {
                              "height": 864,
                              "url": "https://preview.redd.it/s2iddmslbmz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=98d6ea2a4b46ecd53bf3b5041a0e829453395831",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1768,
                          "url": "https://preview.redd.it/s2iddmslbmz81.jpg?auto=webp&amp;s=26d8669683c4867295bcc64924dc55f24c79ebca",
                          "width": 2208
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652611060,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142701,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/WVVF0k1SfeNoamQXkwMSeFc-q-VoXyXgOhV_zw7HGao.jpg",
          "thumbnail_height": 112,
          "thumbnail_width": 140,
          "title": "I firmly believe someone should rot in hell for designing this",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/s2iddmslbmz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/s2iddmslbmz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Rinczmia",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "four-Flan",
          "author_flair_richtext": [
              {
                  "a": ":Flan:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/k2f1wugacxy51_t5_nrn6j/Flan"
              }
          ],
          "author_flair_template_id": "0d61f35a-2563-11eb-a19e-0ea82cef6357",
          "author_flair_text": ":Flan:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_2nlh2tqy",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652608689,
          "distinguished": "moderator",
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq2tqx/weekly_friend_guild_recruitment_megathread_week/",
          "gildings": {},
          "id": "uq2tqx",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#9abd19",
          "link_flair_css_class": "Megathread",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Megathread"
              }
          ],
          "link_flair_template_id": "dbef8262-f5f1-11e8-8f69-0e22455ee724",
          "link_flair_text": "Megathread",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq2tqx/weekly_friend_guild_recruitment_megathread_week/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652608700,
          "score": 1,
          "selftext": "Hello Heirs! This is the **Weekly Friend &amp; Guild Recruitment Megathread.**\n\nIn search of a carry or like-minded individuals? Recruit for guild or expeditions here!\n\nExample formats to use:\n\n* Guild Recruitment\n\n&amp;#8203;\n\n    [Guild Name]\n    Region/Server:\n    Description/Requirements:\n    Spots/GW-Rank/ETC:\n\n* Friend Request\n\n&amp;#8203;\n\n    [In-Game Name]\n    Region/Server:\n    Rank:\n    Details: Looking for active friends / Expedition(s)\n\nPlease respect other recruiters and not spam! For your safety please direct message your discord invite to those interested. Do not post your discord, your comment will be filtered out and not be processed! \n\nCommunity resources can be found here:  /r/EpicSeven wiki \n\n|**Other Megathreads**|\n|:-|\n|[Daily Questions Megathread](https://reddit.com/r/EpicSeven/about/sticky?num=1)|\n|[Weekly Gacha &amp; Rant Megathread](https://www.reddit.com/r/EpicSeven/search?q=title%3A%22Rant+Megathread%22&amp;restrict_sr=on&amp;sort=relevance&amp;t=week) |",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142699,
          "subreddit_type": "public",
          "suggested_sort": "new",
          "thumbnail": "self",
          "title": "Weekly Friend &amp; Guild Recruitment Megathread (Week 05/15)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq2tqx/weekly_friend_guild_recruitment_megathread_week/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Rinczmia",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "four-Flan",
          "author_flair_richtext": [
              {
                  "a": ":Flan:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/k2f1wugacxy51_t5_nrn6j/Flan"
              }
          ],
          "author_flair_template_id": "0d61f35a-2563-11eb-a19e-0ea82cef6357",
          "author_flair_text": ":Flan:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_2nlh2tqy",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652608689,
          "distinguished": "moderator",
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq2tqw/weekly_gacha_rant_megathread_week_0515/",
          "gildings": {},
          "id": "uq2tqw",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#9abd19",
          "link_flair_css_class": "Megathread",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Megathread"
              }
          ],
          "link_flair_template_id": "dbef8262-f5f1-11e8-8f69-0e22455ee724",
          "link_flair_text": "Megathread",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq2tqw/weekly_gacha_rant_megathread_week_0515/",
          "pinned": false,
          "post_hint": "self",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "nHmB3tUVKrCUK7BucSpK1ZRAoN-XyaQHrTTqr75VHnw",
                      "resolutions": [
                          {
                              "height": 56,
                              "url": "https://external-preview.redd.it/VpsxLZIualmxdOClASVbyDkS8bop7vbHfjUJmqHR8e0.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=7a328475553b2740866dcb79ba5c5940a1d88c0f",
                              "width": 108
                          },
                          {
                              "height": 113,
                              "url": "https://external-preview.redd.it/VpsxLZIualmxdOClASVbyDkS8bop7vbHfjUJmqHR8e0.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=820cc427905a95e3ed941aaf60abe02e94e4c58d",
                              "width": 216
                          },
                          {
                              "height": 168,
                              "url": "https://external-preview.redd.it/VpsxLZIualmxdOClASVbyDkS8bop7vbHfjUJmqHR8e0.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=a7613786215cf066b8627ba330eeab07393e796b",
                              "width": 320
                          },
                          {
                              "height": 336,
                              "url": "https://external-preview.redd.it/VpsxLZIualmxdOClASVbyDkS8bop7vbHfjUJmqHR8e0.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=56256734a4b4f71bbfdfe0dfcf8a63f777d03952",
                              "width": 640
                          },
                          {
                              "height": 504,
                              "url": "https://external-preview.redd.it/VpsxLZIualmxdOClASVbyDkS8bop7vbHfjUJmqHR8e0.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=fd0302d81bd888fbd7428b2827fdcbff991c285d",
                              "width": 960
                          },
                          {
                              "height": 567,
                              "url": "https://external-preview.redd.it/VpsxLZIualmxdOClASVbyDkS8bop7vbHfjUJmqHR8e0.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=563e4604911e1b2fd2fd10510335bf87e3f10713",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 630,
                          "url": "https://external-preview.redd.it/VpsxLZIualmxdOClASVbyDkS8bop7vbHfjUJmqHR8e0.jpg?auto=webp&amp;s=77f98c0d46d895bbf2f5c82f3fb70ec0637dba5a",
                          "width": 1200
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652608700,
          "score": 1,
          "selftext": "Hello Heirs! This is the **Weekly Gacha &amp; Rant Megathread.**\n\nAll things salt inducing (pulls and rants) are belong here\\~\n\nWhat did you get from daily pull? Did you craft a god piece of equipment that ended up rolling into effect resist? Want to vent something off your chest? Share your screenshots in the comment section via [imgur](https://imgur.com/)! \n\nCommunity resources can be found here:  /r/EpicSeven wiki \n\n|**Other Megathreads**|\n|:-|\n|[Daily Questions Megathread](https://reddit.com/r/EpicSeven/about/sticky?num=1)|\n|[Weekly Friend &amp; Guild Recruitment Megathread](https://www.reddit.com/r/EpicSeven/search?q=title%3A%22Recruitment+Megathread%22&amp;restrict_sr=on&amp;sort=new&amp;t=week) |",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142699,
          "subreddit_type": "public",
          "suggested_sort": "new",
          "thumbnail": "self",
          "title": "Weekly Gacha &amp; Rant Megathread (Week 05/15)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq2tqw/weekly_gacha_rant_megathread_week_0515/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Rinczmia",
          "author_flair_background_color": "#dadada",
          "author_flair_css_class": "four-Flan",
          "author_flair_richtext": [
              {
                  "a": ":Flan:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/k2f1wugacxy51_t5_nrn6j/Flan"
              }
          ],
          "author_flair_template_id": "0d61f35a-2563-11eb-a19e-0ea82cef6357",
          "author_flair_text": ":Flan:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_2nlh2tqy",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652608511,
          "distinguished": "moderator",
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq2sd2/daily_questions_megathread_0515/",
          "gildings": {},
          "id": "uq2sd2",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#9abd19",
          "link_flair_css_class": "Megathread",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Megathread"
              }
          ],
          "link_flair_template_id": "dbef8262-f5f1-11e8-8f69-0e22455ee724",
          "link_flair_text": "Megathread",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq2sd2/daily_questions_megathread_0515/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652608521,
          "score": 1,
          "selftext": "Hello Heirs! This is the **Daily Questions Megathread**.\n\nYou are welcome to use the daily thread to ask general or personalized questions instead of creating a new thread.\n\nPlease ask all your beginner questions here as well. Help each other out and don't forget to thank/upvote fellow heirs!\n\n&amp;#x200B;\n\n**Useful Link(s):**\n\n* [Reddit Wiki](https://www.reddit.com/r/EpicSeven/wiki/index) \\- List of community resources\n* [Reddit Datamine Threads](https://www.reddit.com/user/YufineThrowaway/submitted/) \\- Spoiler warning\n* [Epic Seven News and Updates](https://page.onstove.com/epicseven/global/list/e7en001?listType=2&amp;direction=latest&amp;page=1) \\- (Official) link to Stove\n* [Hero/Artifact Spotlight](https://reddit.com/r/EpicSeven/about/sticky?num=2) \\- Ongoing discussion for banner featured this week\n* [Epic Seven Update History](https://docs.google.com/spreadsheets/d/15ac0nWz9lm3DRvmKD4OX_KBDpdO35WXNuj109qoQaIs/preview) \\- List of banners and updates since 2018\n\n&amp;#x200B;\n\n|**Other Megathreads**|\n|:-|\n|[Weekly Gacha &amp; Rant Megathread](https://www.reddit.com/r/EpicSeven/search?q=title%3A%22Rant+Megathread%22&amp;restrict_sr=on&amp;sort=relevance&amp;t=week)|\n|[Weekly Friend &amp; Guild Recruitment Megathread](https://www.reddit.com/r/EpicSeven/search?q=title%3A%22Recruitment+Megathread%22&amp;restrict_sr=on&amp;sort=new&amp;t=week)|",
          "send_replies": false,
          "spoiler": false,
          "stickied": true,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142699,
          "subreddit_type": "public",
          "suggested_sort": "new",
          "thumbnail": "self",
          "title": "Daily Questions Megathread (05/15)",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq2sd2/daily_questions_megathread_0515/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "darkpheonix92",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_2u8oa56f",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652607617,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq2lgu/has_anyone_tried_a_revenge_set_maid_chloe/",
          "gildings": {},
          "id": "uq2lgu",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq2lgu/has_anyone_tried_a_revenge_set_maid_chloe/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652607627,
          "score": 1,
          "selftext": "Text in title",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142699,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Has anyone tried a revenge set maid Chloe?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq2lgu/has_anyone_tried_a_revenge_set_maid_chloe/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "LilyKio",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_92e792",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652607059,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq2h3w/my_highest_speed_roll_yet_glad_i_came_back_to_the/",
          "gildings": {},
          "id": "uq2h3w",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq2h3w/my_highest_speed_roll_yet_glad_i_came_back_to_the/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "wOi3sjsodcfDolpGHC0ixKOsAxLBQxCWvEjUtAkx8jg",
                      "resolutions": [
                          {
                              "height": 141,
                              "url": "https://preview.redd.it/57gm7xclzlz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=c69b60e66d5bac5e1a52bf983dd986151b75988e",
                              "width": 108
                          },
                          {
                              "height": 283,
                              "url": "https://preview.redd.it/57gm7xclzlz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=dfe3ca73fdad723c33602af20569e3bcd9fac70c",
                              "width": 216
                          }
                      ],
                      "source": {
                          "height": 368,
                          "url": "https://preview.redd.it/57gm7xclzlz81.png?auto=webp&amp;s=d9cde95708bb635fbefaf303cef58f56dd5cb214",
                          "width": 280
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652607069,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142698,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/tgTClCBXZPkvAflqKkMcomHSDps20j9KzXWu3yWWl_0.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "My highest speed roll yet, glad I came back to the game after one year hiatus.",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/57gm7xclzlz81.png",
          "url_overridden_by_dest": "https://i.redd.it/57gm7xclzlz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Yaory",
          "author_flair_background_color": "#d3d6da",
          "author_flair_css_class": "one-enott",
          "author_flair_richtext": [
              {
                  "a": ":enott:",
                  "e": "emoji",
                  "u": "https://emoji.redditmedia.com/c6w3em05d1s51_t5_nrn6j/enott"
              }
          ],
          "author_flair_template_id": "9930408c-f53e-11e8-ae5d-0ed49c3128cc",
          "author_flair_text": ":enott:",
          "author_flair_text_color": "dark",
          "author_flair_type": "richtext",
          "author_fullname": "t2_705ot0z",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652606963,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq2gc5/my_aravi_build_what_artifact_should_i_put_on_her/",
          "gildings": {},
          "id": "uq2gc5",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_metadata": {
              "8lp8suryylz81": {
                  "e": "Image",
                  "id": "8lp8suryylz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/8lp8suryylz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=69fd986322deaf3a9ccf8bd4ddc7f6c8f8499ba1",
                          "x": 108,
                          "y": 60
                      },
                      {
                          "u": "https://preview.redd.it/8lp8suryylz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=c0ab9c6b60e870a24039f506f70191bcb17290d9",
                          "x": 216,
                          "y": 121
                      },
                      {
                          "u": "https://preview.redd.it/8lp8suryylz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=0a797acb88ec33fdb340ec809a796d6d009d20c7",
                          "x": 320,
                          "y": 180
                      },
                      {
                          "u": "https://preview.redd.it/8lp8suryylz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=6213ffe8ab6b6b43461e0497afc1f7f109d8a5d2",
                          "x": 640,
                          "y": 360
                      },
                      {
                          "u": "https://preview.redd.it/8lp8suryylz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=3b9dca164c3093385409dd1f5510953c8f4ff24a",
                          "x": 960,
                          "y": 540
                      },
                      {
                          "u": "https://preview.redd.it/8lp8suryylz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=75e42c5ba73af7230a62ef343ac6d3027a6dd8f6",
                          "x": 1080,
                          "y": 607
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/8lp8suryylz81.png?width=1600&amp;format=png&amp;auto=webp&amp;s=6e942bd25fa330d7be7bae32e79c5f9f2f055d80",
                      "x": 1600,
                      "y": 900
                  },
                  "status": "valid"
              },
              "osanrxryylz81": {
                  "e": "Image",
                  "id": "osanrxryylz81",
                  "m": "image/png",
                  "p": [
                      {
                          "u": "https://preview.redd.it/osanrxryylz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=3ed3032fc1c971bd6e5974d83013117ca31094fb",
                          "x": 108,
                          "y": 60
                      },
                      {
                          "u": "https://preview.redd.it/osanrxryylz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=a431abe3df0c6d298cfb55b20ef67da3882b68e4",
                          "x": 216,
                          "y": 121
                      },
                      {
                          "u": "https://preview.redd.it/osanrxryylz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=6914a1f363d8a927f42f90633ce249039aaa5223",
                          "x": 320,
                          "y": 180
                      },
                      {
                          "u": "https://preview.redd.it/osanrxryylz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=d8d1377d41997d9d0d238f22b14b506fb5668718",
                          "x": 640,
                          "y": 360
                      },
                      {
                          "u": "https://preview.redd.it/osanrxryylz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=38276186037f99df2821ff0ace787238384c2351",
                          "x": 960,
                          "y": 540
                      },
                      {
                          "u": "https://preview.redd.it/osanrxryylz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=e0d4922ea26e93f317a3c253c96da2f284733361",
                          "x": 1080,
                          "y": 607
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/osanrxryylz81.png?width=1600&amp;format=png&amp;auto=webp&amp;s=e815ec75f280a3ccdef9a01b0d220659917199ea",
                      "x": 1600,
                      "y": 900
                  },
                  "status": "valid"
              }
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq2gc5/my_aravi_build_what_artifact_should_i_put_on_her/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652606973,
          "score": 1,
          "selftext": "&amp;#x200B;\n\nhttps://preview.redd.it/8lp8suryylz81.png?width=1600&amp;format=png&amp;auto=webp&amp;s=6e942bd25fa330d7be7bae32e79c5f9f2f055d80\n\n[These are all the 5 and 4 star warrior artifacts I have](https://preview.redd.it/osanrxryylz81.png?width=1600&amp;format=png&amp;auto=webp&amp;s=e815ec75f280a3ccdef9a01b0d220659917199ea)",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142697,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/rg-PkF6ld801n7rxtAKxqsQVqch-G-4MYlTOxLtsmRw.jpg",
          "thumbnail_height": 78,
          "thumbnail_width": 140,
          "title": "My A.Ravi Build, what artifact should I put on her? is Sepulcrum ok?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/uq2gc5/my_aravi_build_what_artifact_should_i_put_on_her/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Impressive-Contest71",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_e22zkwfn",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652605463,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq24cg/feeling_pretty_good_about_this_10_pull/",
          "gildings": {},
          "id": "uq24cg",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq24cg/feeling_pretty_good_about_this_10_pull/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "YWyvXiZ-u0Q4N2tzTvR_uJYhE4yHgezkREeNFe3rB6k",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://preview.redd.it/uo3c67szulz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=b08dd6b5a0ae70598793f2caad15862a4c6dcd3b",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://preview.redd.it/uo3c67szulz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=0259e28f2e288a49f5284956895410e413e34bae",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://preview.redd.it/uo3c67szulz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=efaa48d4dbeb6eed209f4a900c802ba64225179d",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://preview.redd.it/uo3c67szulz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=12f4df3d57154d1d783b95ad5552d2326b5637e1",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://preview.redd.it/uo3c67szulz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=ca57736dd86c279e6f3e5698e3956840837d0feb",
                              "width": 960
                          },
                          {
                              "height": 499,
                              "url": "https://preview.redd.it/uo3c67szulz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=035b25494214f4ec817af8bb0677da247fe90dc0",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 828,
                          "url": "https://preview.redd.it/uo3c67szulz81.jpg?auto=webp&amp;s=29a9b90e6196318ac1e475b43a0929d6013ca451",
                          "width": 1792
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652605473,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142699,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/YRRu4q9R_g90Vr9w4DpvhkM0xiS9HPrpmexu8hFO8aY.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "Feeling pretty good about this 10 pull!",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/uo3c67szulz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/uo3c67szulz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Zen_artz",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_7tij454q",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652600123,
          "domain": "reddit.com",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uq0xb0/advice_on_choosing_units_for_custom_group_summon/",
          "gallery_data": {
              "items": [
                  {
                      "caption": "These are my units. Cleared Ep1, what units should I choose or u recommend I should choose for the custom summon. ",
                      "id": 142356799,
                      "media_id": "qv44i744flz81"
                  },
                  {
                      "id": 142356800,
                      "media_id": "vnx27844flz81"
                  },
                  {
                      "id": 142356801,
                      "media_id": "j9uvm844flz81"
                  }
              ]
          },
          "gildings": {},
          "id": "uq0xb0",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_gallery": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#cc66ff",
          "link_flair_css_class": "tip",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Tips"
              }
          ],
          "link_flair_template_id": "b093f3ee-17bd-11e9-ad8c-0eb3737ae650",
          "link_flair_text": "Tips",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_metadata": {
              "j9uvm844flz81": {
                  "e": "Image",
                  "id": "j9uvm844flz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/j9uvm844flz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=0590471a364f565a3158c409ce0466da7a901c13",
                          "x": 108,
                          "y": 49
                      },
                      {
                          "u": "https://preview.redd.it/j9uvm844flz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=4bfffef96eb456019218e59ef256ea44566c5839",
                          "x": 216,
                          "y": 99
                      },
                      {
                          "u": "https://preview.redd.it/j9uvm844flz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=2c17178ca6030b7e6d77defef9bfbdd735d7c03d",
                          "x": 320,
                          "y": 147
                      },
                      {
                          "u": "https://preview.redd.it/j9uvm844flz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=3ace14cb966b9386f126f37ac9e012b56512c1dc",
                          "x": 640,
                          "y": 295
                      },
                      {
                          "u": "https://preview.redd.it/j9uvm844flz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=42c753423b7dae14c3ef45cface4fe2e42dcff33",
                          "x": 960,
                          "y": 443
                      },
                      {
                          "u": "https://preview.redd.it/j9uvm844flz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=f90c25b3093b4e3b5694c39f66fb2ed2e9fcf813",
                          "x": 1080,
                          "y": 498
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/j9uvm844flz81.jpg?width=2436&amp;format=pjpg&amp;auto=webp&amp;s=1d9db07e42e7f7a617803ad81067573b67e4e490",
                      "x": 2436,
                      "y": 1125
                  },
                  "status": "valid"
              },
              "qv44i744flz81": {
                  "e": "Image",
                  "id": "qv44i744flz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/qv44i744flz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=9520029f23ae1775756551337960a76ff643f1b9",
                          "x": 108,
                          "y": 65
                      },
                      {
                          "u": "https://preview.redd.it/qv44i744flz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=8a93bdaaa731a58d8d04e6120d8484e32326e317",
                          "x": 216,
                          "y": 130
                      },
                      {
                          "u": "https://preview.redd.it/qv44i744flz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=31d86c4c12eedab4dc5381e778f9a2b371a59eca",
                          "x": 320,
                          "y": 193
                      },
                      {
                          "u": "https://preview.redd.it/qv44i744flz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=e3565e6fd98d2a5b6400327d8395d6073d3f8227",
                          "x": 640,
                          "y": 386
                      },
                      {
                          "u": "https://preview.redd.it/qv44i744flz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=cac73e04d55c41f4e5387b272bbf454376ccd644",
                          "x": 960,
                          "y": 579
                      },
                      {
                          "u": "https://preview.redd.it/qv44i744flz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=aa80785495e50250099f17ba46a3af67c29af9d1",
                          "x": 1080,
                          "y": 651
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/qv44i744flz81.jpg?width=1864&amp;format=pjpg&amp;auto=webp&amp;s=78608623f43ce3505de33aad0b5affba3975955c",
                      "x": 1864,
                      "y": 1125
                  },
                  "status": "valid"
              },
              "vnx27844flz81": {
                  "e": "Image",
                  "id": "vnx27844flz81",
                  "m": "image/jpg",
                  "p": [
                      {
                          "u": "https://preview.redd.it/vnx27844flz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=39ac2983603a52658341d430a49d1fd7289757a5",
                          "x": 108,
                          "y": 49
                      },
                      {
                          "u": "https://preview.redd.it/vnx27844flz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=a7f86972730147ca8d284d005174b8c3549bf160",
                          "x": 216,
                          "y": 99
                      },
                      {
                          "u": "https://preview.redd.it/vnx27844flz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=bd84194587d72c3fbc4aa3af3100f3ff055a1134",
                          "x": 320,
                          "y": 147
                      },
                      {
                          "u": "https://preview.redd.it/vnx27844flz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=56550ca4a298161db0b06e4b569673d456825cd5",
                          "x": 640,
                          "y": 295
                      },
                      {
                          "u": "https://preview.redd.it/vnx27844flz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=ae2391e8c7c0b56d27dcd6244531825c7aad2a38",
                          "x": 960,
                          "y": 443
                      },
                      {
                          "u": "https://preview.redd.it/vnx27844flz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=73ca9ab264c90ee7621f6dd87982321401223d91",
                          "x": 1080,
                          "y": 498
                      }
                  ],
                  "s": {
                      "u": "https://preview.redd.it/vnx27844flz81.jpg?width=2436&amp;format=pjpg&amp;auto=webp&amp;s=764d34fd23f2bd9307e324d933312345b174c75e",
                      "x": 2436,
                      "y": 1125
                  },
                  "status": "valid"
              }
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uq0xb0/advice_on_choosing_units_for_custom_group_summon/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652600133,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142701,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/BK3ok8CK3dnrSIgoVmYI2Tgy2WiZWc5Vr0a0HpL-SOQ.jpg",
          "thumbnail_height": 84,
          "thumbnail_width": 140,
          "title": "Advice on choosing units for custom group summon",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/gallery/uq0xb0",
          "url_overridden_by_dest": "https://www.reddit.com/gallery/uq0xb0",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "xDandylion",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_238enoy9",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652590800,
          "domain": "/r/EpicSeven/comments/upyozx/play_the_animation/",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upyozx/play_the_animation/",
          "gildings": {},
          "id": "upyozx",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": true,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upyozx/play_the_animation/",
          "pinned": false,
          "post_hint": "hosted:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "DJ6hDZp2X50B7-8ajDgtWt5906fXm2hqiamEp_5l59c",
                      "resolutions": [
                          {
                              "height": 60,
                              "url": "https://external-preview.redd.it/LHm1pTZR0x2cNpZ67IL2hgmlNGNVCodmBe9siIv1xu0.png?width=108&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=aaccfb52cc3c288fd064a1204bf3b6ebbb1cc692",
                              "width": 108
                          },
                          {
                              "height": 121,
                              "url": "https://external-preview.redd.it/LHm1pTZR0x2cNpZ67IL2hgmlNGNVCodmBe9siIv1xu0.png?width=216&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=6fafc4a2d8211586c57086af564cf2a12507bc5d",
                              "width": 216
                          },
                          {
                              "height": 180,
                              "url": "https://external-preview.redd.it/LHm1pTZR0x2cNpZ67IL2hgmlNGNVCodmBe9siIv1xu0.png?width=320&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=7c1d31003dbd731e07d6c83b03ded44e991e055f",
                              "width": 320
                          },
                          {
                              "height": 360,
                              "url": "https://external-preview.redd.it/LHm1pTZR0x2cNpZ67IL2hgmlNGNVCodmBe9siIv1xu0.png?width=640&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=6022e268e6fee702a75dcf36aa1da00d518daad5",
                              "width": 640
                          },
                          {
                              "height": 540,
                              "url": "https://external-preview.redd.it/LHm1pTZR0x2cNpZ67IL2hgmlNGNVCodmBe9siIv1xu0.png?width=960&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=bf775ab29e2fa30d42f54da030552f03eac28f34",
                              "width": 960
                          },
                          {
                              "height": 607,
                              "url": "https://external-preview.redd.it/LHm1pTZR0x2cNpZ67IL2hgmlNGNVCodmBe9siIv1xu0.png?width=1080&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=3d514a15a682aae9021df0e3ba4ac3968315ec44",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://external-preview.redd.it/LHm1pTZR0x2cNpZ67IL2hgmlNGNVCodmBe9siIv1xu0.png?format=pjpg&amp;auto=webp&amp;s=a82efc8f463249f7af387c53164425c78822fb6e",
                          "width": 1920
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652590811,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142697,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/NdgncN80YlU3vekhZtEZAHpiGAbrl5v4jfQdbAhsMy4.jpg",
          "thumbnail_height": 78,
          "thumbnail_width": 140,
          "title": "\"Play the Animation\"",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://v.redd.it/5jdquk5enkz81",
          "url_overridden_by_dest": "https://v.redd.it/5jdquk5enkz81",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Competitive-Delay827",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_3hm7o3x9",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652587683,
          "domain": "youtu.be",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upxvux/finally_got_a_banshee_13_one_shot/",
          "gildings": {},
          "id": "upxvux",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#cc66ff",
          "link_flair_css_class": "tip",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Tips"
              }
          ],
          "link_flair_template_id": "b093f3ee-17bd-11e9-ad8c-0eb3737ae650",
          "link_flair_text": "Tips",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media": {
              "oembed": {
                  "author_name": "Epic Masamune",
                  "author_url": "https://www.youtube.com/channel/UCUed6r67r13pfuVFsKlgclA",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/li0MNJWNvDY?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/li0MNJWNvDY/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Epic Seven Baiken Banshee 13 One Shot And Builds![Epic 7]",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/li0MNJWNvDY?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "scrolling": false,
              "width": 356
          },
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upxvux/finally_got_a_banshee_13_one_shot/",
          "pinned": false,
          "post_hint": "rich:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "qcU7hJtom12jF2r7qow3UDjaZVDlUYvDRfC8LbhxUkI",
                      "resolutions": [
                          {
                              "height": 81,
                              "url": "https://external-preview.redd.it/P2KH3Q5E_uJ6GwfzNykZXEmp6UkP8e66GLtXe0Xc4hQ.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=ace10d011a76bd4aa21bbf8d887ab156392a1098",
                              "width": 108
                          },
                          {
                              "height": 162,
                              "url": "https://external-preview.redd.it/P2KH3Q5E_uJ6GwfzNykZXEmp6UkP8e66GLtXe0Xc4hQ.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=ce1957fc2cf20f66f1f45b46a20e284acb9fb82e",
                              "width": 216
                          },
                          {
                              "height": 240,
                              "url": "https://external-preview.redd.it/P2KH3Q5E_uJ6GwfzNykZXEmp6UkP8e66GLtXe0Xc4hQ.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=11d26b52bffc425f39938f4f594a951eb5233d24",
                              "width": 320
                          }
                      ],
                      "source": {
                          "height": 360,
                          "url": "https://external-preview.redd.it/P2KH3Q5E_uJ6GwfzNykZXEmp6UkP8e66GLtXe0Xc4hQ.jpg?auto=webp&amp;s=cbc42107534f2037f3bc8b671b727bae5b47bf1f",
                          "width": 480
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652587693,
          "score": 1,
          "secure_media": {
              "oembed": {
                  "author_name": "Epic Masamune",
                  "author_url": "https://www.youtube.com/channel/UCUed6r67r13pfuVFsKlgclA",
                  "height": 200,
                  "html": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/li0MNJWNvDY?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
                  "provider_name": "YouTube",
                  "provider_url": "https://www.youtube.com/",
                  "thumbnail_height": 360,
                  "thumbnail_url": "https://i.ytimg.com/vi/li0MNJWNvDY/hqdefault.jpg",
                  "thumbnail_width": 480,
                  "title": "Epic Seven Baiken Banshee 13 One Shot And Builds![Epic 7]",
                  "type": "video",
                  "version": "1.0",
                  "width": 356
              },
              "type": "youtube.com"
          },
          "secure_media_embed": {
              "content": "&lt;iframe width=\"356\" height=\"200\" src=\"https://www.youtube.com/embed/li0MNJWNvDY?feature=oembed&amp;enablejsapi=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;",
              "height": 200,
              "media_domain_url": "https://www.redditmedia.com/mediaembed/upxvux",
              "scrolling": false,
              "width": 356
          },
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142690,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/xIcSn-U5uFZNXsSaUmzGaegUInEOxDgMd_LEtg9eA6o.jpg",
          "thumbnail_height": 105,
          "thumbnail_width": 140,
          "title": "Finally got a banshee 13 one shot \ud83d\ude01",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://youtu.be/li0MNJWNvDY",
          "url_overridden_by_dest": "https://youtu.be/li0MNJWNvDY",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Xelllllll",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_6c28iapp",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652584751,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upx2sw/i_really_dont_know_how_to_build_a_defense_team/",
          "gildings": {},
          "id": "upx2sw",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": false,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upx2sw/i_really_dont_know_how_to_build_a_defense_team/",
          "pinned": false,
          "pwls": 7,
          "retrieved_on": 1652584762,
          "score": 1,
          "selftext": "I got Cermia, Sol, Lillias, Yufine, BBK, STene, SSB, Briar Witch Iseria, Cidd, TSurin, Vivian, BDingo, Ruele, Diene, Dizzy, Krau, BM Haste, and ACartuja built  \n\n\nWhat teams can I make out of these?",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142686,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "I really don't know how to build a defense team",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/upx2sw/i_really_dont_know_how_to_build_a_defense_team/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Fraeniir",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_hwgdcce",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652581529,
          "domain": "/r/EpicSeven/comments/upw64j/a_day_in_the_life_of_a_counter_gamer/",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upw64j/a_day_in_the_life_of_a_counter_gamer/",
          "gildings": {},
          "id": "upw64j",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": true,
          "link_flair_background_color": "#9a9d84",
          "link_flair_css_class": "fluff",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Fluff"
              }
          ],
          "link_flair_template_id": "93eaffd2-f53d-11e8-bee8-0e49921681f2",
          "link_flair_text": "Fluff",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upw64j/a_day_in_the_life_of_a_counter_gamer/",
          "pinned": false,
          "post_hint": "hosted:video",
          "preview": {
              "enabled": false,
              "images": [
                  {
                      "id": "aggO7xoiFSQ8hAGhDW1YvqOSNZ0U6aArc3_xoaaojxQ",
                      "resolutions": [
                          {
                              "height": 49,
                              "url": "https://external-preview.redd.it/TDj2eMfe4BUwbvrOGbWEaXiR7ns_u327d0ZBpzNR2EE.png?width=108&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=8f413d4ebf5d89bb8b7c6475bcc6dc7271848fb2",
                              "width": 108
                          },
                          {
                              "height": 99,
                              "url": "https://external-preview.redd.it/TDj2eMfe4BUwbvrOGbWEaXiR7ns_u327d0ZBpzNR2EE.png?width=216&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=b7c93adc44e4a8a75d6680c7a2eb7497cbe341f3",
                              "width": 216
                          },
                          {
                              "height": 147,
                              "url": "https://external-preview.redd.it/TDj2eMfe4BUwbvrOGbWEaXiR7ns_u327d0ZBpzNR2EE.png?width=320&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=a5335061149ab4d8cd14002d447860edf6ce4802",
                              "width": 320
                          },
                          {
                              "height": 295,
                              "url": "https://external-preview.redd.it/TDj2eMfe4BUwbvrOGbWEaXiR7ns_u327d0ZBpzNR2EE.png?width=640&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=b2eebf32e2437a537d19fdfc0b3d0ae23834a9e8",
                              "width": 640
                          },
                          {
                              "height": 443,
                              "url": "https://external-preview.redd.it/TDj2eMfe4BUwbvrOGbWEaXiR7ns_u327d0ZBpzNR2EE.png?width=960&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=b1ce75129da67e38f9845d9cb8eba37d110ea1e3",
                              "width": 960
                          },
                          {
                              "height": 498,
                              "url": "https://external-preview.redd.it/TDj2eMfe4BUwbvrOGbWEaXiR7ns_u327d0ZBpzNR2EE.png?width=1080&amp;crop=smart&amp;format=pjpg&amp;auto=webp&amp;s=133f61529bfc5b75a390fb952c7c91a997aacc37",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 886,
                          "url": "https://external-preview.redd.it/TDj2eMfe4BUwbvrOGbWEaXiR7ns_u327d0ZBpzNR2EE.png?format=pjpg&amp;auto=webp&amp;s=d0ae6f773393086692b8740713f8778f3e9d91c5",
                          "width": 1920
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652581539,
          "score": 1,
          "selftext": "",
          "send_replies": false,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142686,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/oPT_48tKeN2bqwYvEcTnAWYV2KVgTvzUZUsVupysmG4.jpg",
          "thumbnail_height": 64,
          "thumbnail_width": 140,
          "title": "A Day in the Life of a Counter Gamer",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://v.redd.it/57qz9s7ovjz81",
          "url_overridden_by_dest": "https://v.redd.it/57qz9s7ovjz81",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "Snipe_knight94",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_8ecerl48",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652581163,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upw2dv/looking_for_guild_to_join_account_level_34_i_play/",
          "gildings": {},
          "id": "upw2dv",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": false,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 1,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upw2dv/looking_for_guild_to_join_account_level_34_i_play/",
          "pinned": false,
          "pwls": 7,
          "removed_by_category": "moderator",
          "retrieved_on": 1652581173,
          "score": 1,
          "selftext": "[removed]",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142686,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "Looking for guild to join. Account level 34. I play daily. If any other information is needed let me know.",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/upw2dv/looking_for_guild_to_join_account_level_34_i_play/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "DocVIP808",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_4c073lr1",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652579848,
          "domain": "self.EpicSeven",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upvoms/which_3_or_4_would_you_get_to_6_for_pve_i_have_4/",
          "gildings": {},
          "id": "upvoms",
          "is_created_from_ads_ui": false,
          "is_crosspostable": false,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": false,
          "is_robot_indexable": false,
          "is_self": true,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 1,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upvoms/which_3_or_4_would_you_get_to_6_for_pve_i_have_4/",
          "pinned": false,
          "pwls": 7,
          "removed_by_category": "moderator",
          "retrieved_on": 1652579859,
          "score": 1,
          "selftext": "[removed]",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142684,
          "subreddit_type": "public",
          "thumbnail": "self",
          "title": "which 3*** or 4**** would you get to 6****** for PVE? I have 4 bottles of ascension \ud83d\ude05",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://www.reddit.com/r/EpicSeven/comments/upvoms/which_3_or_4_would_you_get_to_6_for_pve_i_have_4/",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "churchillswaglyfe",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_lj8j3",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652579400,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upvk0g/trying_to_build_my_celine_400_more_atk_or_20_more/",
          "gildings": {},
          "id": "upvk0g",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#2f3136",
          "link_flair_css_class": "Team",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Team Building"
              }
          ],
          "link_flair_template_id": "b218f9b4-f79f-11e8-9a07-0eb6e5f1edcc",
          "link_flair_text": "Team Building",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upvk0g/trying_to_build_my_celine_400_more_atk_or_20_more/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "psitH9q4FJ1SQ-gDd3x3AMBUdd8cELquf_J8QcuKsNg",
                      "resolutions": [
                          {
                              "height": 48,
                              "url": "https://preview.redd.it/9qowlovhpjz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=6ffc49feadaae08df737b38972deaa50fe858d0b",
                              "width": 108
                          },
                          {
                              "height": 97,
                              "url": "https://preview.redd.it/9qowlovhpjz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=381379d91f7e5b2af1368244429b3bae75ea3900",
                              "width": 216
                          },
                          {
                              "height": 144,
                              "url": "https://preview.redd.it/9qowlovhpjz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=941b5c400a4630861c377e30239af2a5ec6c9e8a",
                              "width": 320
                          },
                          {
                              "height": 288,
                              "url": "https://preview.redd.it/9qowlovhpjz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=5641c581a0325ecf0f471a4458bac6311b09fbf1",
                              "width": 640
                          },
                          {
                              "height": 432,
                              "url": "https://preview.redd.it/9qowlovhpjz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=794d7ece5af8e0d19768095d8c7251f12d1df569",
                              "width": 960
                          },
                          {
                              "height": 486,
                              "url": "https://preview.redd.it/9qowlovhpjz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=882935a4d111b8149ba7d114003f955cfd234739",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1080,
                          "url": "https://preview.redd.it/9qowlovhpjz81.jpg?auto=webp&amp;s=87ffd297c52df311f4ced06251af614cbbc16b58",
                          "width": 2400
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652579411,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142683,
          "subreddit_type": "public",
          "thumbnail": "https://b.thumbs.redditmedia.com/-i3W_E7MWrK5Qhe05OPpPEIJiyucKMwR86TkK0AhlTI.jpg",
          "thumbnail_height": 63,
          "thumbnail_width": 140,
          "title": "Trying to build my Celine. 400 more atk or 20% more crit dmg?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/9qowlovhpjz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/9qowlovhpjz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "sadupilu",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_39zmma4q",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652577185,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/upuxc7/help_im_getting_this_for_like_a_whole_day/",
          "gildings": {},
          "id": "upuxc7",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#cc66ff",
          "link_flair_css_class": "tip",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Tips"
              }
          ],
          "link_flair_template_id": "b093f3ee-17bd-11e9-ad8c-0eb3737ae650",
          "link_flair_text": "Tips",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/upuxc7/help_im_getting_this_for_like_a_whole_day/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "1MY3bGutkihShYuLsQbGCfac_MGy82bYv_Y1smhzS3Q",
                      "resolutions": [
                          {
                              "height": 60,
                              "url": "https://preview.redd.it/t8ulbk8fijz81.png?width=108&amp;crop=smart&amp;auto=webp&amp;s=f9f0488677a40ad771ade928a6345783503a790a",
                              "width": 108
                          },
                          {
                              "height": 121,
                              "url": "https://preview.redd.it/t8ulbk8fijz81.png?width=216&amp;crop=smart&amp;auto=webp&amp;s=1f3b20b23bd30d4e0bf63614dcda559798fea054",
                              "width": 216
                          },
                          {
                              "height": 180,
                              "url": "https://preview.redd.it/t8ulbk8fijz81.png?width=320&amp;crop=smart&amp;auto=webp&amp;s=409af9cb505246ef888ce8da184cbcdb58745db2",
                              "width": 320
                          },
                          {
                              "height": 360,
                              "url": "https://preview.redd.it/t8ulbk8fijz81.png?width=640&amp;crop=smart&amp;auto=webp&amp;s=c31e3cc4482905591d42918f76280a1bd18ad0d3",
                              "width": 640
                          },
                          {
                              "height": 540,
                              "url": "https://preview.redd.it/t8ulbk8fijz81.png?width=960&amp;crop=smart&amp;auto=webp&amp;s=5ceb167b617ad2b781bfbf210c888581156700ca",
                              "width": 960
                          },
                          {
                              "height": 607,
                              "url": "https://preview.redd.it/t8ulbk8fijz81.png?width=1080&amp;crop=smart&amp;auto=webp&amp;s=47565dc060f7121fa15adc47de51158efbe2d903",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 900,
                          "url": "https://preview.redd.it/t8ulbk8fijz81.png?auto=webp&amp;s=640f54df3ac37d31b1d0959976a6a66206ab185b",
                          "width": 1600
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652577196,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142681,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/rN6i2eeRi2FG_VSUsW26qYeNRw2xwVU7nlgbyjnjDj8.jpg",
          "thumbnail_height": 78,
          "thumbnail_width": 140,
          "title": "Help ! im getting this for like a whole day",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/t8ulbk8fijz81.png",
          "url_overridden_by_dest": "https://i.redd.it/t8ulbk8fijz81.png",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "miusnama",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_e4bhuxtp",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652573925,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uptyxw/aria_by_me/",
          "gildings": {},
          "id": "uptyxw",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#b56e38",
          "link_flair_css_class": "Art",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Art"
              }
          ],
          "link_flair_template_id": "8d6ec580-f542-11e8-b973-0e8766f08cb0",
          "link_flair_text": "Art",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uptyxw/aria_by_me/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "UJ3_YOusvHB321vvEuu3F1y7hMEqBxerxQq7Fn_31yI",
                      "resolutions": [
                          {
                              "height": 160,
                              "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=6f64bdacaa0ab2c2cd80e3cdbb26fefa0fc6f1c9",
                              "width": 108
                          },
                          {
                              "height": 321,
                              "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=4349793935cd0712b14cd6949c2a008a60fb2d75",
                              "width": 216
                          },
                          {
                              "height": 476,
                              "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=5563dbb5c077fff8b2de743bf329feff95cb1239",
                              "width": 320
                          },
                          {
                              "height": 952,
                              "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=39b65caa730f229d93e5ad13d1bf5e8754490221",
                              "width": 640
                          },
                          {
                              "height": 1428,
                              "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=815ad6e8389a194e98dbcd4c90d1ccab0ee6df32",
                              "width": 960
                          },
                          {
                              "height": 1606,
                              "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=aa805a8f08b7846da16e70aa710fa6c0d6c34add",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 5100,
                          "url": "https://preview.redd.it/gc6d8uh29jz81.jpg?auto=webp&amp;s=5bc0a5ce267fef7d72fb503aa278b69c4ecd23dc",
                          "width": 3428
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652573935,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142679,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/Svx4W6MoNGhqmhZ4BdnHoXjuFARKsxOo63dGcWgWgX8.jpg",
          "thumbnail_height": 140,
          "thumbnail_width": 140,
          "title": "Aria by me",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/gc6d8uh29jz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/gc6d8uh29jz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      },
      {
          "all_awardings": [],
          "allow_live_comments": false,
          "author": "HurryHeavy5792",
          "author_flair_css_class": null,
          "author_flair_richtext": [],
          "author_flair_text": null,
          "author_flair_type": "text",
          "author_fullname": "t2_9co0hbr8",
          "author_is_blocked": false,
          "author_patreon_flair": false,
          "author_premium": false,
          "awarders": [],
          "can_mod_post": false,
          "contest_mode": false,
          "created_utc": 1652573157,
          "domain": "i.redd.it",
          "full_link": "https://www.reddit.com/r/EpicSeven/comments/uptqr7/well_uh_i_guess_i_got_lucky/",
          "gildings": {},
          "id": "uptqr7",
          "is_created_from_ads_ui": false,
          "is_crosspostable": true,
          "is_meta": false,
          "is_original_content": false,
          "is_reddit_media_domain": true,
          "is_robot_indexable": true,
          "is_self": false,
          "is_video": false,
          "link_flair_background_color": "#614363",
          "link_flair_css_class": "Discussion",
          "link_flair_richtext": [
              {
                  "e": "text",
                  "t": "Discussion"
              }
          ],
          "link_flair_template_id": "a1681d98-f53d-11e8-9baf-0eddda339404",
          "link_flair_text": "Discussion",
          "link_flair_text_color": "light",
          "link_flair_type": "richtext",
          "locked": false,
          "media_only": false,
          "no_follow": true,
          "num_comments": 0,
          "num_crossposts": 0,
          "over_18": false,
          "parent_whitelist_status": "some_ads",
          "permalink": "/r/EpicSeven/comments/uptqr7/well_uh_i_guess_i_got_lucky/",
          "pinned": false,
          "post_hint": "image",
          "preview": {
              "enabled": true,
              "images": [
                  {
                      "id": "VbpnA7oF8UbAY5fUdxmdF7N4Zl1e20RTb-xlrUvNF1o",
                      "resolutions": [
                          {
                              "height": 67,
                              "url": "https://preview.redd.it/udvlkwix6jz81.jpg?width=108&amp;crop=smart&amp;auto=webp&amp;s=8e348e4912c7e5cabb0d7c0de8ce4672cdadf47c",
                              "width": 108
                          },
                          {
                              "height": 135,
                              "url": "https://preview.redd.it/udvlkwix6jz81.jpg?width=216&amp;crop=smart&amp;auto=webp&amp;s=edfb59788910867f031a097d666b910ba74232ac",
                              "width": 216
                          },
                          {
                              "height": 200,
                              "url": "https://preview.redd.it/udvlkwix6jz81.jpg?width=320&amp;crop=smart&amp;auto=webp&amp;s=21f4e01851bc61a3ee05c160597e6e5f94c09e5c",
                              "width": 320
                          },
                          {
                              "height": 400,
                              "url": "https://preview.redd.it/udvlkwix6jz81.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=79782705bade76b897d946eb4bfb8fc46399a225",
                              "width": 640
                          },
                          {
                              "height": 600,
                              "url": "https://preview.redd.it/udvlkwix6jz81.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=66fa0a695f17256d3a293246f044fed3097174a8",
                              "width": 960
                          },
                          {
                              "height": 675,
                              "url": "https://preview.redd.it/udvlkwix6jz81.jpg?width=1080&amp;crop=smart&amp;auto=webp&amp;s=1e6015a4b244bca611b02ef5da0e6ea07ef8d7cb",
                              "width": 1080
                          }
                      ],
                      "source": {
                          "height": 1600,
                          "url": "https://preview.redd.it/udvlkwix6jz81.jpg?auto=webp&amp;s=6642a5557fe1b5fd2d478e13fa31633e5cea4fe0",
                          "width": 2560
                      },
                      "variants": {}
                  }
              ]
          },
          "pwls": 7,
          "retrieved_on": 1652573167,
          "score": 1,
          "selftext": "",
          "send_replies": true,
          "spoiler": false,
          "stickied": false,
          "subreddit": "EpicSeven",
          "subreddit_id": "t5_nrn6j",
          "subreddit_subscribers": 142679,
          "subreddit_type": "public",
          "thumbnail": "https://a.thumbs.redditmedia.com/oQPqn1mX2ww-JKG4mXZVZCUi-OZPeB--3_nkBKL76_4.jpg",
          "thumbnail_height": 87,
          "thumbnail_width": 140,
          "title": "well uh, I guess I got lucky?",
          "total_awards_received": 0,
          "treatment_tags": [],
          "upvote_ratio": 1.0,
          "url": "https://i.redd.it/udvlkwix6jz81.jpg",
          "url_overridden_by_dest": "https://i.redd.it/udvlkwix6jz81.jpg",
          "whitelist_status": "some_ads",
          "wls": 7
      }
  ]
};

export function getPushShiftDefaultData() {
    return [
        {
            full_link: '.',
            created_utc: Date.now() / 1000,
            author: 'miscavel', 
            subreddit: 'miscavel.gitlab.io', 
            title: 'Nothing to preview. Search to display more results.', 
            thumbnail: '',
        },
        {
            full_link: '.',
            created_utc: Date.now() / 1000,
            author: 'miscavel', 
            subreddit: 'miscavel.gitlab.io', 
            title: `Only the first 25 items are shown. Set 'before' to see more.`, 
            thumbnail: '',
        },
        {
            full_link: '.',
            created_utc: Date.now() / 1000,
            author: 'miscavel', 
            subreddit: 'miscavel.gitlab.io', 
            title: `Use 'flair:{flair_name}' for flairs, e.g. 'flair:art'.`, 
            thumbnail: '',
        },
    ];
}