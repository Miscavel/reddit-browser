import { useState } from 'react';
import './App.css';
import SearchBar from './component/SearchBar';
import Carousel from './component/shared/Carousel/Carousel';
import { getPushShiftDefaultData } from './mock/pushshiftReddit';
import { getPushShiftRedditPosts } from './services/pushshiftReddit';

function App() {
  const [ items, setItems ] = useState(getPushShiftDefaultData());
  const [ isLoading, setLoading ] = useState(false);
  const [ oldestItemTimestamp, setOldestItemTimestamp ] = useState(Date.now());

  function onSearch(subreddit, query, timestamp, flair) {
    setLoading(true);
    fetchData(subreddit, 25, query, timestamp)
      .then(function(res) {
        updateItems(res.data, flair);
        setOldestItemTimestamp(getOldestItemTimestamp(res.data));
      })
      .finally(function() {
        setLoading(false);
      });
  }

  function fetchData(subreddit, entryPerPage, query, before) {
    return getPushShiftRedditPosts(subreddit, entryPerPage, query, before);
  }

  function filterItemsWithRelevantFlair(items, flair) {
    if (!flair) return items;
    
    return items.filter(function(item) {
      const { link_flair_text } = item;

      if (!link_flair_text) return false;

      return link_flair_text.toLowerCase() === flair.toLowerCase();
    });
  }

  function updateItems(newItems, flair) {
    const filteredItems = filterItemsWithRelevantFlair(newItems, flair);

    if (!filteredItems || filteredItems.length === 0) {
      setItems(getPushShiftDefaultData());
    } else {
      setItems(filteredItems);
    }
  }

  function getOldestItemTimestamp(items) {
    let minTimestamp = Number.MAX_SAFE_INTEGER;
    items.forEach(function(post) {
      minTimestamp = Math.min(minTimestamp, post.created_utc);
    });
    return Math.min(minTimestamp * 1000, Date.now());
  }

  return (
    <div className="App">
      <div className='content'>
        <div className='section--searchbar'>
          <SearchBar
            onSearch={ onSearch }
            isLoading={ isLoading }
            oldestItemTimestamp={ oldestItemTimestamp }
          >
          </SearchBar>
        </div>
        <div className='section--carousel'>
          <Carousel
            items={ items }
            isLoading={ isLoading }
          >
          </Carousel>
        </div>
      </div>
    </div>
  );
}

export default App;
